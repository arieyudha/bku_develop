<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
    <div class="col-md-4">
        <div class="box">
            <div class="box-header bg-gray">
                <form name='fLevel' method='get'>
                    <select class="form-control select2 level_user" style="width: 100%;" name='level_user' onchange='document.fLevel.submit();'>
                        <option value="">.: Pilih Level User :.</option>
                        <?php
                        foreach ($get_levelUser as $lev_row) {
                            if ($lev_row->kd_level == $kd_level) {
                                $att = 'selected';
                            } else {
                                $att = '';
                            }
                            echo '<option ' . $att . ' data-kd_level="' . $lev_row->kd_level . '"  value="' . $lev_row->kd_level . '">' . $lev_row->ket_level . '</option>';
                        }
                        ?>
                    </select>
                </form>
            </div>
            <?php if (!empty($kd_level)) { ?>
                <form role="form" class="form-input-user" method="POST" action="<?= site_url('setting/Set_user/insertUser'); ?>">
                    <div class="box-body">
                        <div class="form-group data-pptk">
                            <?php
                            if ($kd_level == 2) {
                                $req = 'required';
                                ?>
                                <label class="label_pptk">Pilih PPTK</label>
                                <select <?= $req; ?> class="select2 form-control data-pptk" name="kd_jabatan" style="width: 100%">
                                    <option value="">.: Pilih PPTK :.</option>
                                    <?php
                                    foreach ($get_dataPptk as $row_pptk) {
                                        $att_pptk = '';
                                        foreach ($get_user as $row) {
                                            if ($row_pptk['nip'] == $row->username) {
                                                $att_pptk = 'disabled';
                                            }
                                        }
                                        ?>
                                        <option <?= $att_pptk; ?>
                                            data-nip="<?= $row_pptk['nip']; ?>"
                                            data-nama_jabatan="<?= $row_pptk['nama_jabatan']; ?>"
                                            value="<?= $row_pptk['kd_jabatan']; ?>"><?= $row_pptk['nama_jabatan'] . ' (' . $row_pptk['nama_lengkap'] . ')'; ?></option>
                                            <?php
                                        }
                                        ?>
                                </select>
                            <?php } ?>          
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control username" placeholder="Username" name="username" required>
                        </div>
                        <div class="form-group text-password">
                            <label>Password</label>
                            <input type="password" class="form-control password" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label>Nama User</label>
                            <input type="text" class="form-control nama_user" placeholder="Nama User" name="nama_user" required>
                        </div>
                        <input type="hidden" class="form-control" name="url" value="<?= $url; ?>">
                        <input type="hidden" class="form-control kd_level" name="kd_level" value="<?= $kd_level; ?>">
                        <input type="hidden" class="form-control kd_user" name="kd_user" value="<?= $kd_user_new; ?>">
                    </div>
                    <div class="box-footer center">
                        <button type="submit" class="btn btn-primary btn-save">Simpan</button>
                        <button type="submit" disabled class="btn btn-warning btn-update">Edit</button>
                        <button type="button" class="btn btn-danger" onclick="window.location.reload()">Reset</button>
                    </div>
                </form>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-8">
        <!-- DIRECT CHAT -->
        <div class="box">
            <div class="box-header bg-gray">
                <h3 class="box-title">Tabel User</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-hover table-bordered tabel_3" width='100%'>
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Username</th>
                            <th>Nama User</th>
                            <th width="8%">Status</th>
                            <th width="8%"><i class="fa fa-refresh"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($get_user as $row) {
                            if ($row->kd_level == $kd_level) {
                                if ($row->kd_user == 1) {
                                    $att = 'disabled';
                                } else {
                                    $att = '';
                                }
                                ?>
                                <tr>
                                    <td class="text-center"><?= $no++; ?></td>
                                    <td><?= $row->username; ?></td>
                                    <td><?= $row->nama_user; ?></td>
                                    <td class="text-center">
                                        <input <?php
                                        if ($row->is_active == 1) {
                                            echo "checked='checked'";
                                            $kunci = "Terbuka";
                                        } else {
                                            $kunci = "Terkunci";
                                        }
                                        ?> class="form-check" type="checkbox" onclick="kunciUser(<?= $row->kd_user ?>, <?= $row->kd_level ?>, <?= $row->is_active ?>)" name="checkbox-toggle">
                                        <br>
                                        <label class="label-info label"><?= $kunci ?></label>
                                    </td>
                                    <td class="text-center">
                                        <?php if (!empty($kd_level)) { ?>
                                            <button <?= $att; ?> 
                                                data-username="<?= $row->username; ?>"  
                                                data-kd_user="<?= $row->kd_user; ?>"
                                                data-kd_level="<?= $row->kd_level; ?>"
                                                data-nama_user="<?= $row->nama_user; ?>"
                                                data-password="<?= deskripsiText($row->password); ?>"
                                                class="btn btn-warning btn-xs btn-flat btn-edit"><i class="fa fa-pencil"></i></button>
                                            <button <?= $att; ?> onclick="hapusUser('<?= $row->kd_user; ?>', '<?= $kd_level; ?>', '<?= $row->nama_user; ?>')" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-trash"></i></button>
                                            <?php } ?>
                                    </td>  
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function kunciUser(kd_user, kd_level, iskunci) {
        var url = "<?= site_url('setting/User/kunciUser'); ?>";

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                kd_user: kd_user,
                kd_level: kd_level,
                iskunci: iskunci
            },
            success: function (data) {
                if (data == 'true') {
                    notif_smartAlertSukses('Berhasil');
                } else {
                    notif_smartAlertGagal('Gagal');
                }
            }
        });
    }
    $(document).ready(function () {
        callBackClassAfter('.username', 'cek-username');
        $('.username').change(function () {
            const username = $(this).val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?= base_url('setting/Set_user/getUsername'); ?>',
                data: {username: username},
                success: (result) => {
                    console.log(result.cek)
                    if (result.cek == 0) {
                        $('.btn-save').prop('disabled', false);
                        $('.cek-username').html('<img src="<?php echo base_url(); ?>/assets/img/true.png"><b style="color:green;"> Username Valid</b>');
                    } else {
                        $('.btn-save').prop('disabled', true);
                        $('.cek-username').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Username Double</b>');
                    }
                }
            });
        });

        $('.data-pptk').change(function () {
            const nama_jabatan = $(this).find('option:selected').data('nama_jabatan');
            const nip = $(this).find('option:selected').data('nip');
            $('.nama_user').val(nama_jabatan).prop('readonly', true);
            $('.username').val(nip).prop('readonly', true);
        });

        $('.btn-edit').click(function () {
            const kd_level = $(this).data('kd_level');
            const username = $(this).data('username');
            const nama_user = $(this).data('nama_user');
            const kd_user = $(this).data('kd_user');
            const password = $(this).data('password');
            if (kd_level == 2) {
                $('.data-pptk').addClass('hidden');
                $('.username').val(username).prop('readonly', true);
            } else {
                $('.data-pptk').removeClass('hidden');
                $('.username').val(username);
            }
            $('.nama_user').val(nama_user);
            $('.kd_user').val(kd_user);
            $('.password').val(password);
            $('.form-input-user').prop('action', '<?= site_url('setting/Set_user/updateUser'); ?>');

        });
    });

    $(".editUser").click(function () {
        var button = $(this);
        var kd_user = button.data('kd_user');
        var username = button.data('username');
        var password = button.data('password');
        var nama_user = button.data('nama_user');
        var no_telpon = button.data('no_telpon');
        var email = button.data('email');
        $(".kd_user").val(kd_user);
        $(".username").val(username);
        $(".password").val(password);
        $(".nama_user").val(nama_user);
        $(".no_telpon").val(no_telpon);
        $(".email").val(email);
    });

    $(".kode_unit").change(function () {
        const nm_unit = $(this).find('option:selected').data('nm_unit');
        const kode = $(this).find('option:selected').data('kode');
        $('.nama_user').val(nm_unit).prop('readonly', true);
    });
    $(".load_subunit_user").change(function () {
        const nm_sub_unit = $(this).find('option:selected').data('nm_sub_unit');
        $('.nama_user').val(nm_sub_unit).prop('readonly', true);
    });

    $(".load_unit").change(function () {
        const kd_urusan = $(this).find('option:selected').data('kd_urusan');
        const kd_bidang = $(this).find('option:selected').data('kd_bidang');
        const kd_unit = $(this).find('option:selected').data('kd_unit');
        $.ajax({
            type: 'GET',
            url: '<?= site_url('setting/User/loadSubUnitUser'); ?>',
            data: {kd_urusan: kd_urusan, kd_bidang: kd_bidang, kd_unit: kd_unit},
            success: function (data) {
                $('.load_subunit_user').html(data);
            }
        });
    });

    $(".load_unit").change(function () {
        const kd_urusan = $(this).find('option:selected').data('kd_urusan');
        const kd_bidang = $(this).find('option:selected').data('kd_bidang');
        const kd_unit = $(this).find('option:selected').data('kd_unit');
        $.ajax({
            type: 'GET',
            url: '<?= site_url('setting/User/loadSubUnitPgw'); ?>',
            data: {kd_urusan: kd_urusan, kd_bidang: kd_bidang, kd_unit: kd_unit},
            success: function (data) {
                $('.load_subunit_pgw').html(data);
            }
        });
    });


    $(".load_subunit_pgw").change(function () {
        const kd_urusan = $(this).find('option:selected').data('kd_urusan');
        const kd_bidang = $(this).find('option:selected').data('kd_bidang');
        const kd_unit = $(this).find('option:selected').data('kd_unit');
        const kd_sub = $(this).find('option:selected').data('kd_sub');
//        $('.nama_user').val(nm_unit);
        $.ajax({
            type: "GET",
            url: "<?= site_url('setting/User/getPegawaiSubSkpd'); ?>",
            data: {
                kd_urusan: kd_urusan,
                kd_bidang: kd_bidang,
                kd_unit: kd_unit,
                kd_sub: kd_sub
            },
            cache: false,
            success: function (response) {
                $('.kode_pegawai').html(response);
            },
        })
    });


    $(".kode_pegawai").change(function () {
        const nm_pgw = $(this).find('option:selected').data('nm_pgw');
        const nip = $(this).find('option:selected').data('nip');
        const kode = $(this).find('option:selected').data('kode');
        $('.nama_user').val(nm_pgw).prop('readonly', true);
        $('.username').val(nip).prop('readonly', true);
    });



    function hapusUser(kd_user, kd_level, nama_user) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus User dangen Nama : ' + nama_user,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('setting/Set_user/deleteUser'); ?>",
                    data: {
                        kd_user: kd_user,
                        kd_level: kd_level
                    },
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        )
            }
        })
    }
</script>