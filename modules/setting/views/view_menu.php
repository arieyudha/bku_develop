<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$a = aksesLog();
$m = aksesMenu($a['kd_user']);
$url = getUrl();
echo $javasc;
echo $notifikasi;
if ($kd_user == 1) {
    $att = 'disabled';
} else {
    $att = '';
}
?>
<div class="row">
    <div class="col-md-12">
        <!-- DIRECT CHAT -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                        <button <?= $m['rul_tambah']; ?> class="btn btn-primary btn-sm" 
                                                         data-toggle="modal" 
                                                         data-target="#tambahRoleMenu" 
                                                         data-aksi="tambah"
                                                         data-kd_user="<?= $kd_user; ?>"
                                                         data-parent="0">
                            <i class="fa fa-plus"></i>  Menu
                        </button>
                    </div>
                    <span class="pull-right">
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                            <button <?= $m['rul_tambah']; ?> class="btn btn-primary btn-sm" onclick="insertAll('<?= $kd_user; ?>', 'Tambah Semua Menu')">
                                <i class="fa fa-plus"></i> Semua Menu
                            </button>
                        </div>
                    </span>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="tabel_skpd" class="table tabel_2 table-bordered">
                    <thead>
                        <tr >
                            <th class="bg-gray-active" width="3%">No</th>
                            <th class="bg-gray-active">Nama Menu</th>
                            <th class="bg-gray-active" >LINK</th>
                            <th class="bg-gray-active" width="5%">Tambah</th>
                            <th class="bg-gray-active" width="5%">Edit</th>
                            <th class="bg-gray-active" width="5%">Hapus</th>
                            <th class="bg-gray-active" width="8%"><i class="fa fa-plus"></i></th>
                            <th class="bg-gray-active" width="3%"><i class="fa fa-trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($get_ruleMenu as $row) {
                            if ($row->parent == 0) {
                                ?>
                                <tr style="background-color: #ededed">
                                    <td><?= $row->urutan; ?></td>
                                    <td><?= $row->nama; ?></td>
                                    <td><?= $row->link; ?></td>

                                    <td>
                                        <?php if ($row->link != '#') { ?>
                                            <input type="checkbox" class="form-check"  <?php
                                            if ($row->tambah == 1) {
                                                echo "checked='checked'";
                                            }
                                            ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row->id_menu; ?>', '<?= $row->kd_user; ?>', '<?= $row->tambah; ?>', 'tambah');" >
                                               <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->link != '#') { ?>
                                            <input type="checkbox" class="form-check"  <?php
                                            if ($row->edit == 1) {
                                                echo "checked='checked'";
                                            }
                                            ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row->id_menu; ?>', '<?= $row->kd_user; ?>', '<?= $row->edit; ?>', 'edit');" >
                                               <?php } ?>
                                    </td>
                                    <td>
                                        <?php if ($row->link != '#') { ?>
                                            <input type="checkbox" class="form-check"  <?php
                                            if ($row->hapus == 1) {
                                                echo "checked='checked'";
                                            }
                                            ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row->id_menu; ?>', '<?= $row->kd_user; ?>', '<?= $row->hapus; ?>', 'hapus');">
                                               <?php } ?>
                                    </td>

                                    <td class="no-padding">
                                        <?php if ($row->link == '#') { ?>
                                            <button  <?= $m['rul_tambah']; ?> class="btn btn-primary btn-flat btn-block" data-toggle="modal"
                                                                              data-target="#tambahRoleMenu" 
                                                                              data-aksi="tambah"
                                                                              data-kd_user="<?= $kd_user; ?>"
                                                                              data-parent="<?= $row->id; ?>"><i class="fa fa-plus"></i> Sub Menu</button>
                                                                          <?php } ?>
                                    </td>
                                    <td>
                                        <button <?= $m['rul_hapus']; ?> <?= $att; ?> class="btn btn-danger btn-xs btn-flat" onclick="hapusMenu('<?= $kd_user; ?>', '<?= $row->id_menu; ?>', 'Hapus Menu')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php
                                $nop = 1;
                                foreach ($get_ruleMenu as $row_p) {
                                    if ($row->id == $row_p->parent and $row->link == '#') {
                                        ?>
                                        <tr>
                                            <td><?= $row->urutan . '.' . $row_p->urutan; ?></td>
                                            <td><?= $row_p->nama; ?></td>
                                            <td><?= $row_p->link; ?></td>

                                            <td>
                                                <?php if ($row_p->link != '#') { ?>
                                                    <input type="checkbox" class="form-check" <?php
                                                    if ($row_p->tambah == 1) {
                                                        echo "checked='checked'";
                                                    }
                                                    ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row_p->id_menu; ?>', '<?= $row_p->kd_user; ?>', '<?= $row_p->tambah; ?>', 'tambah');" >
                                                    </div>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if ($row_p->link != '#') { ?>
                                                    <input type="checkbox" class="form-check" <?php
                                                    if ($row_p->edit == 1) {
                                                        echo "checked='checked'";
                                                    }
                                                    ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row_p->id_menu; ?>', '<?= $row_p->kd_user; ?>', '<?= $row_p->edit; ?>', 'edit');" >
                                                       <?php } ?>
                                            </td>
                                            <td>
                                                <?php if ($row_p->link != '#') { ?>
                                                    <input type="checkbox" class="form-check"
                                                    <?php
                                                    if ($row_p->hapus == 1) {
                                                        echo "checked='checked'";
                                                    }
                                                    ?> <?= $m['rul_edit']; ?> onclick="updateRuleAksi('<?= $row_p->id_menu; ?>', '<?= $row_p->kd_user; ?>', '<?= $row_p->hapus; ?>', 'hapus');">
                                                    </div>
                                                <?php } ?>
                                            </td>
                                            <td></td>
                                            <td>
                                                <button <?= $att; ?> class="btn btn-danger btn-xs btn-flat" onclick="hapusMenu('<?= $kd_user; ?>', '<?= $row_p->id_menu; ?>', 'Hapus Menu')"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } $no++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tambahRoleMenu" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="label_head"></h4>
            </div>
            <form id="aksi_menu" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Pilih Menu</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select class="form-control select2 id_menu" id="id_menu" name="id_menu" required>
                                    <option value="">--Pilih Menu--</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" class="form-control text-center  kd_user" name="kd_user">
                        <input type="hidden" class="form-control text-center  parent" name="parent">
                        <input type="hidden" class="form-control text-center  ket" name="ket">
                        <input type="hidden" class="form-control text-center  url" name="url" value="<?= $url; ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="submitMenu"></div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('#tambahRoleMenu').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var ket = button.data('ket');
        var kd_user = button.data('kd_user');
        var parent = button.data('parent');
        var aksi = button.data('aksi');
        var modal = $(this);
        modal.find('.modal-body input.ket').val(aksi);
        modal.find('.modal-body input.kd_user').val(kd_user);
        modal.find('.modal-body input.parent').val(parent);
        $('#label_head').html('Form Tambah Rule Menu ' + ket);
        $('.submitMenu').html('<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>');
        $('#aksi_menu').attr('action', '<?= site_url('setting/Role_menu/insertRoleMenu'); ?>');
        $.ajax({
            url: '<?= base_url(); ?>setting/Role_menu/pilihMenu/' + parent + '/' + kd_user,
            success: function (data) {
                $('#id_menu').html(data);
            }
        })
    });

    function updateRuleAksi(id_menu, kd_user, status, aksi) {
        var url_form = '<?= site_url('setting/Role_menu/update_rule_aksi'); ?>';
        $.ajax({
            type: 'POST',
            url: url_form,
            data: {id_menu: id_menu, kd_user: kd_user, status: status, aksi: aksi},
            success: function (data) {
                if (data == 'true') {
                    notif_smartAlertSukses('Berhasil Update');
                } else {
                    notif_smartAlertGagal('Gagal Update');
                }
            }
        });
    }

    function insertAll(kd_user, ket) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apakah Anda Yakin Menambahkan Semua Menu Pada User Ini',
            text: "Silahkan Klik Tombol YES Untuk melanjutkan aksi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes ',
            cancelButtonText: 'No',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var url_form = '<?= site_url('setting/Role_menu/insertAllMenu'); ?>';
                $.ajax({
                    type: 'POST',
                    url: url_form,
                    data: {kd_user: kd_user, kd_level: <?=$kd_level;?>},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        )
            }
        })
    }

    function hapusMenu(kd_user, id_menu, ket) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Menu dengan Ket : ' + ket,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var url_form = '<?= site_url('setting/Role_menu/deleteMenu'); ?>';
                $.ajax({
                    type: 'POST',
                    url: url_form,
                    data: {kd_user: kd_user, id_menu: id_menu},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        )
            }
        })
    }
</script>
