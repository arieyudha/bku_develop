<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
    <div class="col-md-5">
        <div class="panel">
            <div class="panel-heading bg-gray">
                <h3 class="panel-title">Data-data Validator</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php
                            $attr = "data-toggle='modal'
                                data-target='#modal-validator' 
                                data-aksi='tambah'";
                            $ket = "Validator";
                            $class = "btn-block";
                            btn_tambah($attr, $ket, $class);
                            ?>
                        </div>
                    </div>
                </div>
                <table class="table table-hover table-bordered tabel_2" width='100%'>
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Jabatan</th>
                            <th>Jumlah</th>
                            <th width="15%"><i class="fa fa-refresh"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($get_dataValidatorJabatan as $row) {
                            ?>
                            <tr>
                                <td class="text-center"><?= $no++; ?></td>
                                <td class=""><?= $row['nama_jabatan']; ?></td>
                                <td class="text-center">
                                    <a href="<?= site_url('setting/set_validator') . '?kd_jabatan=' . $row['kd_jabatan']; ?>" class="btn btn-primary btn-xs btn-block btn-flat"><i class="fa fa-search-plus"></i> View</a>

                                </td>
                                <td class="text-center">
                                    <?php
                                    $attrEdit = "data-toggle='modal'
                                data-target='#modal-validator' 
                                data-kd_jabatan='" . $row['kd_jabatan'] . "' 
                                data-kd_validator='" . $row['kd_validator'] . "' 
                                data-aksi='edit'";
                                    $ketEdit = "";
                                    $classEdit = "btn-xs";
                                    btn_edit($attrEdit, $ketEdit, $classEdit);

                                    $attrHps = 'onclick="hapusValidator(\'' . $row['kd_validator'] . '\', \'' . $row['nama_jabatan'] . '\')"';
                                    $ketHps = "";
                                    $classHps = "btn-xs";
                                    btn_hapus($attrHps, $ketHps, $classHps);
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <?php
        if (!empty($kd_jabatan)) {
            foreach ($get_dataValidatorJabatan as $val_row) {
                if ($kd_jabatan == $val_row['kd_jabatan']) {
                    $kd_validator = $val_row['kd_validator'];
                    $nama_jabatan = $val_row['nama_jabatan'];
                }
            }
            ?>
            <div class="panel">
                <div class="panel-heading bg-gray">
                    <h3 class="panel-title">Data-data Kegiatan pada <?= $nama_jabatan; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <!--<h3 class="alert alert-success"></h3>-->  
                            </div>
                            <div class="col-lg-4">
                                <?php
                                $attr = "data-toggle='modal'
                                data-target='#modal-pptk-validator' 
                                data-kd_valid='$kd_validator' 
                                data-nama_jabatan='$nama_jabatan' 
                                data-kd_jabatan='$kd_jabatan' 
                                data-aksi='tambah'";
                                $ket = "PPTK";
                                $class = "btn-block";
                                btn_tambah($attr, $ket, $class);
                                ?>
                            </div>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered tabel_3" style="width: 100%">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Nama Jabatan PPTK</th>
                                <th><i class="fa fa-refresh"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($get_dataPptkValidator as $row_pptk) {
                                ?>
                                <tr>
                                    <td class="text-center"><?= $no++; ?></td>
                                    <td><?= $row_pptk['nama_jabatan']; ?></td>
                                    <td class="text-center">
                                        <?php
                                        $attr = 'onclick="hapusPptk(\'' . $row_pptk['nama_jabatan'] . '\', \'' . $row_pptk['kd_pptk'] . '\', \'' . $kd_validator . '\')"';
                                        $ket = "";
                                        $class = "";
                                        btn_hapus($attr, $ket, $class);
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        } else {
            $status = "Keterangan Kegiatan";
            $ket = "Pilih Terlebih dahulu PPTK untuk melihat Kegiatan";
            statusWarning($status, $ket);
        }
        ?>
    </div>
</div>
<div class="modal fade" id="modal-validator" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head"></h4>
            </div>
            <form class="form-pptk" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Pilih Pegawai</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select class="form-control select2 data_pegawai" name="data_pegawai" style="width: 100%" required>
                                    <option value="">--Pilih Pegawai--</option>
                                    <?php
                                    foreach ($getDataPegawai as $row_pptk) {
                                        if ($row_pptk->status_pgw == 'PNS') {
                                            ?>
                                            ?>
                                            <option
                                                data-kd_jabatan="<?= $row_pptk->kd_jabatan; ?>"
                                                value="<?= $row_pptk->kd_jabatan; ?>"><?= $row_pptk->nama_jabatan . ' (' . $row_pptk->nama . ')'; ?></option>
                                                <?php
                                            }
                                        }
                                        ?> 
                                </select>
                            </div>
                            <input type="hidden" class="form-control kd_validator" name="kd_validator">
                            <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan">
                            <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-pptk-validator" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head_pptk" id=""></h4>
            </div>
            <form class="form-pptk-validator" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="alert alert-success nama-jabatan"></h4>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-hover table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Nama Kegiatan</th>
                                            <th><i class="fa fa-check"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($get_dataPptk as $row_pptk) {
                                            foreach ($get_pegawaiPptkWhereValidator as $row_pv) {
                                                if ($row_pv->kd_pptk == $row_pptk['kd_pptk']) {
                                                    $attr = 'disabled';
                                                    break;
                                                } else {
                                                    $attr = '';
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $no++; ?></td>
                                                <td><?= $row_pptk['nama_jabatan']; ?></td>
                                                <td class="text-center">
                                                    <input <?= $attr; ?> class="form-check" value='0' id='pilih<?= $row_pptk['kd_pptk']; ?>'
                                                                         onclick="
                                                                                     var pilih = $('#pilih<?= $row_pptk['kd_pptk']; ?>').val();
                                                                                     if (pilih == 0) {
                                                                                         $('#pilih<?= $row_pptk['kd_pptk']; ?>').val(1);
                                                                                         $('.pptk<?= $row_pptk['kd_pptk']; ?>').prop('disabled', false);
                                                                                     } else {
                                                                                         $('#pilih<?= $row_pptk['kd_pptk']; ?>').val(0);
                                                                                         $('.pptk<?= $row_pptk['kd_pptk']; ?>').prop('disabled', true);
                                                                                     }
                                                                         "
                                                                         type="checkbox" name="checkbox-toggle">
                                                    <input disabled type="hidden" class="form-control pptk<?= $row_pptk['kd_pptk']; ?>" name="pptk[]" value="<?= $row_pptk['kd_pptk']; ?>">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" class="form-control kd_validator" name="kd_validator">
                            <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    callBackClassAfter('.data_pegawai', 'cek-validator');
    $('#modal-validator').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var kd_jabatan = button.data('kd_jabatan');
        var kd_validator = button.data('kd_validator');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.kd_jabatan').val('');
            modal.find('.modal-body input.kd_validator').val('');
            modal.find('.modal-body select.data_pegawai').val('');
            $('.label_head').html('Form Tambah Data Validator');
            $('.form-pptk').attr('action', '<?= site_url('setting/Set_validator/insertValidator'); ?>');
        } else {
            modal.find('.modal-body select.data_pegawai').val(kd_jabatan).change();
            modal.find('.modal-body input.kd_jabatan').val(kd_jabatan);
            modal.find('.modal-body input.kd_validator').val(kd_validator);
            $('.label_head').html('Form Edit Data Validator');
            $('.form-pptk').attr('action', '<?= site_url('setting/Set_validator/updateValidator'); ?>');
        }
    });
    $('#modal-pptk-validator').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var nama_jabatan = button.data('nama_jabatan');
        var kd_valid = button.data('kd_valid');
        var modal = $(this);
        console.log(kd_valid);
        $('.nama-jabatan').text(nama_jabatan);
        modal.find('.modal-body input.kd_validator').val(kd_valid);
        $('.label_head_pptk').html('Form Tambah PPTK');
        $('.form-pptk-validator').attr('action', '<?= site_url('setting/Set_validator/insertPptkValidator'); ?>');
    });


    $('.data_pegawai').change(function () {
        const kd_jabatan = $(this).find('option:selected').data('kd_jabatan');
        $('.kd_jabatan').val(kd_jabatan);
        $.ajax({
            type: 'POST',
            url: '<?= site_url('setting/Set_pptk/cekPptk'); ?>',
            data: {kd_jabatan: kd_jabatan},
            success: (result) => {
                if (result.cek == 0) {
                    $('.btn-save').prop('disabled', false);
                    $('.cek-pptk').html('<img src="<?php echo base_url(); ?>/assets/img/true.png"><b style="color:green;"> Data PPTK Valid</b>');
                } else {
                    $('.btn-save').prop('disabled', true);
                    $('.cek-pptk').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Data PPTK Sudah ada</b>');
                }
            },
            error: (e) => {
                console.log(e.responseText);
            }
        })
    });

    function hapusPptk(nama_jabatan, kd_pptk, kd_validator) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus PPTK dangen Nama : ' + nama_jabatan,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('setting/Set_validator/deletePptk'); ?>",
                    data: {kd_pptk: kd_pptk, kd_validator: kd_validator},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }
    function hapusKegiatan(kdkegunit, unitkey, kdtahap, kd_jabatan, nama_keg) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Kegiatan dangen Nama : ' + nama_keg,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('setting/Set_pptk/deleteKegiatanPptk'); ?>",
                    data: {kdtahap: kdtahap, unitkey: unitkey, kdkegunit: kdkegunit, kd_jabatan: kd_jabatan},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        )
            }
        })
    }
</script>