<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
    <div class="col-md-12">
        <!-- DIRECT CHAT -->
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-4">
                        <form  name='fLevel' method='get' >
                            <select class="form-control select2 level_user" style="width: 100%;" name='level_user' onchange='document.fLevel.submit();'>
                                <option value="">.: Pilih Level User :.</option>
                                <?php
                                foreach ($get_levelUser as $lev_row) {
                                    if ($lev_row->kd_level == $kd_level) {
                                        $att = 'selected';
                                    } else {
                                        $att = '';
                                    }
                                    echo '<option ' . $att . ' data-kd_level="' . $lev_row->kd_level . '"  value="' . $lev_row->kd_level . '">' . $lev_row->ket_level . '</option>';
                                }
                                ?>
                            </select>
                        </form>
                    </div>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <?php if (!empty($kd_level)) { ?>
                    <div class="row">
                        <div class="col-md-4">
                            <button onclick="insertMenuLevel('<?= $kd_level; ?>')" class="btn btn-primary btn-block btn-flat"><i class="fa fa-plus"></i> Semua Menu Level</button>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered tabel_3" width='100%'>
                        <thead>
                            <tr>
                                <th width="4%">No</th>
                                <th>Username</th>
                                <th>Nama User</th>
                                <th>Ket Level</th>
                                <th width="4%"><i class="fa fa-refresh"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($get_user as $row) {
                                ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row->username; ?></td>
                                    <td><?= $row->nama_user; ?></td>
                                    <td class="text-center"><?= $row->ket_level; ?></td>
                                    <td class="text-center">
                                        <a href="<?= site_url('setting/Role_menu/view_menu?kd_level=' . $kd_level . '&kd_user=' . $row->kd_user); ?>" class="btn btn-success btn-flat btn-block"><i class="fa fa-search"></i> Menu</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    function insertMenuLevel(kd_level) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apakah Anda Yakin Menambahkan Semua Menu Pada Level User Ini',
            text: "Silahkan Klik Tombol YES Untuk melanjutkan aksi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes ',
            cancelButtonText: 'No',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var url_form = '<?= site_url('setting/Role_menu/insertAllMenuUser'); ?>';
                $.ajax({
                    type: 'POST',
                    url: url_form,
                    data: {kd_level: kd_level},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        )
            }
        })
    }
</script>