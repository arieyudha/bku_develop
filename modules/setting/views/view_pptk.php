<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
    <div class="col-md-5">
        <div class="panel">
            <div class="panel-heading bg-gray">
                <h3 class="panel-title">Data-data PPTK</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php
                            $attr = "data-toggle='modal'
                                data-target='#modal-pptk' 
                                data-aksi='tambah'";
                            $ket = "PPTK";
                            $class = "btn-block";
                            btn_tambah($attr, $ket, $class);
                            ?>
                        </div>
                    </div>
                </div>
                <table class="table table-hover table-bordered tabel_2" width='100%'>
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Jabatan</th>
                            <th>Jumlah</th>
                            <th width="15%"><i class="fa fa-refresh"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($get_dataPptk as $row) {
                            if ($row['jml_keg'] != 0) {
                                $attHps = 'disabled';
                            } else {
                                $attHps = '';
                            }
                            ?>
                            <tr>
                                <td class="text-center"><?= $no++; ?></td>
                                <td class=""><?= $row['nama_jabatan']; ?></td>
                                <td class="text-center">
                                    <a href="<?= site_url('setting/set_pptk') . '?kd_jabatan=' . $row['kd_jabatan']; ?>" class="btn btn-primary btn-xs btn-block btn-flat"><i class="fa fa-search-plus"></i> View</a>
                                    <label class="label label-success"><?= $row['jml_keg']; ?> Kegiatan</label>
                                </td>
                                <td class="text-center">
                                    <?php
                                    $attrEdit = "data-toggle='modal'
                                data-target='#modal-pptk' 
                                data-kd_jabatan='" . $row['kd_jabatan'] . "' 
                                data-kd_pptk='" . $row['kd_pptk'] . "' 
                                data-aksi='edit'";
                                    $ketEdit = "";
                                    $classEdit = "btn-xs";
                                    btn_edit($attrEdit, $ketEdit, $classEdit);

                                    $attrHps = 'onclick="hapusPptk(\'' . $row['kd_pptk'] . '\', \'' . $row['nama_jabatan'] . '\')"';
                                    $ketHps = "";
                                    $classHps = "btn-xs";
                                    btn_hapus($attrHps, $ketHps, $classHps);
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <?php
        if (!empty($kd_jabatan)) {
            foreach ($get_dataPptkJabatan as $pptk_row) {
                $kd_pptk = $pptk_row['kd_pptk'];
                $nama_jabatan = $pptk_row['nama_jabatan'];
            }
            ?>
            <div class="panel">
                <div class="panel-heading bg-gray">
                    <h3 class="panel-title">Data-data Kegiatan pada <?= $nama_jabatan; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <!--<h3 class="alert alert-success"></h3>-->  
                            </div>
                            <div class="col-lg-4">
                                <?php
                                $attr = "data-toggle='modal'
                                data-target='#modal-pptk-kegiatan' 
                                data-kd_pptk='$kd_pptk' 
                                data-nama_jabatan='$nama_jabatan' 
                                data-kd_jabatan='$kd_jabatan' 
                                data-aksi='tambah'";
                                $ket = "Kegiatan";
                                $class = "btn-block";
                                btn_tambah($attr, $ket, $class);
                                ?>
                            </div>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered tabel_3" style="width: 100%">
                        <thead>
                            <tr>
                                <th width="15%">KODE</th>
                                <th>Nama Kegiatan</th>
                                <th><i class="fa fa-refresh"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($get_kegiatanPptk as $row_keg_pptk) {
                                if ($kd_jabatan == $row_keg_pptk->kd_jabatan) {
                                    ?>
                                    <tr>
                                        <td><?= $row_keg_pptk->nuprgrm . $row_keg_pptk->nukeg; ?></td>
                                        <td><?= $row_keg_pptk->nmkegunit; ?></td>
                                        <td class="text-center">
                                            <?php
                                            $attr = 'onclick="hapusKegiatan(\'' . $row_keg_pptk->kdkegunit . '\', \'' . $row_keg_pptk->unitkey . '\', \'' . $row_keg_pptk->kdtahap . '\', \'' . $row_keg_pptk->kd_jabatan . '\', \'' . $row_keg_pptk->nmkegunit . '\')"';
                                            $ket = "";
                                            $class = "";
                                            btn_hapus($attr, $ket, $class);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        } else {
            $status = "Keterangan Kegiatan";
            $ket = "Pilih Terlebih dahulu PPTK untuk melihat Kegiatan";
            statusWarning($status, $ket);
        }
        ?>
    </div>
</div>
<div class="modal fade" id="modal-pptk" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head" id=""></h4>
            </div>
            <form class="form-pptk" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Pilih Pegawai</label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <select class="form-control select2 data_pegawai" name="data_pegawai" style="width: 100%" required>
                                    <option value="">--Pilih Pegawai--</option>
                                    <?php
                                    foreach ($getDataPegawai as $row_pptk) {
                                        if ($row_pptk->status_pgw == 'PNS') {
                                            ?>
                                            ?>
                                            <option
                                                data-kd_jabatan="<?= $row_pptk->kd_jabatan; ?>"
                                                value="<?= $row_pptk->kd_jabatan; ?>"><?= $row_pptk->nama_jabatan . ' (' . $row_pptk->nama . ')'; ?></option>
                                                <?php
                                            }
                                        }
                                        ?> 
                                </select>
                            </div>
                            <input type="hidden" class="form-control kd_pptk" name="kd_pptk">
                            <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan">
                            <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-pptk-kegiatan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head_keg" id=""></h4>
            </div>
            <form class="form-pptk-kegiatan" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="alert alert-success nama-jabatan"></h4>
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-hover table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Nama Kegiatan</th>
                                            <th><i class="fa fa-check"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($get_dataKegiatan as $row_keg) {
                                            $att = '';
                                            foreach ($get_kegiatanPptk as $row_keg_pptk) {
                                                if ($row_keg->kdkegunit == $row_keg_pptk->kdkegunit and $row_keg_pptk->unitkey == $row_keg->unitkey) {
                                                    $att = 'disabled';
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $row_keg->nuprgrm . ' ' . $row_keg->nukeg; ?></td>
                                                <td><?= $row_keg->nmkegunit; ?></td>
                                                <td>
                                                    <input <?= $att; ?> class="form-check" value='0' id='pilih<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>'
                                                                        onclick="
                                                                                    var pilih = $('#pilih<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>').val();
                                                                                    if (pilih == 0) {
                                                                                        $('#pilih<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>').val(1);
                                                                                        $('.kdkegunit<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>').prop('disabled', false);
                                                                                    } else {
                                                                                        $('#pilih<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>').val(0);
                                                                                        $('.kdkegunit<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>').prop('disabled', true);
                                                                                    }
                                                                        "
                                                                        type="checkbox" name="checkbox-toggle">
                                                    <input disabled type="hidden" class="form-control kdkegunit<?= $row_keg->kdkegunit . $row_keg->unitkey; ?>" name="kdkegunit[]" value="<?= $row_keg->kdkegunit; ?>">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" class="form-control kd_pptk_keg" name="kd_pptk">
                            <input type="hidden" class="form-control kd_jabatan_keg" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                            <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    callBackClassAfter('.data_pegawai', 'cek-pptk');
    $('#modal-pptk').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var kd_jabatan = button.data('kd_jabatan');
        var kd_pptk = button.data('kd_pptk');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.kd_jabatan').val('');
            modal.find('.modal-body input.kd_pptk').val('');
            modal.find('.modal-body select.data_pegawai').val('');
            $('.label_head').html('Form Tambah Data PPTK');
            $('.form-pptk').attr('action', '<?= site_url('setting/Set_pptk/insertPptk'); ?>');
        } else {
            modal.find('.modal-body select.data_pegawai').val(kd_jabatan);
            modal.find('.modal-body input.kd_jabatan').val(kd_jabatan);
            modal.find('.modal-body input.kd_pptk').val(kd_pptk);
            $('.label_head').html('Form Edit Data PPTK');
            $('.form-pptk').attr('action', '<?= site_url('setting/Set_pptk/updatePptk'); ?>');
        }
    });
    $('#modal-pptk-kegiatan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var nama_jabatan = button.data('nama_jabatan');
        var kd_pptk_keg = button.data('kd_pptk');
        var modal = $(this);
        $('.nama-jabatan').text(nama_jabatan);
        modal.find('.modal-body input.kd_pptk_keg').val(kd_pptk_keg);
        if (aksi == 'tambah') {
            $('.label_head_keg').html('Form Tambah Kegiatan PPTK');
            $('.form-pptk-kegiatan').attr('action', '<?= site_url('setting/Set_pptk/insertKegiatanPptk'); ?>');
        } else {
            $('.label_head_keg').html('Form Edit Kegiatan PPTK');
            $('.form-pptk-kegiatan').attr('action', '<?= site_url('setting/Set_pptk/updateKegiatanPptk'); ?>');
        }
    });


    $('.data_pegawai').change(function () {
        const kd_jabatan = $(this).find('option:selected').data('kd_jabatan');
        $('.kd_jabatan').val(kd_jabatan);
        $.ajax({
            type: 'POST',
            url: '<?= site_url('setting/Set_pptk/cekPptk'); ?>',
            data: {kd_jabatan: kd_jabatan},
            success: (result) => {
                if (result.cek == 0) {
                    $('.btn-save').prop('disabled', false);
                    $('.cek-pptk').html('<img src="<?php echo base_url(); ?>/assets/img/true.png"><b style="color:green;"> Data PPTK Valid</b>');
                } else {
                    $('.btn-save').prop('disabled', true);
                    $('.cek-pptk').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Data PPTK Sudah ada</b>');
                }
            },
            error: (e) => {
                console.log(e.responseText);
            }
        })
    });

    function hapusPptk(kd_pptk, nama_jabatan) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus PPTK dangen Nama : ' + nama_jabatan,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('setting/Set_pptk/deletePptk'); ?>",
                    data: {kd_pptk: kd_pptk},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        )
            }
        })
    }
    function hapusKegiatan(kdkegunit, unitkey, kdtahap, kd_jabatan, nama_keg) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Kegiatan dangen Nama : ' + nama_keg,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('setting/Set_pptk/deleteKegiatanPptk'); ?>",
                    data: {kdtahap: kdtahap, unitkey: unitkey, kdkegunit: kdkegunit, kd_jabatan: kd_jabatan},
                    cache: false,
                    success: function (response) {
                        notif_smartAlertSukses('Berhasil');
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal')
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        )
            }
        })
    }
</script>