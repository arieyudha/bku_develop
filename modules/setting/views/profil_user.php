<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<style>

</style>
<div class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-body">
                        <?php
                        $foto = $row_user['foto'];
                        $nama_user = $row_user['nama_user'];
                        if ($foto == '') {
                            echo "<img src='" . logoKab() . "' class='profile-user-img img-responsive img-circle' />";
                        } else {
                            echo "<img src='" . base_url() . "assets/img/user/" . $foto . "' class='profile-user-img img-responsive img-circle' />";
                        }
                        ?>
                        <h3 class="profile-username text-center"><?= $nama_user; ?></h3>
                        <hr>
                        <h5>Tombol Aksi</h5>
                        <button class="btn btn-info btn-block" onclick="viewAktifitas('<?= $row_user['kd_user']; ?>')">Lihat Aktifitas</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    <form class="" method="POST" action="<?= site_url('setting/Profil/updateProfilPass'); ?>">
                        <div class="panel-heading bg-green-gradient">
                            Form Update User
                        </div>
                        <div class="panel-body">
                            <h3 class="text-center">Update data Profil</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" name="username" value="<?= $row_user['username']; ?>" type="text" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Password Lama</label>
                                        <input class="form-control password_lama" name="password_lama" value="" type="password">
                                        <input class="form-control pass_lama" name="pass_lama" value="<?= $row_user['password']; ?>" type="hidden">
                                    </div>
                                    <div class="form-group">
                                        <label>Password Baru</label>
                                        <input class="form-control" name="password_baru" value="" type="password">
                                        <input class="form-control" name="url" value="<?= $url; ?>" type="hidden">
                                        <input class="form-control" name="kd_user" value="<?= $row_user['kd_user']; ?>" type="hidden">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button disabled class="btn btn-primary btnSave btn-block"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-heading">
                Data Profil User
            </div>
            <div class="panel-body">

                <div class="col-md-12">
                    <center>
                        <div class="ajax-loader">
                            <img src="<?= base_url(); ?>assets/img/loading1.gif" class="img-responsive" />
                        </div>
                    </center>
                    <div class="viewOnclick">
                        <h4 class="alert alert-info">Klik Tombol Button untuk melihat Aksi</h4>
                    </div>
                    <!-- <h3 class="alert alert-danger">Ini Info</h3> -->
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.password_lama').after('<span class="ketPassLama"></span>').css('margin-right', '10px');
            $('.password_lama').keyup(function () {
                $(this).css({'border': '1px solid #ccc', 'background': 'none'});
            });

            $(".password_lama").change(function () {
                var passLama = $('.password_lama').val();
                var pass_lama = $('.pass_lama').val();
                if (passLama == pass_lama) {
                    $('.btnSave').prop('disabled', false);
                    $('.ketPassLama').html('<img src="<?php echo base_url(); ?>/assets/img/true.png"><b style="color:green;"> Password Diterima</b>');
                } else {
                    $('.btnSave').prop('disabled', true);
                    $('.ketPassLama').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Password Ditolak</b>');
                }
            })
        })

        function viewAktifitas(kd_user) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('setting/Profil/getAktifitas'); ?>",
                data: {
                    kd_user: kd_user
                },
                cache: false,
                beforeSend: function () {
                    $('.ajax-loader').css("visibility", "visible");
                },
                complete: function () {
                    $('.ajax-loader').css("visibility", "hidden");
                },
                success: function (data) {
                    $('.viewOnclick').html(data);
                },
                error: function (response) {
                    notif_smartAlertGagal('Gagal');
                }
            });
        }

        function refresh() {
            window.location.reload()
        }
    </script>