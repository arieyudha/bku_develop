<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_setting extends CI_Model {

    //put your code here
    function getRefPegawai($nip = null) {
        if ($nip == '') {
            $query = $this->db->query("select a.*, b.pangkat, c.nama_jabatan, c.kd_jenis, d.nama_kelamin, e.nama_jenis, e.background from ref_pegawai a 
join ref_pangkat_gol b on a.kd_pangkat_gol=b.golongan 
join ref_jabatan c on c.kd_jabatan=a.kd_jabatan
join ref_kelamin d on d.kd_kelamin=a.jns_kelamin
join ref_jenis_jabatan e on e.kd_jenis=c.kd_jenis order by c.kd_jenis, a.kd_pangkat_gol");
        } else {
            $query = $this->db->query("select a.*, b.pangkat, c.nama_jabatan, c.kd_jenis, d.nama_kelamin, e.nama_jenis, e.background from ref_pegawai a 
join ref_pangkat_gol b on a.kd_pangkat_gol=b.golongan 
join ref_jabatan c on c.kd_jabatan=a.kd_jabatan
join ref_kelamin d on d.kd_kelamin=a.jns_kelamin
join ref_jenis_jabatan e on e.kd_jenis=c.kd_jenis where a.nip='$nip' order by c.kd_jenis, a.kd_pangkat_gol");
        }
        return $query;
    }

//    function getHariKerjaSkpd($tahun, $kd_urusan = null, $kd_bidang = null, $kd_unit = null) {
//        if ($kd_urusan == '' or $kd_bidang = '' or $kd_unit = '') {
//            $query = $this->db->query("select a.*, 
//sum(case when b.bulan=1 then b.hari_kerja else 0 end) jan,
//sum(case when b.bulan=2 then b.hari_kerja else 0 end) feb,
//sum(case when b.bulan=3 then b.hari_kerja else 0 end) mar,
//sum(case when b.bulan=4 then b.hari_kerja else 0 end) apr,
//sum(case when b.bulan=5 then b.hari_kerja else 0 end) mei,
//sum(case when b.bulan=6 then b.hari_kerja else 0 end) jun,
//sum(case when b.bulan=7 then b.hari_kerja else 0 end) jul,
//sum(case when b.bulan=8 then b.hari_kerja else 0 end) agt,
//sum(case when b.bulan=9 then b.hari_kerja else 0 end) sep,
//sum(case when b.bulan=10 then b.hari_kerja else 0 end) okt,
//sum(case when b.bulan=11 then b.hari_kerja else 0 end) nop,
//sum(case when b.bulan=12 then b.hari_kerja else 0 end) des,
//b.tahun
//from ref_unit a 
//left join set_haker_skpd b on a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit and b.tahun=$tahun 
//left join uti_bulan c on c.kd_bulan=b.bulan 
//group by a.kd_urusan, a.kd_bidang, a.kd_unit");
//        } else {
//            $query = $this->db->query("select a.*, b.tahun,
//sum(case when b.bulan=1 then b.hari_kerja else 0 end) jan,
//sum(case when b.bulan=2 then b.hari_kerja else 0 end) feb,
//sum(case when b.bulan=3 then b.hari_kerja else 0 end) mar,
//sum(case when b.bulan=4 then b.hari_kerja else 0 end) apr,
//sum(case when b.bulan=5 then b.hari_kerja else 0 end) mei,
//sum(case when b.bulan=6 then b.hari_kerja else 0 end) jun,
//sum(case when b.bulan=7 then b.hari_kerja else 0 end) jul,
//sum(case when b.bulan=8 then b.hari_kerja else 0 end) agt,
//sum(case when b.bulan=9 then b.hari_kerja else 0 end) sep,
//sum(case when b.bulan=10 then b.hari_kerja else 0 end) okt,
//sum(case when b.bulan=11 then b.hari_kerja else 0 end) nop,
//sum(case when b.bulan=12 then b.hari_kerja else 0 end) des
//from ref_unit a 
//left join set_haker_skpd b on a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit and b.tahun=$tahun 
//left join uti_bulan c on c.kd_bulan=b.bulan where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit group by a.kd_urusan, a.kd_bidang, a.kd_unit");
//        }
//        return $query;
//    }

    function getHariKerjaSkpd() {
        $query = $this->db->query("select a.*, b.hari_kerja from ref_sub_unit a  
            left join set_haker_skpd b on a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit and a.kd_sub=b.kd_sub
            order by a.kd_urusan, a.kd_bidang, a.kd_unit");
        return $query;
    }

    function getKetHariLibur($tahun, $bulan) {
        $query = $this->db->query("select a.* from set_libur_hari a 
join set_libur_tgl b on a.kode=b.kode where a.tahun=$tahun and MONTH(b.tgl)=$bulan
group by a.kode");
        return $query;
    }

    function getTglHariLibur($tahun, $bulan, $kode = null) {
        if ($kode == '') {
            $query = $this->db->query("select b.*, a.tahun, a.keterangan from set_libur_hari a 
join set_libur_tgl b on a.kode=b.kode where a.tahun=$tahun and MONTH(b.tgl)=$bulan");
        } else {
            $query = $this->db->query("select b.*, a.tahun, a.keterangan from set_libur_hari a 
join set_libur_tgl b on a.kode=b.kode where a.tahun=$tahun and MONTH(b.tgl)=$bulan and b.kode='$kode'");
        }
        return $query;
    }
    
    function getJamKerjaSkpd($kd_urusan, $kd_bidang, $kd_unit, $harikerja){
        $query = $this->db->query("SELECT a.kd_hari, a.nm_hari, b.jam_masuk, b.jam_keluar FROM ref_hari a LEFT JOIN set_jamker_skpd b 
ON a.kd_hari = b.kd_hari and b.kd_urusan = $kd_urusan AND b.kd_bidang = $kd_bidang AND b.kd_unit = $kd_unit order by a.kd_hari LIMIT $harikerja");
        return $query;
    }
    function get_refJamKerja(){
        $query = $this->db->query("SELECT * FROM ref_jamkerja");
        return $query;
    }

}
