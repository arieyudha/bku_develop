<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_pptk
 *
 * @author BappedaKalsel
 */
class Model_pptk extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cek_pptk($kd_jabatan) {
        $query = $this->db->query("select count(*) as cek from pptk_pegawai a where a.kd_jabatan='$kd_jabatan'");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_kegiatanPptk($tahun, $kdtahap) {
        $query = $this->db->query("select a.*, b.kd_jabatan, b.kd_pptk, c.nmprgrm, c.nuprgrm, c.unitkey_bidang from sip_kegunit a 
join pptk_kegunit b on a.unitkey=b.unitkey and a.kdtahap=b.kdtahap and a.tahun=b.tahun and a.kdkegunit=b.kdkegunit
join sip_pgrmunit c on a.idprgrm=c.idprgrm and a.tahun=c.tahun and a.unitkey=c.unitkey and a.kdtahap=c.kdtahap
where a.tahun=$tahun and a.kdtahap=$kdtahap order by c.nuprgrm, a.nukeg");
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    public function get_pegawaiPptkWhereValidator($kd_jabatan) {
        $query = $this->db->query("select a.*, b.kd_pptk, c.kd_jabatan as kd_jabatan_pptk from validator_pegawai a 
join validator_group b on a.kd_validator=b.kd_validator
join pptk_pegawai c on c.kd_pptk=b.kd_pptk
where a.kd_jabatan=$kd_jabatan");
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    public function get_pptkPegawai($kd_jabatan = null) {
        if ($kd_jabatan == '') {
            $query = $this->db->query("select a.*, (select count(*) from pptk_kegunit b where b.kd_jabatan=a.kd_jabatan) as jml_keg 
                from pptk_pegawai a");
        } else {
            $query = $this->db->query("select a.*, (select count(*) from pptk_kegunit b where b.kd_jabatan=a.kd_jabatan) as jml_keg 
                from pptk_pegawai a where a.kd_jabatan='$kd_jabatan'");
        }
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    public function get_validatorPegawai($kd_jabatan = null) {
        if ($kd_jabatan == '') {
            $query = $this->db->query("select a.* from validator_pegawai a");
        } else {
            $query = $this->db->query("select a.* from validator_pegawai a where a.kd_jabatan='$kd_jabatan'");
        }
        return $query;
    }

}
