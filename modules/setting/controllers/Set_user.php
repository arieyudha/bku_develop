<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Set_user extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('login/Login');
        } else {
            $this->load->model(['Model_setting']);
        }
    }

    public function index() {
        $record = $this->javasc_back();
//        $kd_level = isset($_REQUEST['level_user']) ? $_REQUEST['level_user'] : '';
        $record['kd_user_new'] = $this->Model_Auth->maxUser();
        $kd_level = isset($_REQUEST['level_user']) ? $_REQUEST['level_user'] : '';
        $record['kd_level'] = $kd_level;
        $record['get_levelUser'] = $this->Model_Auth->get_levelUser()->result();
        $record['get_user'] = $this->Model_Auth->get_userWhereLevel($kd_level)->result();
        $record['get_dataPptk'] = $this->get_dataPptk($this->a['kd_level']);
//        $record['getDataPegawai'] = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        $data = $this->layout_back('view_user', $record);
        $data['ribbon_left'] = ribbon_left('Setting', 'User');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    public function getUsername() {
        $username = $this->post('username');
        $cek = $this->Model_Auth->cekUsername($username);
        return jsonArray($cek);
    }

    function insertUser() {
        $url = $this->input->post('url');
        $kd_level = $this->input->post('kd_level', true);
        $kd_user = $this->input->post('kd_user', true);
        $password = $this->input->post('password', true);
        $row_lvl = $this->Model_Auth->get_levelUser($kd_level)->row();
        $data['username'] = $this->input->post('username', true);
        $data['password'] = enkripsiText($password);
        $data['nama_user'] = $this->input->post('nama_user', true);
        $data['kd_level'] = $kd_level;
        $data['ket_level'] = $row_lvl->ket_level;
        $data['kd_user'] = $kd_user;
        $que = $this->insert_duplicate('user', $data);
        $ket = 'Menambah User : ' . $this->input->post('username', true);
        if ($que) {
            if ($data['kd_level'] == 1 or $data['kd_level'] == 3) {
                if ($data['kd_level'] == 1) {
                    $ket = 'admin';
                } else {
                    $ket = 'validator';
                }
                $menu = $this->Model_Auth->get_menuWhereField($ket);
            } elseif ($data['kd_level'] == 2) {
                $menu = $this->Model_Auth->get_menuWhereField('pptk');
                $data1['kd_user'] = $kd_user;
                $data1['kd_level'] = $kd_level;
                $data1['kd_jabatan'] = $this->post('kd_jabatan');
                $this->insert_duplicate('user_group', $data1);
            }
            foreach ($menu as $m) {
                $data2['kd_user'] = $data['kd_user'];
                $data2['id_menu'] = $m->id;
                $this->insert_duplicate('menu_role', $data2);
            }
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function resetPassword() {
        
    }

    function updateUser() {
        $url = $this->input->post('url');
        $kd_user = $this->post('kd_user');
        $password = $this->post('password');
        $data['username'] = $this->post('username');
        $data['password'] = enkripsiText($password);
        $data['nama_user'] = $this->post('nama_user');
        $data['kd_user'] = $kd_user;
        $query = $this->update('kd_user', $kd_user, 'user', $data);
        $ket = 'Update Data User : ' . $this->input->post('username', true);
        if ($query) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function kunciUser() {
        $col['kd_user'] = $this->input->post('kd_user');
        $col['kd_level'] = $this->input->post('kd_level');
        $iskunci = $this->input->post('iskunci');

        if ($iskunci == 1) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }
        $q = $this->update_where('user', $data, $col);
        if ($q) {
            echo 'true';
        }
    }

    function deleteUser() {
        $col['kd_user'] = $this->input->post('kd_user', true);
        $col['kd_level'] = $this->input->post('kd_level', true);
        $this->delete_where('user', $col);
        $this->delete_where('user_group', $col);
        $ket = 'Menghapus Data User dengan Kode : ' . $this->input->post('kd_user', true) . ' dan Level ' . $this->input->post('kd_level', true);
        $this->aktifitas($ket);
    }

}
