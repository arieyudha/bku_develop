<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role_menu
 *
 * @author Yusda
 */
class Role_menu extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('login/Login');
        } else {
            $this->load->model('Model_setting');
        }
    }

    function index() {
        $kd_level = isset($_REQUEST['level_user']) ? $_REQUEST['level_user'] : '';

        $record = $this->javasc_back();
        $record['get_levelUser'] = $this->Model_Auth->get_levelUser()->result();
        $record['get_user'] = $this->Model_Auth->get_userWhereLevel($kd_level)->result();
        $record['kd_level'] = $kd_level;
        //data
        $data = $this->layout_back('list_user', $record);
        $data['ribbon_left'] = ribbon_left('Setting', 'User');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
        // $this->load->view('admin_dashboard_halaman');
    }

    function view_menu() {
        $kd_level = $this->input->get('kd_level');
        $kd_user = $this->input->get('kd_user');
        $record = $this->javasc_back();
        $record['get_levelUser'] = $this->Model_Auth->get_levelUser()->result();
        $record['get_user'] = $this->Model_Auth->get_userWhereLevel($kd_level)->result();
        $record['kd_level'] = $kd_level;
        $record['kd_user'] = $kd_user;
        $record['get_ruleMenu'] = $this->Model_Auth->get_ruleMenu($kd_user)->result();
        $record['get_menu'] = $this->Model_Auth->get_menu();
        //data
        $data = $this->layout_back('view_menu', $record);
        $data['ribbon_left'] = ribbon_left('Setting', 'User');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function update_rule_aksi() {
        $aksi = $this->input->post('aksi');
        $status = $this->input->post('status');
        $id_menu = $this->input->post('id_menu');
        $kd_user = $this->input->post('kd_user');
        if ($status == 0) {
            $st = 1;
        } elseif ($status == 1) {
            $st = 0;
        }
        if ($aksi == 'lihat') {
            $data['lihat'] = $st;
        } elseif ($aksi == 'tambah') {
            $data['tambah'] = $st;
        } elseif ($aksi == 'edit') {
            $data['edit'] = $st;
        } elseif ($aksi == 'hapus') {
            $data['hapus'] = $st;
        } elseif ($aksi == 'print') {
            $data['print'] = $st;
        }
        $id_colum['id_menu'] = $id_menu;
        $id_colum['kd_user'] = $kd_user;
        $query = $this->update_where('menu_role', $data, $id_colum);
        if ($query) {
            echo "true";
        } else {
            echo "false";
        }
    }

    function pilihMenu($parent, $kd_user) {
        $get_menu = $this->Model_Auth->get_menu();
        $get_ruleMenu = $this->Model_Auth->get_ruleMenu($kd_user);
        $data = "<option value=''>Pilih Menu<option>";
        foreach ($get_menu->result() as $row_m) {
            if ($row_m->parent == $parent) {
                foreach ($get_ruleMenu->result() as $row) {
                    if ($row->id_menu == $row_m->id and $row->kd_user == $kd_user) {
                        $att = 'disabled';
                        break;
                    } else {
                        $att = '';
                    }
                }
                $data .= "<option $att value='$row_m->id'>$row_m->nama </option>";
            }
        }
        echo $data;
    }

    function insertAllMenuUser() {
        $kd_level = $this->input->post('kd_level');
        if ($kd_level == 1) {
            $menu = $this->Model_Auth->get_menuWhereField('admin');
        } elseif ($kd_level == 2) {
            $menu = $this->Model_Auth->get_menuWhereField('pptk');
        } elseif ($kd_level == 3) {
            $menu = $this->Model_Auth->get_menuWhereField('validator');
        }

        $get_user = $this->Model_Auth->get_userWhereLevel($kd_level)->result();
        foreach ($get_user as $us) {
            foreach ($menu as $m) {
                $data2['kd_user'] = $us->kd_user;
                $data2['id_menu'] = $m->id;
                $this->insert_duplicate('menu_role', $data2);
            }
        }

        $ket = 'Menambah Semua Role Menu : ' . $kd_level;
        echo 'true';
    }

    function insertAllMenu() {
        $kd_user = $this->input->post('kd_user');
        $kd_level = $this->input->post('kd_level');
        if ($kd_level == 1) {
            $menu = $this->Model_Auth->get_menuWhereField('admin');
        } elseif ($kd_level == 2) {
            $menu = $this->Model_Auth->get_menuWhereField('skpd');
        } elseif ($kd_level == 3) {
            $menu = $this->Model_Auth->get_menuWhereField('pegawai');
        } elseif ($kd_level == 4) {
            $menu = $this->Model_Auth->get_menuWhereField('sub_skpd');
        }
        foreach ($menu as $m) {
            $data2['kd_user'] = $kd_user;
            $data2['id_menu'] = $m->id;
            $this->insert_duplicate('menu_role', $data2);
        }

        $ket = 'Menambah Semua Role Menu : ' . $kd_user;
        echo 'true';
    }

    function insertRoleMenu() {
        $url = $this->input->post('url');
        $kd_user = $this->input->post('kd_user');
        $id_menu = $this->input->post('id_menu');
        $data['kd_user'] = $kd_user;
        $data['id_menu'] = $id_menu;
        $q = $this->insert_duplicate('menu_role', $data);
        $ket = 'Menambah Role Menu : ' . $id_menu . ' - ' . $kd_user;
        if ($q) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteMenu() {
        $kd_user = $this->input->post('kd_user');
        $id_menu = $this->input->post('id_menu');
        $data['id_menu'] = $id_menu;
        $data['kd_user'] = $kd_user;
        $q = $this->delete_where('menu_role', $data);
        if ($q) {
            $ket = 'Menghapus Role Menu : ' . $id_menu . ' - ' . $kd_user;
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
