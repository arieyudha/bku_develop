<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('utility/Model_utility');
            $this->load->model('Model_setting');
        }
    }

    public function index() {
        $record = $this->javasc_back();
        $record['row_user'] = $this->Model_Auth->get_user($this->a['kd_user'])->row_array();
        $data = $this->layout_back('profil_user', $record);
        $data['ribbon_left'] = ribbon_left('Profil', 'User');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    public function getAktifitas() {
        $kd_user = $this->input->post('kd_user');
        $data['kd_user'] = $kd_user;
        $record['aktifitas'] = $this->Model_Auth->aktifitas($data)->result_array();
        echo $this->load->view('setting/ajax/aktifitas_user', $record);
    }

    public function updateProfilPass() {
        $url = $this->input->post('url');
        $kd_user = $this->input->post('kd_user', true);
        $data['password'] = $this->input->post('password_baru', true);
        $query = $this->update('kd_user', $kd_user, 'user', $data);
        $ket = 'Update Password User';
        if ($query) {
            $this->aktifitas($ket);
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

}
