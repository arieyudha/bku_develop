<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Set_pptk
 *
 * @author BappedaKalsel
 */
class Set_pptk extends MY_Controller {

    //put your code here
    var $a;
    var $kdtahap;
    var $unitkey;

    public function __construct() {
        parent::__construct();
        $this->kdtahap = $this->getKdTahap()->kdtahap;
        $this->unitkey = $this->getUnitkey();
        if (!$this->a) {
            redirect('login/Login');
        } else {
            $this->load->model(['Model_setting', 'Model_pptk', 'master/Model_ref']);
        }
    }

    public function index() {
        $record = $this->javasc_back();
        $kd_jabatan = isset($_REQUEST['kd_jabatan']) ? $_REQUEST['kd_jabatan'] : '';
        $record['kd_jabatan'] = $kd_jabatan;
        $record['getDataPegawai'] = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        $record['get_dataPptk'] = $this->get_dataPptk($this->a['kd_level'], $kd_jabatan);
        if (!empty($kd_jabatan)) {
            
            $record['get_dataPptkJabatan'] = $this->get_dataPptk($this->a['kd_level'],$kd_jabatan);
            $record['get_dataKegiatan'] = $this->Model_ref->get_dataKegiatan($this->a['tahun'], $this->kdtahap)->result();
            $record['get_kegiatanPptk'] = $this->Model_pptk->get_kegiatanPptk($this->a['tahun'], $this->kdtahap)->result();
        }
        $data = $this->layout_back('view_pptk', $record);
        $data['ribbon_left'] = ribbon_left('Setting', 'User');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function insertPptk() {
        $url = $this->input->post('url');
        $data['kd_jabatan'] = $this->post('kd_jabatan');
        $que = $this->insert('pptk_pegawai', $data);
        $ket = 'Menambah Data PPTK dengan Kd Jabatan ' . $data['kd_jabatan'];
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function insertKegiatanPptk() {
        $url = $this->input->post('url');
        $kdkegunit = $this->post('kdkegunit[]');
        $count = count($kdkegunit);
        for ($i = 0; $i < $count; $i++) {
            $data['kdkegunit'] = $kdkegunit[$i];
            $data['unitkey'] = $this->unitkey;
            $data['tahun'] = $this->a['tahun'];
            $data['kdtahap'] = $this->kdtahap;
            $data['kd_pptk'] = $this->post('kd_pptk');
            $data['kd_jabatan'] = $this->post('kd_jabatan');
            $que = $this->insert_duplicate('pptk_kegunit', $data);
        }
        $ket = 'Menambah Data Kegiatan PPTK';
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteKegiatanPptk() {
        $id_colum['kdkegunit'] = $this->post('kdkegunit');
        $id_colum['kdtahap'] = $this->post('kdtahap');
        $id_colum['unitkey'] = $this->post('unitkey');
        $id_colum['kd_jabatan'] = $this->post('kd_jabatan');
        $id_colum['tahun'] = $this->a['tahun'];
        $query = $this->delete_where('pptk_kegunit', $id_colum);
        $ket = 'Menghapus data Kegiatan dengan kegiatan ' . $id_colum['kegunit'] . ' dan Jabatan ' . $id_colum['kd_jabatan'];
        if ($query) {
            $this->aktifitas($ket);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function deletePptk() {
        $kd_pptk = $this->post('kd_pptk');
        $query = $this->delete('kd_pptk', $kd_pptk, 'pptk_pegawai');
        $ket = 'Menghapus data PPTK dengan Kode ' . $kd_pptk;
        if ($query) {
            $this->aktifitas($ket);
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function cekPptk() {
        $kd_jabatan = $this->post('kd_jabatan');
        $cekPptk = $this->Model_pptk->cek_pptk($kd_jabatan);
        return jsonArray($cekPptk);
    }

}
