<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                Integrasi Data
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikUnitKerja()">
                                        <i class="fa fa-exchange"></i> Tarik Data Unit</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikMPrgrm()">
                                        <i class="fa fa-exchange"></i> Tarik Data Program</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikMKegiatan()">
                                        <i class="fa fa-exchange"></i> Tarik Data Kegiatan</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikKinkeg()">
                                        <i class="fa fa-exchange"></i> Tarik Data Kinerja Kegiatan</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikMatangR()">
                                        <i class="fa fa-exchange"></i> Tarik Data Kode Rekening Belanja</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="loader"></div>
                        <div class="progress progress-lg active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" id="bar"role="progressbar" >
                                <span class="sr-only"></span>
                            </div>
                        </div>
                        <p class="textProgres"></p>
                        <p class="load_angka"></p>
                        <p class="textClick"></p>
                        <p class="persenLoad"></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    var auth_basic = {user: "admin", password: "bappedakalsel"};
    function tarikUnitKerja() {
        var no = 0;
        var nox = 1;
        $.ajax({
            url: '<?= urlApi('Api_master/getDaftUnit'); ?>',
            data: auth_basic,
            success: (result) => {
                result.forEach(daft => {
                    $.ajax({
                        type: 'POST',
                        url: '<?= site_url('integrasi/Tarik_master/insertUnitkerja'); ?>',
                        data: {
                            unitkey: daft.UNITKEY,
                            kdlevel: daft.KDLEVEL,
                            kdunit: daft.KDUNIT,
                            nmunit: daft.NMUNIT,
                            akrounit: daft.AKROUNIT,
                            alamat: daft.ALAMAT,
                            telepon: daft.TELEPON,
                            type: daft.TYPE
                        },
                        success: function (data) {
                            var max = result.length;
                            $('.load_angka').text(++no + ' / ' + max + ' Data');
                            var persen = nox++ / max * 100;
                            var persenx = parseInt(persen).toFixed(0);
                            $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                            $('.textProgres').html('');
                            $('.persenLoad').text(persenx + ' %');
                            if (persenx == 100) {
                                $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                            }
                        },
                        error: (e) => {
                            console.log(e.responseText);
                        }
                    });
                });
            }
        });
    }
    function tarikMatangR() {
        var no = 0;
        var nox = 1;
        $.ajax({
            url: '<?= urlApi('Api_master/getMatang'); ?>',
            data: auth_basic,
            success: (result) => {
                result.forEach(matang => {
                    $.ajax({
                        type: 'POST',
                        url: '<?= site_url('integrasi/Tarik_master/insertMatang'); ?>',
                        data: {
                            mtgkey: matang.MTGKEY,
                            kdper: matang.KDPER,
                            nmper: matang.NMPER,
                            mtglevel: matang.MTGLEVEL,
                            kdkhusus: matang.KDKHUSUS,
                            type: matang.TYPE
                        },
                        success: function (data) {
                            var max = result.length;
                            $('.load_angka').text(++no + ' / ' + max + ' Data');
                            var persen = nox++ / max * 100;
                            var persenx = parseInt(persen).toFixed(0);
                            $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                            $('.textProgres').html('');
                            $('.persenLoad').text(persenx + ' %');
                            if (persenx == 100) {
                                $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                            }
                        },
                        error: (e) => {
                            console.log(e.responseText);
                        }
                    });
                });
            }
        });
    }
    function tarikMPrgrm() {
        var no = 0;
        var nox = 1;
        $.ajax({
            url: '<?= urlApi('Api_master/getProgKeg'); ?>',
            data: auth_basic,
            success: (result) => {
                var dataProg = result.program;
                dataProg.forEach(prog => {
                    $.ajax({
                        type: 'POST',
                        url: '<?= site_url('integrasi/Tarik_master/insertProgram'); ?>',
                        data: {
                            kdtahap: prog.kdtahap,
                            idprgrm: prog.idprgrm,
                            nmprgrm: prog.nmprgrm,
                            nuprgrm: prog.nuprgrm,
                            unitkey_bidang: prog.unitkey_bidang
                        },
                        success: function (data) {
                            var max = dataProg.length;
                            $('.load_angka').text(++no + ' / ' + max + ' Data');
                            var persen = nox++ / max * 100;
                            var persenx = parseInt(persen).toFixed(0);
                            $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                            $('.textProgres').html('');
                            $('.persenLoad').text(persenx + ' %');
                            if (persenx == 100) {
                                $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                            }
                        },
                        error: (e) => {
                            console.log(e.responseText);
                        }
                    });
                });
            }
        });
    }
    function tarikMKegiatan() {
        var no = 0;
        var nox = 1;
        $.ajax({
            url: '<?= urlApi('Api_master/getProgKeg'); ?>',
            data: auth_basic,
            success: (result) => {
                var dataProg = result.program;
                dataProg.forEach(prog => {

                    var dataKeg = prog.kegiatan;
                    dataKeg.forEach(keg => {
                        console.log(keg);
                        $.ajax({
                            type: 'POST',
                            url: '<?= site_url('integrasi/Tarik_master/insertKegiatan'); ?>',
                            data: {
                                kdtahap: keg.kdtahap,
                                unitkey: keg.unitkey,
                                kdkegunit: keg.kdkegunit,
                                idprgrm: keg.idprgrm,
                                noprior: keg.noprior,
                                kdsifat: keg.kdsifat,
                                nip: keg.nip,
                                tglakhir: keg.tglakhir,
                                tglawal: keg.tglawal,
                                targetp: keg.targetp,
                                lokasi: keg.lokasi,
                                jumlahmin1: keg.jumlahmin1,
                                pagu: keg.pagu,
                                jumlahpls1: keg.jumlahpls1,
                                sasaran: keg.sasaran,
                                ketkeg: keg.ketkeg,
                                nmkegunit: keg.nmkegunit,
                                nukeg: keg.nukeg
                            },
                            success: function (data) {
                                var max = dataKeg.length;
                                $('.load_angka').text(++no + ' / ' + max + ' Data');
                                var persen = nox++ / max * 100;
                                var persenx = parseInt(persen).toFixed(0);
                                $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                                $('.textProgres').html('');
                                $('.persenLoad').text(persenx + ' %');
                                if (persenx == 100) {
                                    $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                                }
                            }
                        });
                    });
                });
            },
            error: (e) => {
                console.log(e.responseText);
            }
        });
    }
    function tarikKinkeg() {
        $.ajax({
            url: '<?= urlApi('Api_master/getProgKeg'); ?>',
            data: auth_basic,
            success: (result) => {
                var dataProg = result.program;
                dataProg.forEach(prog => {
                    var dataKeg = prog.kegiatan;
                    dataKeg.forEach(keg => {
                        var no = 0;
                        var nox = 1;
                        var dataKinkeg = keg.kinkeg;
                        dataKinkeg.forEach(kinkeg => {
                            $.ajax({
                                type: 'POST',
                                url: '<?= site_url('integrasi/Tarik_master/insertKinkeg'); ?>',
                                data: {
                                    kdtahap: kinkeg.kdtahap,
                                    unitkey: kinkeg.unitkey,
                                    kdkegunit: kinkeg.kdkegunit,
                                    kdjkk: kinkeg.kdjkk,
                                    tolokur: kinkeg.tolokur,
                                    target: kinkeg.target,
                                    urjkk: kinkeg.urjkk
                                },
                                success: function (data) {
                                    var max = dataKinkeg.length;
                                    $('.load_angka').text(++no + ' / ' + max + ' Data');
                                    var persen = nox++ / max * 100;
                                    var persenx = parseInt(persen).toFixed(0);
                                    $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                                    $('.textProgres').html('');
                                    $('.persenLoad').text(persenx + ' %');
                                    if (persenx == 100) {
                                        $('.textProgres').html('<i class="fa fa-check"></i> Selesai');
                                    }
                                }
                            });
                        });
                    });
                });
            },
            error: (e) => {
                console.log(e.responseText);
            }
        });
    }
//    
//    result.forEach(prog => {
//                    
//                });

</script>