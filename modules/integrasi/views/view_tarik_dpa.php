<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                Integrasi Data
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikDpaR()">
                                        <i class="fa fa-exchange"></i> Tarik Data DPA Rincian <?= $a['tahun']; ?></button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-flat btn-block" onclick="tarikDpaDetR()">
                                        <i class="fa fa-exchange"></i> Tarik Data DPA Detail Rincian <?= $a['tahun']; ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="loader"></div>
                        <div class="progress progress-lg active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" id="bar"role="progressbar" >
                                <span class="sr-only"></span>
                            </div>
                        </div>
                        <p class="textProgres"></p>
                        <p class="load_angka"></p>
                        <p class="textClick"></p>
                        <p class="persenLoad"></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    var auth_basic = {user: "admin", password: "bappedakalsel"};
    function tarikDpaR() {
        var no = 0;
        var nox = 1;
        $.getJSON('<?= urlApi('Api_dpa/getDpaR'); ?>', auth_basic).done(function (result) {
            $.each(result, function (i, field) {
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url('integrasi/Tarik_dpa/insertDpaR'); ?>',
                    data: {
                        kdkegunit: field.KDKEGUNIT,
                        mtgkey: field.MTGKEY,
                        unitkey: field.UNITKEY,
                        idxdask: field.IDXDASK,
                        kdtahap: field.KDTAHAP,
                        nilai: field.NILAI
                    },
                    success: function (data) {
                        console.log(result);
                        var max = result.length;
                        $('.load_angka').text(++no + ' / ' + max + ' Data');
                        var persen = nox++ / max * 100;
                        var persenx = parseInt(persen).toFixed(0);
                        $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                        $('.textProgres').html('');
                        $('.persenLoad').text(persenx + ' %');
                        if (persenx == 100) {
                            $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                        }
                    }
                });
            });
        });
    }
    function tarikDpaDetR() {
        var no = 0;
        var nox = 1;
        $.getJSON('<?= urlApi('Api_dpa/getDpaDetR'); ?>', auth_basic).done(function (result) {
            $.each(result, function (i, field) {
                $.ajax({
                    type: 'POST',
                    url: '<?= site_url('integrasi/Tarik_dpa/insertDpaDetR'); ?>',
                    data: {
                        unitkey: field.UNITKEY,
                        kdkegunit: field.KDKEGUNIT,
                        mtgkey: field.MTGKEY,
                        idxdask: field.IDXDASK,
                        kdnilai: field.KDNILAI,
                        kdjabar: field.KDJABAR,
                        uraian: field.URAIAN,
                        jumbyek: field.JUMBYEK,
                        satuan: field.SATUAN,
                        tarif: field.TARIF,
                        subtotal: field.SUBTOTAL,
                        ekspresi: field.EKSPRESI,
                        inclsubtotal: field.INCLSUBTOTAL,
                        type: field.TYPE,
                        idstdharga: field.IDSTDHARGA,
                        kddana: field.KDDANA,
                        kdtahap: field.KDTAHAP
                    },
                    success: function (data) {
                        console.log(result);
                        var max = result.length;
                        $('.load_angka').text(++no + ' / ' + max + ' Data');
                        var persen = nox++ / max * 100;
                        var persenx = parseInt(persen).toFixed(0);
                        $('.progress-bar').css('width', persenx + '%').attr('aria-valuenow', persenx);
                        $('.textProgres').html('');
                        $('.persenLoad').text(persenx + ' %');
                        if (persenx == 100) {
                            $('.textProgres').html('<i class="fa fa-check"></i> Selesai');

                        }
                    }
                });
            });
        });
    }
</script>