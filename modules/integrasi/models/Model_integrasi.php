<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_database extends CI_Model {

    //put your code here

    function getPegawaiUnit($kd_urusan, $kd_bidang, $kd_unit, $kd_sub, $nip = null) {
        if ($nip == '') {
            $query = $this->db->query("select a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, 
            f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
left join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
left join ref_eselon g on g.eselon=h.eselon
where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit and a.kd_sub='$kd_sub' group by nip  order by g.eselon asc, a.golongan desc");
        } else {
            $query = $this->db->query("select a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, 
            f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit and a.kd_sub=$kd_sub and a.nip='$nip' group by nip order by g.eselon asc, a.golongan desc");
        }
        return $query;
    }

    function getPegawaiSubUnit($kd_urusan, $kd_bidang, $kd_unit, $kd_sub) {
        $query = $this->db->query("select a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, 
            f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit and a.kd_sub=$kd_sub 
group by a.nip order by g.eselon asc, a.golongan desc");
        return $query;
    }

    function getPegawaiSubUnitWherePin($pin) {
        $query = $this->db->query("select a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, 
            f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where a.pin=$pin group by a.nip order by g.eselon asc, a.golongan desc");
        return $query;
    }

    function getPegawaiUnitLikeNip($nip) {
        $query = $this->db->query("select a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan 
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where a.nip like '%%" . $nip . "%%'");
        return $query;
    }

    function getPegawaiUnitWhereNip($nip) {
        $query = $this->db->query("select count(a.nip) as cek, a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan 
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where a.nip='$nip'");
        return $query;
    }

    function getPegawaiUnitWhereGenNip($nip) {
        $query = $this->db->query("select count(a.nip) as cek, a.*, b.nama_jabatan, c.pangkat, d.nama as nm_agama, e.nama as nm_pendidikan, f.nama as stts_menikah, g.jenis, h.eselon, h.parent_jabatan,
b.job_value, b.kelangkaan_profesi, b.objektifitas, b.kelas_jabatan 
from data_pegawai a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
join ref_pangkat_gol c on c.golongan=a.golongan 
join uti_agama d on d.id=a.agama 
join uti_pendidikan e on e.id=a.pendidikan 
join uti_status f on f.id=a.status_menikah 
join data_peta_jabatan h on h.kd_jabatan=a.kd_jabatan
join ref_eselon g on g.eselon=h.eselon
where md5(a.nip)='$nip'");
        return $query;
    }

    function getPetaJabatan($kd_urusan, $kd_bidang, $kd_unit) {
        $query = $this->db->query("select a.*, b.nama_jabatan, c.jenis, d.nama_jabatan as parent from data_peta_jabatan a 
join ref_peta_jabatan b on a.kd_jabatan=b.kd_jabatan and a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit
join ref_eselon c on c.eselon=a.eselon 
left join ref_peta_jabatan d on d.kd_jabatan=a.parent_jabatan and a.kd_urusan=d.kd_urusan and a.kd_bidang=d.kd_bidang and a.kd_unit=d.kd_unit
where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit order by a.eselon");
        return $query;
    }

    function getPangkatGol($data = null) {
        if ($data == '') {
            $query = $this->db->order_by('golongan', 'asc')
                    ->get("ref_pangkat_gol");
        } else {
            $query = $this->db->get_where("ref_pangkat_gol", $data);
        }
        return $query;
    }

    function getJenisJabatan($data = null) {
        if ($data == '') {
            $query = $this->db->order_by('eselon', 'asc')
                    ->get("ref_eselon");
        } else {
            $query = $this->db->get_where("ref_eselon", $data);
        }
        return $query;
    }

    function getDataPlt($kd_urusan, $kd_bidang, $kd_unit) {
        $query = $this->db->query("SELECT a.kd_plt, a.kd_jabatan, a.nip, b.nama_lengkap, c.nama_jabatan, d.nm_unit, b.kd_urusan, b.kd_bidang, b.kd_unit, b.kd_sub, a.tanggal_penetapan  FROM data_plt a JOIN data_pegawai b ON b.nip = a.nip
JOIN ref_peta_jabatan c ON a.kd_jabatan = c.kd_jabatan
JOIN ref_unit d ON d.kd_urusan = c.kd_urusan AND d.kd_bidang = c.kd_bidang AND d.kd_unit=c.kd_unit where c.kd_urusan='$kd_urusan' and c.kd_bidang='$kd_bidang' and c.kd_unit='$kd_unit'");
        return $query;
    }

    function getJabatan($kd_urusan, $kd_bidang, $kd_unit) {
        $query = $this->db->query("select * from ref_peta_jabatan a where a.kd_urusan = '$kd_urusan' and a.kd_bidang='$kd_bidang' and a.kd_unit='$kd_unit'");
        return $query;
    }

    function getPegawai($kd_urusan, $kd_bidang, $kd_unit, $kd_sub) {
        $query = $this->db->query("select * from data_pegawai a where a.kd_urusan = '$kd_urusan' and a.kd_bidang='$kd_bidang' and a.kd_unit='$kd_unit' and a.kd_sub='$kd_sub'");
        return $query;
    }

    function getCariPegawai($kata, $kode) {
        $sql = "";
        if ($kode == 'nip') {
            $sql = "WHERE a.nip like '%$kata%'";
        } elseif ($kode == 'nama') {
            $sql = "WHERE a.nama_lengkap like '%$kata%'";
        } elseif ($kode == 'jabatan') {
            $sql = "WHERE b.nama_jabatan like '%$kata%'";
        }

        $query = $this->db->query("SELECT a.nip, a.gelar_dpn, a.gelar_blkng, a.nama_lengkap, b.nama_jabatan, c.nm_sub_unit, d.eselon FROM data_pegawai a 
JOIN ref_sub_unit c ON c.kd_urusan = a.kd_urusan
AND c.kd_bidang = a.kd_bidang AND c.kd_unit = a.kd_unit AND c.kd_sub = a.kd_sub
 LEFT JOIN  ref_peta_jabatan b ON a.kd_jabatan = b.kd_jabatan
 LEFT JOIN data_peta_jabatan d ON d.kd_peta = a.kd_peta
" . $sql);
        return $query;
    }

}
