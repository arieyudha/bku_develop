<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tarik_dpa extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('login/Login');
        } else {
//            $this->load->model('setting/Model_set');
        }
    }

    public function index() {
        $record = $this->javasc_back();
        $record['a'] = $this->a;
        //Data
        $data = $this->layout_back('view_tarik_dpa', $record);
        $data['ribbon_left'] = ribbon_left('Dashboard', 'Control Panel');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    public function insertDpaR() {
        $data['kdtahap'] = $this->post('kdtahap');
        $data['mtgkey'] = $this->post('mtgkey');
        $data['unitkey'] = $this->post('unitkey');
        $data['idxdask'] = $this->post('idxdask');
        $data['nilai'] = $this->post('nilai');
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['tahun'] = $this->a['tahun'];
        $query = $this->insert_duplicate('sip_daskr', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function insertDpaDetR() {
        $data['unitkey'] = $this->post('unitkey');
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['mtgkey'] = $this->post('mtgkey');
        $data['idxdask'] = $this->post('idxdask');
        $data['kdnilai'] = $this->post('kdnilai');
        $data['kdjabar'] = $this->post('kdjabar');
        $data['uraian'] = $this->post('uraian');
        $data['jumbyek'] = $this->post('jumbyek');
        $data['satuan'] = $this->post('satuan');
        $data['tarif'] = $this->post('tarif');
        $data['subtotal'] = $this->post('subtotal');
        $data['ekspresi'] = $this->post('ekspresi');
        $data['inclsubtotal'] = $this->post('inclsubtotal');
        $data['type'] = $this->post('type');
        $data['idstdharga'] = $this->post('idstdharga');
        $data['kddana'] = $this->post('kddana');
        $data['kdtahap'] = $this->post('kdtahap');
        $data['tahun'] = $this->a['tahun'];
        $query = $this->insert_duplicate('sip_daskdetr', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function update_lock() {
        $unitkey = $this->input->post('unitkey');
        $data['lock_skpd'] = 'Y';
        $data['interval'] = 'N';
        $colum['unitkey'] = $unitkey;
        $this->update_where('lock_unit', $data, $colum);
    }

}
