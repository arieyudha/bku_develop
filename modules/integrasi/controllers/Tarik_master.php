<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tarik_master extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('login/Login');
        } else {
//            $this->load->model('setting/Model_set');
        }
    }

    public function index() {
        $record = $this->javasc_back();
        $record['a'] = $this->a;
        //Data
        $data = $this->layout_back('view_tarik_master', $record);
        $data['ribbon_left'] = ribbon_left('Integrasi', 'Tarik Master');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    public function insertUnitkerja() {
        $data['unitkey'] = $this->post('unitkey');
        $data['kdlevel'] = $this->post('kdlevel');
        $data['kdunit'] = $this->post('kdunit');
        $data['nmunit'] = $this->post('nmunit');
        $data['akrounit'] = $this->post('akrounit');
        $data['alamat'] = $this->post('alamat');
        $data['type'] = $this->post('type');
        $data['telepon'] = $this->post('telepon');
        $query = $this->insert_duplicate('sip_daftunit', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function insertProgram() {
        $data['kdtahap'] = $this->post('kdtahap');
        $data['idprgrm'] = $this->post('idprgrm');
        $data['nmprgrm'] = $this->post('nmprgrm');
        $data['nuprgrm'] = $this->post('nuprgrm');
        $data['unitkey_bidang'] = $this->post('unitkey_bidang');
        $data['tahun'] = $this->a['tahun'];
        $query = $this->insert_duplicate('sip_pgrmunit', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function insertKegiatan() {
        $dataKeg['kdtahap'] = $this->post('kdtahap');
        $dataKeg['unitkey'] = $this->post('unitkey');
        $dataKeg['kdkegunit'] = $this->post('kdkegunit');
        $dataKeg['idprgrm'] = $this->post('idprgrm');
        $dataKeg['nukeg'] = $this->post('nukeg');
        $dataKeg['nmkegunit'] = $this->post('nmkegunit');
        $dataKeg['noprior'] = $this->post('noprior');
        $dataKeg['kdsifat'] = $this->post('kdsifat');
        $dataKeg['nip'] = $this->post('nip');
        $dataKeg['tglakhir'] = $this->post('tglakhir');
        $dataKeg['tglawal'] = $this->post('tglawal');
        $dataKeg['targetp'] = $this->post('targetp');
        $dataKeg['lokasi'] = $this->post('lokasi');
        $dataKeg['jumlahmin1'] = $this->post('jumlahmin1');
        $dataKeg['pagu'] = $this->post('pagu');
        $dataKeg['jumlahpls1'] = $this->post('jumlahpls1');
        $dataKeg['sasaran'] = $this->post('sasaran');
        $dataKeg['ketkeg'] = $this->post('ketkeg');
        $dataKeg['tahun'] = $this->a['tahun'];
        $query = $this->insert_duplicate('sip_kegunit', $dataKeg);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function insertKinkeg() {
        $dataKeg['kdtahap'] = $this->post('kdtahap');
        $dataKeg['unitkey'] = $this->post('unitkey');
        $dataKeg['kdkegunit'] = $this->post('kdkegunit');
        $dataKeg['kdjkk'] = $this->post('kdjkk');
        $dataKeg['tolokur'] = $this->post('tolokur');
        $dataKeg['target'] = $this->post('target');
        $dataKeg['urjkk'] = $this->post('urjkk');
        $dataKeg['tahun'] = $this->a['tahun'];
        $query = $this->insert_duplicate('sip_kinkeg', $dataKeg);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function insertMatang() {
        $data['mtgkey'] = $this->post('mtgkey');
        $data['kdper'] = $this->post('kdper');
        $data['nmper'] = $this->post('nmper');
        $data['mtglevel'] = $this->post('mtglevel');
        $data['kdkhusus'] = $this->post('kdkhusus');
        $data['type'] = $this->post('type');
        $query = $this->insert_duplicate('sip_matangr', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
