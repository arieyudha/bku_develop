<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_tabel
 *
 * @author Asus
 */
class Model_tabel extends CI_Model {
    //put your code here
    
     var $table = 'aktifitas';
    var $column_search = array('keterangan', 'komputer', 'tanggal'); //set column field database for datatable searchable 

    public function __construct() {
        parent::__construct();
    }

    private function _get_datatables_query() {

        $this->db->from($this->table);
        $this->db->order_by('tanggal', 'desc')->order_by('jam', 'desc');
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_GET['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_GET['search']['value']);
                } else {
                    $this->db->or_like($item, $_GET['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
    }

    function get_datatables($kd_user) {
        $this->_get_datatables_query();
        if ($_GET['length'] != -1)
            $this->db->limit($_GET['length'], $_GET['start']);
        $this->db->where('kd_user',$kd_user);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($kd_user) {
        $this->_get_datatables_query();
        $this->db->where('kd_user',$kd_user);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($kd_user) {
        $this->_get_datatables_query();
        $this->db->where('kd_user',$kd_user);
        return $this->db->count_all_results();
    }
}
