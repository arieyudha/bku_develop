<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo $javasc;
$bln = '';
$i = 0;
foreach ($get_refBulan as $row_bln) {
    if ($i != 0) {
        $bln .= ',';
    }
    $bln .= '"' . $row_bln->inisial . '"';
    $i++;
}
$bln .= '';

$persen521 = round(@($row_apbd->nilai521 / $pagu->total_pagu) * 100);
$persen522 = round(@($row_apbd->nilai522 / $pagu->total_pagu) * 100);
$persen523 = round(@($row_apbd->nilai523 / $pagu->total_pagu) * 100);
?>
<section id="widget-grid" class=""> 
    <div class="well">
        <div class="row">
            <div class="col-sm-1">
                <label>Pilih Tahun :</label>
            </div>
            <form name="ftahun" method="get">
                <div class="col-sm-4">
                    <select class="form-control" name="tahun" onchange='document.ftahun.submit();' >
                        <?php for ($i = 2018; $i <= date('Y'); $i++) { ?>
                            <option <?php
                            if ($i == $tahun) {
                                echo 'selected';
                            };
                            ?>  value="<?= $i; ?>"><?= $i; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </form>
        </div>
        <legend></legend>
        <div class="row">
            <div class="col-sm-12">
                <h1><span class="semi-bold">Kondisi APBD Provinsi Kalimantan Selatan</span></h1>
                <div class="col-sm-6 col-md-4">
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?= base_url('assets/img/icon/polling3.png'); ?>" class="img-responsive"> 
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <h2 class="name">APBD</h2>
                                    <p class="price-container">
                                        <span>Rp. <?= numberFormat($pagu->total_pagu, 2); ?></span>
                                    </p>
                                    <!--<p><?= $persen522; ?></p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?= base_url('assets/img/icon/modul5.png'); ?>" class="img-responsive"> 
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <h2 class="name">Realisasi APBD</h2>
                                    <p class="price-container">
                                        <span>Rp. <?= numberFormat($realisasi->total_realisasi, 2); ?></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6 col-md-4">
                    <div class="product-content bg-gray product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3 text-align-center">
                                <i class="fa fa-product-hunt fa-5x"></i>
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <p class="price-container">
                                        <span><?= count($program) ?> Program</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="product-content bg-gray product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3 text-align-center">
                                <i class="fa fa-keyboard-o fa-5x"></i>
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <p class="price-container">
                                        <span><?= count($kegiatan) ?> Kegiatan</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6 col-md-4">
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?= base_url('assets/img/icon/user3.png'); ?>" class="img-responsive"> 
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <h2 class="name">Belanja Pegawai</h2>
                                    <p class="price-container">
                                        <span>Rp. <?= numberFormat($row_apbd->nilai521, 2); ?></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?= base_url('assets/img/icon/save3.png'); ?>" class="img-responsive"> 
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <h2 class="name">Belanja Barang dan Jasa</h2>
                                    <p class="price-container">
                                        <span>Rp. <?= numberFormat($row_apbd->nilai522, 2); ?></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="product-content product-wrap clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?= base_url('assets/img/icon/home5.png'); ?>" class="img-responsive"> 
                            </div>
                            <div class="col-md-9">
                                <div class="product-deatil">
                                    <h2 class="name">Belanja Modal</h2>
                                    <p class="price-container">
                                        <span>Rp. <?= numberFormat($row_apbd->nilai523, 2); ?></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 well"> 
        <h1 class="alert alert-info text-align-center">Pagu Anggaran Per SKPD</h1>
        <table class="table table-condensed table-striped table-hover table-bordered tabel_2" width="100%">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama SKPD</th>
                    <th width="20%">Pagu</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($get_totalPaguSkpd as $pg_skpd) { ?>
                    <tr>
                        <td><?= $pg_skpd->kdunit; ?></td>
                        <td><?= $pg_skpd->nmunit; ?></td>
                        <td class="text-right"><?= numberFormat($pg_skpd->total_pagu); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-6 well"> 
        <div id="belanja_langsung"></div>
    </div>
    <div class="col-sm-12 well"> 
        <div id="realisai_fisik_keuangan"></div>
    </div>
</section>
<script>
    $('.tabel_pagu_skpd').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        'pageLength': 100,
        "lengthChange": true,
        "searching": true,
        "info": true,
        "autoWidth": true,
        "ordering": false,
        "stateSave": true,

        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i> Hadangi Dulu...'
        },
        "ajax": {
            "url": '<?php echo site_url('frontend/Home/paguSkpd') ?>',
            "type": "GET"
        },
        "columnDefs": [
            {
                className: "text-center",
                "targets": [0, 2, 3, 4]
            },
        ]
    });

    Highcharts.chart('belanja_langsung', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                        name: 'Belanja Pegawai',
                        y: <?= $persen521; ?>,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Belanja Barang dan Jasa',
                        y: <?= $persen522; ?>
                    }, {
                        name: 'Belanja Modal',
                        y: <?= $persen523; ?>
                    }]
            }]
    });


    Highcharts.chart('realisai_fisik_keuangan', {
        title: {
            text: 'Realisasi Fisik dan Keuangan'
        },
        yAxis: {
            title: {
                text: '%'
            },
            labels: {
                format: "{value} %"
            },
            "reversed": false,
            "opposite": false,
            "type": "linear",
        },
        xAxis: {
            categories: [<?= $bln; ?>],
            labels: {
                rotation: -45,
                align: 'right',
                style: {
                    font: 'normal 13px Verdana, sans-serif'
                }
            }

        },
        "chart": {
            "polar": false,
            "inverted": false
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        tooltip: {
            "shared": true
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                "animation": false,
                "dataLabels": {}
            }
        },
        series: [{
                name: 'Target Fisik',
                data: [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 95, 100]
            }, {
                name: 'Realisasi Fisik',
                data: [2, 8, 12, 22, 35, 46, 59, 70, 77, 88, 95, 100]
            }, {
                name: 'Target Keuangan',
                data: [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 95, 100]
            }, {
                name: 'Realisasi Keuangan',
                data: [0, 0, 20, 30, 40, 46, 59, 70, 77, 88, 95, 100]
            }],

        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
        }

    });
</script>

