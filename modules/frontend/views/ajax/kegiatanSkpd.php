<legend></legend>
<h1 class="alert bg-blue-active text-align-center"><?= strtoupper('Kegiatan ' . $row_unit->nmunit); ?></h1>
<table class="table table-condensed table-striped table-hover table-bordered tabel_3" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Kegiatan</th>
            <th width="20%">Pagu</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($get_totalPaguKegiatanSkpd as $pg_skpd) {
            ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $pg_skpd->nmkeg; ?></td>
                <td class="text-right"><?= numberFormat($pg_skpd->total_pagu); ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script>
    
</script>