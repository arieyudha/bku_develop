<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo $javasc;
$bln = '';
$i = 0;
foreach ($get_refBulan as $row_bln) {
    if ($i != 0) {
        $bln .= ',';
    }
    $bln .= '"' . $row_bln->inisial . '"';
    $i++;
}
$bln .= '';

$persen521 = round(@($row_apbd->nilai521 / $pagu->total_pagu) * 100);
$persen522 = round(@($row_apbd->nilai522 / $pagu->total_pagu) * 100);
$persen523 = round(@($row_apbd->nilai523 / $pagu->total_pagu) * 100);
?>
<section id="widget-grid" class=""> 
    <div class="well">
        <div class="row">
            <div class="col-sm-1">
                <label>Pilih Tahun :</label>
            </div>
            <form name="ftahun" method="get">
                <div class="col-sm-4">
                    <select class="form-control" name="tahun" onchange='document.ftahun.submit();' >
                        <?php for ($i = 2018; $i <= date('Y'); $i++) { ?>
                            <option <?php
                            if ($i == $tahun) {
                                echo 'selected';
                            };
                            ?>  value="<?= $i; ?>"><?= $i; ?></option>
                            <?php } ?>
                    </select>
                </div>
            </form>
        </div>
        <legend></legend>
        <div class="row">
            <div class="col-md-8">
                <h1 class="alert alert-info text-align-center">Pagu Anggaran Per SKPD</h1>
                <table class="table table-bordered tabel_3" width="100%">
                    <thead>
                        <tr>
                            <th>Nama SKPD</th>
                            <th width="15%">Pagu</th>
                            <th width="15%">Belanja<br>Pegawai</th>
                            <th width="15%">Belanja<br>Barang Jasa</th>
                            <th width="15%">Belanja<br>Modal</th>
                            <th width="5%">Persentase</th>
                            <!--<th width="5%">Rincian</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($get_totalPaguSkpd as $pg_skpd) {
                            $row_pg = $this->Apbdmod->get_totalPaguLv3Skpd($tahun, $pg_skpd->unitkey)->row();
                            ?>
                            <tr>
                                <td><?= $pg_skpd->nmunit; ?></td>
                                <td class="text-right"><?= numberFormat($pg_skpd->total_pagu); ?></td>
                                <td class="text-right"><?= numberFormat($row_pg->nilai521); ?></td>
                                <td class="text-right"><?= numberFormat($row_pg->nilai522); ?></td>
                                <td class="text-right"><?= numberFormat($row_pg->nilai523); ?></td>
                                <td class="text-center"><?= numberFormat($pg_skpd->persen, 2); ?> %</td>
                                <!--<td class="text-center">-->
<!--                                    <button class="btn btn-success" onclick="lihatKegiatan('<?= $pg_skpd->unitkey; ?>', '<?=
                                    $tahun;
                                    ;
                                    ?>')"><i class="fa fa-search"></i> Detail</button>-->
                                <!--</td>-->
                            </tr>
<?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-4"> 
                <div id="ulItem"></div>
                <div id="containerPaguSkpd"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12"> 
                <center>
                    <div class="ajax-loader" >
                        <img src="<?= base_url(); ?>assets/img/loading1.gif" class="img-responsive" />
                    </div>
                </center>
                <div id="rincianKegiatanSkpd"></div>
            </div>
        </div>
    </div>
</section>
<script>
    function lihatKegiatan(unitkey, tahun) {
        $.ajax({
            type: "POST",
            url: "<?= site_url('frontend/Rincian_anggaran/rincianDetailSkpd'); ?>",
            data: {tahun: tahun, unitkey: unitkey},
            success: function (data) {
                $('#rincianKegiatanSkpd').html(data);
            },
            beforeSend: function () {
                $('.ajax-loader').css("visibility", "visible");
            },
            complete: function () {
                $('.ajax-loader').css("visibility", "hidden");
            }
        })
    }

    $(function () {

        $(document).ready(function () {

            var options = {

                chart: {
                    renderTo: 'containerPaguSkpd',
                    type: 'pie',
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: true
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
                },
                plotOptions: {
                    pie: {
                        innerSize: 100,
                        depth: 45,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Persentase',
                        data: []
                    }]
            }
//            var data = [{"kdunit": "2.02.01.00.", "nmunit": "DINAS PEMBERDAYAAN PEREMPUAN DAN PERLINDUNGAN ANAK", "total_pagu": "7695129859", "persen": 40.07}, {"kdunit": "3.01.01.03.", "nmunit": "BALAI BENIH TANAMAN PANGAN DAN HORTIKULTURA", "total_pagu": "2549664500", "persen": 13.28}, {"kdunit": "3.01.02.02.", "nmunit": "BALAI INSEMINASI BUATAN", "total_pagu": "4332716940", "persen": 22.56}, {"kdunit": "3.06.01.01.", "nmunit": "BALAI PENGUJIAN DAN SERTIFIKASI MUTU BARANG", "total_pagu": "2602097000", "persen": 13.55}, {"kdunit": "2.09.01.01.", "nmunit": "BALAI PELATIHAN KOPERASI DAN USAHA KECIL", "total_pagu": "2026273750", "persen": 10.55}];
            $.ajax({
                type: "GET",
                url: "<?= site_url('frontend/Rincian_anggaran/paguSkpdJson?tahun=' . $tahun); ?>",
                data: {},
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async: true,
                success: OnSuccess,
                error: OnError
            });

            function OnSuccess(data) {
                $.each(data, function (key, value) {
                    options.series[0].data.push([value.nmunit, value.persen]);
                    //alert(value.Status_Color);
                    //alert(value.Corrective_Action_ID);
                })
                chart = new Highcharts.Chart(options);
            }

            function OnError(data) {
                alert('fail');
            }
        });
    });
</script>

