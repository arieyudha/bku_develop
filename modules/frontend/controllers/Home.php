<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
//        $this->load->model('referensi/Model_ref');
//        $this->load->model('apbd/Apbdmod');
    }

    public function index() {
        $record = $this->javasc_front();
        $tahun = isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
        $record['get_refBulan'] = $this->Model_ref->get_refBulan()->result();
        $record['tahun'] = $tahun;
        $record['pagu'] = $this->total_pagu($tahun);
        $record['realisasi'] = $this->total_realisasi($tahun);
        $record['kegiatan'] = $this->total_kegiatan($tahun);
        $record['fisik'] = $this->total_fisik($tahun);
        $record['program'] = $this->Model_ref->get_program($tahun)->result();
        $record['kegiatan'] = $this->Model_ref->get_kegiatan($tahun)->result();
        $record['row_apbd'] = $this->Apbdmod->get_nilaiApbd($tahun)->row();
        $record['get_totalPaguSkpd'] = $this->Apbdmod->get_totalPaguSkpd($tahun)->result();
        $data = $this->layout_front('home', $record);
        $this->frontend($data);
    }

    function refBulan() {
        $get_refBulan = $this->Model_ref->get_refBulan()->result();
        foreach ($get_refBulan as $row) {
            $data['inisial'] = $row->inisial;
        }
        jsonArray($data);
    }

    public function paguSkpd() {
        $kd_user = $this->input->get('kd_user');
        $aktifitas = $this->Aktifitas->get_datatables($kd_user);
        $data = array();
        $no = $_GET['start'];
        $nor = 1;
        foreach ($aktifitas as $row_data) {
            $row = array();
            $row[] = '<td class="text-center">' . $nor++ . '</td>';
            $row[] = $row_data->keterangan;
            $row[] = $row_data->tanggal;
            $row[] = $row_data->jam;
            $row[] = $row_data->komputer;
            $row[] = $row_data->browser;
//            $row[] = '<button class="btn btn-danger"><i class="fa fa-trash"></i></button>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_GET['draw'],
            "recordsTotal" => $this->Aktifitas->count_all($kd_user),
            "recordsFiltered" => $this->Aktifitas->count_filtered($kd_user),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

}
