<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rincian_anggaran
 *
 * @author Asus
 */
class Rincian_anggaran extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('referensi/Model_ref');
        $this->load->model('apbd/Apbdmod');
    }

    public function index() {
        $record = $this->javasc_front();
        $tahun = isset($_REQUEST['tahun']) ? $_REQUEST['tahun'] : date('Y');
        $record['get_refBulan'] = $this->Model_ref->get_refBulan()->result();
        $record['tahun'] = $tahun;
        $record['pagu'] = $this->total_pagu($tahun);
        $record['row_apbd'] = $this->Apbdmod->get_nilaiApbd($tahun)->row();
        $record['get_totalPaguSkpd'] = $this->Apbdmod->get_totalPaguSkpd($tahun)->result();
        $data = $this->layout_front('rincian_anggaran', $record);
        $this->frontend($data);
    }

    function paguSkpdJson() {
        $tahun = $this->input->get('tahun');
        $array = $this->Apbdmod->get_totalPaguSkpd($tahun)->result_array();
        foreach ($array as $k => $val) {
            $array[$k]['persen'] = floatval($array[$k]['persen']);
        }
        jsonArray($array);
    }

    function rincianDetailSkpd() {
        $tahun = $this->input->post('tahun');
        $unitkey = $this->input->post('unitkey');
        $record['get_totalPaguKegiatanSkpd'] = $this->Apbdmod->get_totalPaguKegiatanSkpd($tahun, $unitkey)->result();
        $record['row_unit'] = $this->Model_ref->get_refDaftUnitWhereKey($unitkey)->row();
        $data = $this->load->view('ajax/kegiatanSkpd', $record);
        echo $data;
    }

}
