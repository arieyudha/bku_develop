<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pegawai extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('Model_ref');
        }
    }

    public function index() {
        //record
        $record = $this->javasc_back();
        $record['kd_level'] = $this->a['kd_level'];
        $record['getDataPegawai'] = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        //data   getDataJabatan
        $data = $this->layout_back('view_pegawai', $record);
        $data['ribbon_left'] = ribbon_left('Master', 'Jabatan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }


}
