<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                Data - data Jabatan 
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover tabel_2" width="100%">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jabatan</th>
                                    <th>Pangkat  / Gol</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($getDataPegawai as $row) {
                                    if ($row->status_pgw == 'PNS') {
                                        ?>
                                        <tr>
                                            <td><?= $row->nip; ?></td>
                                            <td class=""><?= $row->glr_depan . ' ' . $row->nama . ' ' . $row->glr_belakang; ?></td>
                                            <td class=""><?= $row->nama_jabatan; ?></td>
                                            <td class="text-center"><?= $row->pangkat . ' (' . $row->golongan . ')'; ?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
