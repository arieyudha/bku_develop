<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel">
            <div class="panel-heading">
                Data - data Jabatan 
            </div>
            <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover tabel_2" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama Jabatan</th>
                                        <th width="10%">Jenis Jabatan</th>
                                        <th width="10%">Eselon</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($getDataJabatan as $row){ ?>
                                    <tr>
                                        <td class="text-center"><?=$no++;?></td>
                                        <td class=""><?=$row->nama_jabatan;?></td>
                                        <td class="text-center"><?=$row->kd_jenis;?></td>
                                        <td class="text-center"><?=$row->kd_eselon;?></td>
                                    </tr>
                                    
                                    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
