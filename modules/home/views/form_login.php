<!DOCTYPE html>
<html lang="en-us" class="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>BKU Online</title>
        <link rel="icon" type="image/png" sizes="56x56" href="<?= logoKab(); ?>">
        <?= css_asset('bootstrap.min.css', 'bower_components/bootstrap/dist/css/'); ?>
        <?= css_asset('AdminLTE.min.css', 'css/'); ?>
        <?= css_asset('font-awesome.min.css', 'bower_components/font-awesome/css/'); ?>
        <?= css_asset('ionicons.min.css', 'bower_components/Ionicons/css/'); ?>
        <?= css_asset('blue.css', 'plugins/iCheck/square/'); ?>
        <?= css_asset('sweetalert2.min.css', 'plugins/sweetalert/dist/'); ?>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style type="text/css">
            .bg-login {
                background: url('<?= base_url("assets/img/bg2.png") ?>');
                background-size: cover;
                width: 100%
            }
            .logo-oneplan {
                /*float: left;*/
                width: 100px; 

                padding: 10px;
                background: #ffffff;
                border-radius: 10px;

            }
            .login-box-bg {
                border: solid 2px; 
                border-color: #33ff33;
                background-color:rgba(0, 0, 0, 0.10);
            }

            .login-box,.register-box{
                margin-top:5%; 
                margin-left: 10%;
            }
            .running_text{
                margin-top:3%; 
                margin-left: 10%;
                margin-right: 10%;
            }
            @media (max-width:1200px){
                .login-box,.register-box{width:360px;margin-top:10%; margin-left: 6%}
            }
            @media (max-width:960px){
                .login-box,.register-box{width:360px;margin-top:10%; margin-left: 5%}
            }
            @media (max-width:768px){
                .login-box,.register-box{width:360px;margin-top:10%; margin-left: 5%}
            }
            @media (max-width:360px){
                .login-box,.register-box{width:300px;margin-top:10%; margin-left: 5%}
            }


        </style>

    </head>

    <body class="login-page bg-login" style="height: 0;">
        <div class="login-box">
            <div class="login-box-body login-box-bg">
                <!--<p><?=$this->encryption->encrypt('admin');?></p>-->
                <center >
                    <img src="<?= logoKab() ?>" alt="logo" class="logo-oneplan">
                </center>
                <h3  class="text-center text-bold">Silahkan Login :</h3>
                <form id="form-login" method="post">
                    <div class="form-group has-feedback">
                        <label>Username :</label>
                        <input type="text" class="form-control" id="username" name="username" autofocus="" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span> 
                    </div>
                    <div class="form-group has-feedback">
                        <label>Password :</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
                    </div>
                    <div class="form-group has-feedback">
                        <label>Tahun Anggaran :</label>
                        <select class="form-control" id="tahun" name="tahun">
                            <?php
                            $tahun = date('Y');
                            for ($i = date('Y'); $i <= $tahun; $i++) {
                                ?>
                                <option value="<?= $tahun; ?>"><?= "Tahun $tahun"; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <!--<a href="#">Lupa Password?</a>-->
                        </div>

                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                        </div>
                    </div>
                </form>
                <hr>
                <h4 class="text-center">
                    Sistem Informasi Registrasi BKU Online
                </h4>
                <p class="text-center">
                    <b>Hak Cipta Dilindungi Undang-Undang</b>
                    <br>
                    Copyright &copy; <?php
                    if (date('Y') == 2020) {
                        echo '2020';
                    } else {
                        echo '2020 -' . date('Y');
                    }
                    ?>
                </p>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <?= js_asset('jquery.min.js', 'bower_components/jquery/dist/'); ?>
        <?= js_asset('bootstrap.min.js', 'bower_components/bootstrap/dist/js/'); ?>
        <?= js_asset('sweetalert2.min.js', 'plugins/sweetalert/dist/'); ?>
        <?= js_asset('icheck.min.js', 'plugins/iCheck/'); ?>
    </body>
</html>
<script>
    $("#form-login").submit(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        var tahun = $("#tahun").val();
        if (!username || !password || !tahun)
        {
            $(".has-feedback").toggleClass('has-feedback has-error');
            Swal({
                position: 'center',
                type: 'warning',
                title: 'Anda tidak memasukkan Username dan Password',
                showConfirmButton: false,
                timer: 1500,
                animation: false
            });
        } else {
            // kirim post
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('home/Login/validasi'); ?>",
                data: {
                    username: username,
                    password: password,
                    tahun: tahun
                },
                cache: false,
                success: function (result) {
                    console.log(username);
                    console.log(password);
                    console.log(tahun);
                    if (result == "true") {
                        Swal({
                            type: 'success',
                            title: 'Berhasil masuk!',
                            html: 'Anda akan dihubungkan, mohon menunggu',
                            timer: 2500,
                            onBeforeOpen: () => {
                                Swal.showLoading();
                            }
                        }).then((result) => {
                            window.location.href = "<?= base_url('home/Login/'); ?>";
                        });
                    } else if (result == "nani") {

                        Swal({
                            position: 'center',
                            type: 'info',
                            title: 'Ada kesalahan pada sistem',
                            showConfirmButton: false,
                            timer: 1000
                        });

                    } else {
                        Swal({
                            position: 'center',
                            type: 'error',
                            title: 'Username/Password tidak dikenali<br>Silahkan Coba lagi',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }
                }
            });
        }

        return false;
    });
</script>