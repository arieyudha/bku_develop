<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $a = aksesLog();
        if ($a) {
            redirect('home/Dashboard');
        } else {
            $this->load->view('form_login', NULL);
        }
    }

    function Form_login() {
        $this->load->view('form_login', NULL);
    }

    public function validasi() {
        $username = $this->post('username');
        $pass = $this->post('password');
        $tahun = $this->post('tahun');
        $row = $this->Model_Auth->validate_login($username);
        if (deskripsiText($row->password) == $pass) {
            if ($row) {
                $is_active = $row->is_active;
                if ($is_active == 1) {
                    $data = $this->sessionAplikasi($row->kd_level, $row->kd_user, $tahun);
                    $this->session->set_userdata('is_logined', $data);
                    echo "true";
                } else {
                    echo "false";
                }
            } else {  //username atau password salah
                return false;
            }
        }
    }

    function logout($kd_user) {
//        $ket = 'Logout';
//        $this->aktifitas($ket);
        $data['last_login_dt'] = date('Y-m-d');
        $data['last_login_tm'] = date('H:i:s');
        $data['is_login'] = 0;
        $this->update('kd_user', $kd_user, 'user', $data);
        $this->session->unset_userdata('is_logined');
        redirect($this->index());
    }

    function lock_screen($kd_user) {
        if (aksesLog()) {
            $ket = 'Lock Screen';
            $this->aktifitas($ket);
        }
        $data['is_login'] = 0;
        $req['tahun'] = $_GET['tahun'];
        $this->update('kd_user', $kd_user, 'user', $data);
        $req['kd_user'] = $kd_user;
        $req['row_user'] = $this->Model_Auth->get_user($kd_user)->row();
        $this->session->unset_userdata('is_logined');
        $this->load->view('back/lock_screen', $req);
    }

}
