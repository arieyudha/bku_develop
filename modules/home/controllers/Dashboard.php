<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        }
    }

    public function index() {
        $record = $this->javasc_back();
        $record['a'] = $this->a;
        $data = $this->layout_back('admin', $record);
        $data['ribbon_left'] = ribbon_left('Dashboard', 'Control Panel');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function update_lock() {
        $unitkey = $this->input->post('unitkey');
        $data['lock_skpd'] = 'Y';
        $data['interval'] = 'N';
        $colum['unitkey'] = $unitkey;
        $this->update_where('lock_unit', $data, $colum);
    }

}
