<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  
<style>
    .border_fieldset{border:2px solid #000; padding:10px; margin: 10px}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <form  name='fLevel' method='get'>
                        <div class="col-md-4">
                            <label>Pilih PPTK</label>
                            <div  class="form-group">
                                <select class="form-control select2 kd_jabatan" style="width: 100%;" name='kd_jabatan' onchange='document.fLevel.submit();'>
                                    <option value="">.: Pilih PPTK :.</option>
                                    <?php
                                    foreach ($get_dataPptkJabatan as $row_pptk) {
                                        $att = '';
                                        if ($row_pptk['kd_jabatan'] == $kd_jabatan) {
                                            $att = 'selected';
                                        }
                                        echo '<option ' . $att . ' 
                                        value="' . $row_pptk['kd_jabatan'] . '">' . $row_pptk['nama_jabatan'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-12">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data-data Program dan Kegiatan</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($kd_jabatan) && !empty($kd_bulan)) { ?>
                            <form  name='fBulan' method='get'>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Pilih Bulan</label>
                                        <div  class="form-group">
                                            <select class="form-control select2 kd_bulan" style="width: 100%;" name='kd_bulan' onchange='document.fBulan.submit();'>
                                                <?php
                                                foreach ($getBulan as $bln) {
                                                    $att = '';
                                                    if ($bln->kd_bulan == $kd_bulan) {
                                                        $att = 'selected';
                                                    }
                                                    echo '<option ' . $att . ' 
                                        value="' . $bln->kd_bulan . '">' . $bln->nama_bulan . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                            </form>
                            <table class="table table-hover table-bordered tabel_3" width='100%'>
                                <thead>
                                    <tr>
                                        <th width="5%">Kode</th>
                                        <th>Nama Program / Kegiatan</th>
                                        <th width="15%"><i class="fa fa-refresh"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($get_dataDpaProgram as $row_prog) {
                                        ?>
                                        <tr style="background-color: #e2e1e1">
                                            <td><?= $row_prog->nuprgrm; ?></td>
                                            <td><?= $row_prog->nmprgrm; ?></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        foreach ($get_dataReaKegPptk as $row_keg) {
                                            if ($row_prog->idprgrm == $row_keg->idprgrm) {

                                                if ($row_keg->status_posting == 'N') {
                                                    $bg_color = '';
                                                    $attrBtn = 'disabled';
                                                } else {
                                                    $bg_color = 'bg-green';
                                                    $attrBtn = '';
                                                }
                                                ?>
                                                <tr class="<?= $bg_color; ?>">
                                                    <td><?= $row_prog->nuprgrm . $row_keg->nukeg; ?></td>
                                                    <td><?= $row_keg->nmkegunit; ?></td>
                                                    <td class="no-padding">
                                                        <button <?=$attrBtn;?> onclick="postingData('<?= $kd_jabatan; ?>', '<?= $kd_bulan; ?>', '<?= $row_keg->kdkegunit; ?>', '<?= $row_bln->nama_bulan; ?>', '<?= $row_keg->nmkegunit; ?>')" class="btn btn-danger btn-flat btn-block">
                                                            <i class="fa fa-power-off"></i> Batalkan Posting
                                                        </button>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        } else {
                            $status = 'Pilih PPTK Terlebih dahulu';
                            $ket = '';
                            statusWarning($status, $ket);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function postingData(kd_jabatan, kd_bulan, kdkegunit, nm_bulan, nmkegunit) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });
        swalWithBootstrapButtons({
            title: `Apakah Anda yakin akan Membatalkan posting pada Bulan ${nm_bulan} dan Kegiatan ${nmkegunit}`,
            text: "Silahkan Klik Tombol Unposting untuk melakukan Aksi ini",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Unposting ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('posting/Post_kegiatan/postingKegiatan'); ?>",
                    data: {kd_jabatan: kd_jabatan, kd_bulan: kd_bulan, kdkegunit: kdkegunit, status_posting: 'N'},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        );
            }
        });
    }
</script>