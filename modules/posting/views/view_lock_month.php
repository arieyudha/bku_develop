<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?> 
<div class="row">
    <div class="col-md-12">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data-data PPTK</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered" width='100%'>
                            <thead>
                                <tr>
                                    <th rowspan="2" width="3%">No</th>
                                    <th rowspan="2" >Nama PPTK</th>
                                    <th colspan="<?= count($getBulan); ?>">Bulan</th>
                                </tr>
                                <tr>
                                    <?php foreach ($getBulan as $bln) { ?>
                                        <th><?= $bln->kd_bulan; ?></th>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <?php foreach ($getBulan as $bln) { ?>
                                        <th class="no-padding">
                                            <button onclick="unlockAll('<?= $bln->kd_bulan; ?>', 'N')" class="btn btn-primary btn-xs btn-flat btn-block"><i class="fa fa-unlock"></i> ALL</button>
                                            <button onclick="unlockAll('<?= $bln->kd_bulan; ?>', 'Y')" class="btn btn-danger btn-xs btn-flat btn-block"><i class="fa fa-lock"></i> ALL</button>
                                        </th>
                                    <?php } ?>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                $no = 1;
                                foreach ($get_dataPptkJabatan as $pptk) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $no++; ?></td>
                                        <td><?= $pptk['nama_jabatan']; ?></td>
                                        <?php
                                        foreach ($getBulan as $bln) {
                                            foreach ($get_lockPptkBulan as $lock) {
                                                if ($lock->kd_jabatan == $pptk['kd_jabatan'] && $lock->kd_bulan == $bln->kd_bulan) {
                                                    $status_lock = $lock->status_lock;
                                                    break;
                                                } else {
                                                    $status_lock = 'N';
                                                }
                                            }
                                            ?>
                                            <td class="no-padding">
                                                <?php if ($status_lock == 'N') { ?>
                                                    <button onclick="unlockPptk('<?= $pptk['kd_jabatan']; ?>', '<?= $bln->kd_bulan; ?>', 'Y')" class="btn btn-primary btn-flat btn-block"><i class="fa fa-unlock"></i></button>
                                                <?php } else { ?>
                                                    <button onclick="unlockPptk('<?= $pptk['kd_jabatan']; ?>', '<?= $bln->kd_bulan; ?>', 'N')" class="btn btn-danger btn-flat btn-block"><i class="fa fa-lock"></i></button>
                                                    <?php } ?>
                                            </td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function unlockPptk(kd_jabatan, kd_bulan, status) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });
        swalWithBootstrapButtons({
            title: `Apakah Anda yakin Melakukan ${ status == "N" ? 'Unlock' : 'Lock' } pada bulan ke ${kd_bulan}`,
            text: "Silahkan Klik Tombol Yes untuk melakukan Aksi ini",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('posting/Lock_month/actionLock'); ?>",
                    data: {kd_jabatan: kd_jabatan, kd_bulan: kd_bulan, status: status},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        );
            }
        });
    }
    function unlockAll(kd_bulan, status) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });
        swalWithBootstrapButtons({
            title: `Apakah Anda yakin Melakukan ${ status == "N" ? 'Unlock' : 'Lock' } Pada Semua PPTK pada bulan ke ${kd_bulan}`,
            text: "Silahkan Klik Tombol Yes untuk melakukan Aksi ini",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('posting/Lock_month/actionLockAll'); ?>",
                    data: {kd_bulan: kd_bulan, status: status},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        );
            }
        });
    }
</script>