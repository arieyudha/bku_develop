<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_posting extends CI_Model {

    //put your code here

    function get_dataPostingKegiatan($tahun, $kdtahap, $unitkey, $kdkegunit, $kd_bulan) {
        $query = $this->db->query("select a.*, b.kd_bulan, if(isnull(b.status_posting), 'N',b.status_posting) as status_posting, c.kode_qr, c.tgl_laporan
from sip_kegunit a 
left join post_kegiatan b on a.kdtahap=b.kdtahap and a.unitkey=b.unitkey and a.kdkegunit=b.kdkegunit and a.tahun=b.tahun and b.kd_bulan=$kd_bulan 
left join data_laporan_realisasi c on a.kdtahap=c.kdtahap and a.unitkey=c.unitkey and a.kdkegunit=c.kdkegunit and a.tahun=c.tahun and b.kd_bulan=$kd_bulan 
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' order by a.nukeg");
        return $query;
    }

    function get_lockPptkBulan() {
        $query = $this->db->query("select a.*, if(isnull(b.status_lock), 'N', b.status_lock) as status_lock, b.kd_jabatan from uti_bulan a 
left join lock_pptk b on a.kd_bulan=b.kd_bulan");
        return $query;
    }

    function get_lockPptkWhereThnBlnJbtn($tahun, $kd_bulan, $kd_jabatan) {
        $query = $this->db->query("select a.*, b.kd_jabatan, b.tahun, if(isnull(b.status_lock), 'N',b.status_lock) as status_lock from uti_bulan a 
left join lock_pptk b on a.kd_bulan=b.kd_bulan and b.kd_jabatan=$kd_jabatan and b.tahun=$tahun
where a.kd_bulan=$kd_bulan");
        return $query;
    }

}
