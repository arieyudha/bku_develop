<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lock_month extends MY_Controller {

    var $a;
    var $unitkey;

    public function __construct() {
        parent::__construct();
        $this->unitkey = $this->getUnitkey();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model(['dpa/Model_dpa', 'utility/Model_utility', 'master/Model_ref', 'dpa/Model_realisasi']);
        }
    }

    function index() {
        $record = $this->javasc_back();
        $record['get_dataPptkJabatan'] = $this->get_dataPptk($this->a['kd_level']);
        $record['get_lockPptkBulan'] = $this->Model_posting->get_lockPptkBulan()->result();
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $data = $this->layout_back('posting/view_lock_month', $record);
        $data['ribbon_left'] = ribbon_left('Kunci Data', 'Kunci Data Bulanan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function postingKegiatan() {
        $kd_bulan = $this->post('kd_bulan');
        $kdtahap = $this->getKdTahap($kd_bulan)->kdtahap;
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['kdtahap'] = $kdtahap;
        $data['kd_bulan'] = $kd_bulan;
        $data['tahun'] = $this->a['tahun'];
        $data['unitkey'] = $this->unitkey;
        $data['status_posting'] = $this->post('status_posting');
        $query = $this->insert_duplicate('post_kegiatan', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function actionLock() {
        $kd_bulan = $this->post('kd_bulan');
        $kd_jabatan = $this->post('kd_jabatan');
        $data['kd_bulan'] = $kd_bulan;
        $data['kd_jabatan'] = $kd_jabatan;
        $data['tahun'] = $this->a['tahun'];
        $data['status_lock'] = $this->post('status');
        $query = $this->insert_duplicate('lock_pptk', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function actionLockAll() {
        $kd_bulan = $this->post('kd_bulan');
        $get_dataPptkJabatan = $this->get_dataPptk($this->a['kd_level']);
        foreach ($get_dataPptkJabatan as $pptk) {
            $data['kd_bulan'] = $kd_bulan;
            $data['kd_jabatan'] = $pptk['kd_jabatan'];
            $data['tahun'] = $this->a['tahun'];
            $data['status_lock'] = $this->post('status');
            $query = $this->insert_duplicate('lock_pptk', $data);
        }
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
