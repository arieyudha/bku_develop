<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_kegiatan extends MY_Controller {

    var $a;
    var $unitkey;

    public function __construct() {
        parent::__construct();
        $this->unitkey = $this->getUnitkey();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model(['dpa/Model_dpa', 'utility/Model_utility', 'master/Model_ref', 'dpa/Model_realisasi']);
        }
    }

    function index() {
        $record = $this->javasc_back();
        $kd_jabatan = isset($_REQUEST['kd_jabatan']) ? $_REQUEST['kd_jabatan'] : '';
        $kd_bulan = isset($_REQUEST['kd_bulan']) ? $_REQUEST['kd_bulan'] : date('m');
        $record['kd_jabatan'] = $kd_jabatan;
        $record['kd_bulan'] = $kd_bulan;
        $record['get_dataPptkJabatan'] = $this->get_dataPptk($this->a['kd_level']);
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        if (!empty($kd_jabatan)) {
            $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
            $record['get_dataDpaProgram'] = $this->Model_dpa->get_dataDpaProgramPptk($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kd_jabatan)->result();
            $record['get_dataReaKegPptk'] = $this->Model_realisasi->get_dataReaKegPptk($this->a['tahun'], $row_kdtahap->kdtahap, $kd_bulan, $this->unitkey, $kd_jabatan)->result();
        }
        $data = $this->layout_back('posting/view_post_kegiatan', $record);
        $data['ribbon_left'] = ribbon_left('Posting', 'Data-data Program dan Kegiatan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function postingKegiatan() {
        $kd_bulan = $this->post('kd_bulan');
        $kdtahap = $this->getKdTahap($kd_bulan)->kdtahap;
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['kdtahap'] = $kdtahap;
        $data['kd_bulan'] = $kd_bulan;
        $data['tahun'] = $this->a['tahun'];
        $data['unitkey'] = $this->unitkey;
        $data['status_posting'] = $this->post('status_posting');
        $query = $this->insert_duplicate('post_kegiatan', $data);
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
