<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
$ttl_volume = !is_null($row_det->ttl_volume) ? $row_det->ttl_volume : 0;
$rea_volume = !is_null($row_det->rea_volume) ? $row_det->rea_volume : 0;
$ttl_realisasi = !is_null($row_det->ttl_realisasi) ? $row_det->ttl_realisasi : 0;
$keu_realisasi = !is_null($row_det->keu_realisasi) ? $row_det->keu_realisasi : 0;
$sisa_volume = $row_det->jumbyek - $ttl_volume;
$sisa_pagu = $row_det->subtotal - $ttl_realisasi;
if ($row_keg->status_posting == 'Y') {
    $classPost = 'hidden';
} else {
    $classPost = '';
}
if ($row_post->status_lock == 'Y') {
    $attrLock = 'disabled';
} else {
    $attrLock = '';
}
$attr_status = $attrLock;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel ">
            <div class="panel-body no-padding">
                <div class="table-responsive">
                    <table class="" border="1">
                        <tbody>
                            <tr>
                                <td width="20%">Program</td>
                                <td class="text-center">:</td>
                                <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . ' - ' . $row_prog->nmprgrm; ?></td>
                            </tr>
                            <tr>
                                <td>Kegiatan</td>
                                <td class="text-center">:</td>
                                <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . ' - ' . $row_keg->nmkegunit; ?></td>
                            </tr>
                            <tr>
                                <td>Rekening</td>
                                <td class="text-center">:</td>
                                <td ><?= $row_lv5->kdper . ' - ' . $row_lv5->nmper ?></td>
                            </tr>
                            <tr>
                                <td>Rincian Belanja</td>
                                <td class="text-center">:</td>
                                <td ><?= $row_det->uraian ?></td>
                            </tr>
                            <tr>
                                <td>Volume</td>
                                <td class="text-center">:</td>
                                <td ><?= $row_det->jumbyek . ' ' . $row_det->satuan ?></td>
                            </tr>
                            <tr>
                                <td>Satuan Pagu</td>
                                <td class="text-center">:</td>
                                <td >Rp. <?= numberFormat($row_det->tarif) ?></td>
                            </tr>
                            <tr>
                                <td>Pagu Anggaran</td>
                                <td class="text-center">:</td>
                                <td >Rp. <?= numberFormat($row_det->subtotal) ?></td>
                            </tr>
                            <tr>
                                <td>Realisasi Sampai Dengan</td>
                                <td class="text-center">:</td>
                                <td >Rp. <?= numberFormat($ttl_realisasi) ?></td>
                            </tr>
                            <tr>
                                <td>Sisa Pagu</td>
                                <td class="text-center">:</td>
                                <td >Rp. <?= numberFormat($sisa_pagu) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </div>
    <div class="col-md-12">
        <?php
        if ($row_keg->status_posting == 'Y') {
            $status = 'Posting Sudah dilakukan';
            $ket = 'Kegiatan ini sudah dilakukan Posting data-data tidak bisa di Edit';
            statusWarning($status, $ket);
        }
        ?>
        <div class="panel ">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-2">
                        <?php
                        $attr = $attr_status . " data-toggle='modal'
                                data-target='#modal-realisasi' 
                                data-aksi='tambah' 
                                data-status_lock='$row_post->status_lock' 
                                data-volume='$row_det->jumbyek' 
                                data-sisa_volume='$sisa_volume' 
                                data-ttl_volume='$ttl_volume' 
                                data-rea_volume='$rea_volume' 
                                data-satuan='$row_det->satuan' 
                                data-kdnilai='$row_det->kdnilai' 
                                data-mtgkey='$row_det->mtgkey' 
                                data-pagu_anggaran='$row_det->subtotal'
                                data-ttl_realisasi='$ttl_realisasi' 
                                data-sisa_pagu='$sisa_pagu'";
                        $ket = "Realisasi";
                        $class = "btn-flat btn-block " . $classPost;
                        btn_tambah($attr, $ket, $class);
                        ?>
                    </div>
                    <div class="col-md-2 col-md-offset-8">
                        <form class="" method="GET" action="<?= site_url('dpa/Data_input/input_dpa'); ?>">
                            <input type="hidden" class="form-control" name="kd_bulan" value="<?= $kd_bulan; ?>">
                            <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                            <input type="hidden" class="form-control " name="idprgrm" value="<?= $idprgrm; ?>">
                            <input type="hidden" class="form-control" name="kdkegunit" value="<?= $kdkegunit; ?>">
                            <button class="btn btn-danger btn-flat btn-block"><i class="fa fa-backward"></i> Kembali</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <label>Pilih Bulan :</label>
                    </div>
                    <div class="col-md-4">
                        <form  name='fBulan' method='get'>
                            <div  class="form-group">
                                <select class="form-control select2 kd_bulan" style="width: 100%;" name='kd_bulan' onchange='document.fBulan.submit();'>
                                    <?php
                                    foreach ($getBulan as $bln) {
                                        $att = '';
                                        if ($bln->kd_bulan == $kd_bulan) {
                                            $att = 'selected';
                                        }
                                        echo '<option ' . $att . ' 
                                        value="' . $bln->kd_bulan . '">' . $bln->nama_bulan . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                            <input type="hidden" class="form-control " name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                            <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                            <input type="hidden" class="form-control" name="mtgkey" value="<?= $row_det->mtgkey; ?>">
                            <input type="hidden" class="form-control" name="kdnilai" value="<?= $row_det->kdnilai; ?>">
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered tabel_3" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Uraian Realisasi</th>
                                        <th width="8%">Bulan</th>
                                        <th width="7%">Volum</th>
                                        <th width="15%">Realisasi</th>
                                        <th width="7%"><i class="fa fa-cog"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    $ttl_rea_volume = 0;
                                    $ttl_rea_keu = 0;
                                    foreach ($get_dataRealisasi as $row) {
                                        if ($kd_bulan == $row->kd_bulan) {
                                            if ($row->volume == 1) {
                                                $ttl_rea_volume = 1;
                                            } else {
                                                $ttl_rea_volume += $row->volume;
                                            }
                                            $ttl_rea_keu += $row->keu_realisasi;
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $no++; ?></td>
                                                <td><?= $row->uraian; ?></td>
                                                <td class="text-center"><?= $row->nama_bulan; ?></td>
                                                <td class="text-center"><?= $row->volume . ' ' . $row_det->satuan; ?></td>
                                                <td class="text-right"><span class="pull-left">Rp. </span><?= numberFormat($row->keu_realisasi); ?></td>
                                                <td class="text-center">
                                                    <?php
                                                    $uraian = str_replace(["'", "\r\n"], ' ', $row->uraian);
                                                    $attrEdt = $attr_status . " data-toggle='modal'
                                                            data-target='#modal-realisasi' 
                                                            data-aksi='edit' 
                                                            data-status_lock='$row_post->status_lock' 
                                                            data-kd_rea='$row->kd_rea' 
                                                            data-uraian='$uraian' 
                                                            data-volume='$row_det->jumbyek' 
                                                            data-sisa_volume='$sisa_volume' 
                                                            data-ttl_volume='$ttl_volume' 
                                                            data-rea_volume='$row->volume' 
                                                            data-keu_realisasi='$row->keu_realisasi' 
                                                            data-satuan='$row_det->satuan' 
                                                            data-kdnilai='$row_det->kdnilai' 
                                                            data-mtgkey='$row_det->mtgkey' 
                                                            data-pagu_anggaran='$row_det->subtotal'
                                                            data-ttl_realisasi='$ttl_realisasi' 
                                                            data-sisa_pagu='$sisa_pagu'";
                                                    $ketEdt = "";
                                                    $classEdt = "$classPost";
                                                    btn_edit($attrEdt, $ketEdt, $classEdt);

                                                    $attrHps = $attr_status . ' onclick="hapusRealisasi(\'' . $row->kd_rea . '\', \'' . $uraian . '\',\'' . $row_post->status_lock . '\')"';
                                                    $ketHps = "";
                                                    $classHps = "$classPost";
                                                    btn_hapus($attrHps, $ketHps, $classHps);
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center" colspan="3">Total</th>
                                        <th class="text-center"><?= $ttl_rea_volume . ' ' . $row_det->satuan; ?></th>
                                        <th class="text-right"><span class="pull-left">Rp. </span><?= numberFormat($ttl_rea_keu); ?></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-realisasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head" id=""></h4>
            </div>
            <form class="form-realisasi" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Pagu Anggaran</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Rp.</span>
                                    </div>
                                    <input readonly class="form-control text-right pagu_anggaran" name="pagu_anggaran" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="">,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Total Realisasi </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Rp.</span>
                                    </div>
                                    <input readonly class="form-control text-right ttl_realisasi" name="ttl_realisasi" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="">,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Realisasi Bulan ini</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Rp.</span>
                                    </div>
                                    <input class="form-control text-right keu_realisasi" readonly name="keu_realisasi" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="">,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <legend></legend>
                        </div>
                        <div class="col-md-4">
                            <label>Sisa Pagu</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Rp.</span>
                                    </div>
                                    <input readonly class="form-control text-right sisa_pagu" name="sisa_pagu" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="">,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Volume</span>
                                    </div>
                                    <input readonly class="form-control text-right volume" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="satuan"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-7 realisasi-volume">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Realisasi Volume</span>
                                    </div>
                                    <input class="form-control text-right rea_volume" name="rea_volume" value="0" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="satuan"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Harga satuan</span>
                                    </div>
                                    <input class="form-control text-right harga_satuan" name="harga_satuan" required>
                                    <input readonly class="form-control text-right harga_satuan_fix" name="harga_satuan_fix" value="<?= $row_det->tarif; ?>">
                                    <div class="input-group-addon bg-gray">
                                        <span class="satuan"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Total Rea Volume</span>
                                    </div>
                                    <input readonly class="form-control text-right ttl_volume" name="ttl_volume" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="satuan"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <span class="">Sisa Volume</span>
                                    </div>
                                    <input readonly class="form-control text-right sisa_volume" name="sisa_volume" required>
                                    <div class="input-group-addon bg-gray">
                                        <span class="satuan"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Uraian Realisasi</label>
                                    <textarea class="form-control uraian" name="uraian" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                    <input type="hidden" class="form-control kd_bulan" name="kd_bulan" value="<?= $kd_bulan; ?>">
                    <input type="hidden" class="form-control kdkegunit" name="kdkegunit" value="<?= $kdkegunit; ?>">
                    <input type="hidden" class="form-control kdtahap" name="kdtahap" value="<?= $kd_tahap; ?>">
                    <input type="hidden" class="form-control mtgkey" name="mtgkey">
                    <input type="hidden" class="form-control kdnilai" name="kdnilai">
                    <input type="hidden" class="form-control kd_rea" name="kd_rea">
                    <input type="hidden" class="form-control status_lock" name="status_lock">
                    <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    callBackClassAfter('.keu_realisasi', 'cek-keu');
    callBackClassAfter('.rea_volume', 'cek-volume');
    callBackClassAfter('.harga_satuan', 'cek-harga_satuan');
    $('#modal-realisasi').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var status_lock = button.data('status_lock');
        var mtgkey = button.data('mtgkey');
        var kdnilai = button.data('kdnilai');
        var ttl_realisasi = button.data('ttl_realisasi');
        var sisa_pagu = button.data('sisa_pagu');
        var pagu_anggaran = button.data('pagu_anggaran');
        var volume = button.data('volume');
        var ttl_volume = button.data('ttl_volume');
        var rea_volume = button.data('rea_volume');
        var keu_realisasi = button.data('keu_realisasi');
        var satuan = button.data('satuan');
        var sisa_volume = button.data('sisa_volume');
        var kd_rea = button.data('kd_rea');
        var uraian = button.data('uraian');
        var modal = $(this);
        if (satuan == 'Tahun') {
            $('.keu_realisasi').prop('readonly', false);
            modal.find('.modal-body input.rea_volume').val('1').prop('required', false);
            modal.find('.modal-body input.harga_satuan').prop('required', false);
            $('.realisasi-volume').addClass('hidden').prop('required', false);
        } else {
            $('.keu_realisasi').prop('readonly', true);
            modal.find('.modal-body input.rea_volume').val('1').prop('required', true);
            modal.find('.modal-body input.harga_satuan').prop('required', true);
            $('.realisasi-volume').removeClass('hidden').prop('required', true);
        }
        modal.find('.satuan').text(satuan);
        modal.find('.modal-body input.volume').val(volume);
        modal.find('.modal-body input.ttl_volume').val(ttl_volume).change();
        modal.find('.modal-body input.sisa_volume').val(sisa_volume);
        modal.find('.modal-body input.pagu_anggaran').val(pagu_anggaran);
        modal.find('.modal-body input.ttl_realisasi').val(ttl_realisasi).change();
        modal.find('.modal-body input.sisa_pagu').val(sisa_pagu);
        modal.find('.modal-body input.kdnilai').val(kdnilai);
        modal.find('.modal-body input.mtgkey').val(mtgkey);
        modal.find('.modal-body input.mtgkey').val(mtgkey);
        modal.find('.modal-body input.status_lock').val(status_lock);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.keu_realisasi').val('').change();
            modal.find('.modal-body input.kd_rea').val('');
            modal.find('.modal-body textarea.uraian').text('');
            $('.label_head').html('Form Tambah Input Realisasi');
            $('.form-realisasi').attr('action', '<?= site_url('dpa/Data_input/inputRealisasi'); ?>');
        } else {
            modal.find('.modal-body input.rea_volume').val(rea_volume).change();
            modal.find('.modal-body input.keu_realisasi').val(keu_realisasi).change();
            modal.find('.modal-body input.kd_rea').val(kd_rea);
            modal.find('.modal-body textarea.uraian').text(uraian);
            $('.label_head').html('Form Edit Input Realisasi');
            $('.form-realisasi').attr('action', '<?= site_url('dpa/Data_input/inputRealisasi'); ?>');
        }

    });

    $('.harga_satuan').keyup('on', function () {
        var harga_satuan = parseFloat($(this).val());
        var harga_satuan_fix = parseFloat($('.harga_satuan_fix').val());
        var rea_volume = parseFloat($('.rea_volume').val());
        var harga_ttl = harga_satuan * rea_volume;
        $('.keu_realisasi').val(harga_ttl);
        if (harga_satuan > harga_satuan_fix) {

            $('.btn-save').prop('disabled', true);
            $('.cek-harga_satuan').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Melebihi Sisa Pagu</b>');
        } else {
            $('.btn-save').prop('disabled', false);
            $('.cek-harga_satuan').html('');
        }
    });

    $('.keu_realisasi').keyup('on', function () {
        var rea_keu = parseFloat($(this).val());
        var pagu = parseFloat($('.pagu_anggaran').val());
        var ttl_keu = parseFloat($('.ttl_realisasi').val());
        var sisa_pagu = parseFloat($('.sisa_pagu').val());
        var ttl_keu = ttl_keu + rea_keu;
        var sisa_pagu = pagu - ttl_keu;
        $('.sisa_pagu').val(sisa_pagu);
        if (sisa_pagu >= 0) {
            $('.btn-save').prop('disabled', false);
            $('.cek-keu').html('');
        } else {
            $('.btn-save').prop('disabled', true);
            $('.cek-keu').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Melebihi Sisa Pagu</b>');
        }
    });

    $('.rea_volume').keyup('on', function () {
        var rea_volume = parseFloat($(this).val());
        var harga_satuan = parseFloat($('.harga_satuan').val());
        var volume = parseFloat($('.volume').val());
        var ttl_volume = parseFloat($('.ttl_volume').val());
        var sisa_volume = parseFloat($('.sisa_volume').val());
        var ttl_volume = ttl_volume + rea_volume;
        var sisa_volume = volume - ttl_volume;
        var harga_ttl = harga_satuan * rea_volume;
        $('.sisa_volume').val(sisa_volume);
        $('.keu_realisasi').val(harga_ttl);
        if (sisa_volume >= 0) {
            $('.btn-save').removeClass('disabled');
            $('.cek-volume').html('');
        } else {
            $('.btn-save').addClass('disabled');
            $('.cek-volume').html('<img src="<?php echo base_url(); ?>/assets/img/false.png"><b style="color:red;"> Melebihi Volume</b>');
        }
    });

    function hapusRealisasi(kd_rea, uraian, status_lock) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Uraian dangen : ' + uraian,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('dpa/Data_input/deleteRealisasi'); ?>",
                    data: {kd_rea: kd_rea, status_lock: status_lock},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });

    }
</script>