<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body no-padding">
                <div class="col-md-4" style="padding: 10px">
                    <div class="row">
                        <form  name='fLevel' method='get'>
                            <div class="col-md-12">
                                <label>Pilih PPTK <?=$tahun;?></label>
                                <div  class="form-group">
                                    <select class="form-control select2 kd_jabatan" style="width: 100%;" name='kd_jabatan' onchange='document.fLevel.submit();'>
                                        <option value="">.: Pilih PPTK :.</option>
                                        <?php
                                        foreach ($get_dataPptkJabatan as $row_pptk) {
                                            $att = '';
                                            if ($row_pptk['kd_jabatan'] == $kd_jabatan) {
                                                $att = 'selected';
                                            }
                                            echo '<option ' . $att . ' 
                                        value="' . $row_pptk['kd_jabatan'] . '">' . $row_pptk['nama_jabatan'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8 bg-gray" style="padding: 10px">
                    <p>Keterangan :</p>
                    <ul>
                        <li>Input Data Realisasi yang mana disini memilih Kegiatan yang akan di Input</li>
                        <li>Apabila sudah selesai Input data silakan Klik Tombol Posting <label class="label label-warning"><i class="fa fa-power-off"></i> Posting</label></li>
                        <li>Lakukan Pengecekan terlebih dahulu sebelum melakukan aksi Posting karena apabila sudah dilakukan Posting Sistem akan otomatis terkunci</li>
                        <li>Apabila batas waktu yang ditentukan PPTK belum melakukan Aksi Posting maka dianggap Penginputan Belum selesai dilakukan ?</li>
                        <li>Jika pada Baris kegiatan <label class="label label-success">Berwarna Hijau</label> berarti kegiatan tersebut sudah dilakukan Posting</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <?php if (!empty($kd_jabatan)) { ?>
            <div class="panel">
                <div class="panel-heading bg-gray">
                    <h3 class="panel-title">Data Program dan Kegiatan</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form  name='fBulan' method='get'>
                                <div class="col-md-4">
                                    <label>Pilih Bulan</label>
                                    <div  class="form-group">
                                        <select class="form-control select2 kd_bulan" style="width: 100%;" name='kd_bulan' onchange='document.fBulan.submit();'>
                                            <?php
                                            foreach ($getBulan as $bln) {
                                                $att = '';
                                                if ($bln->kd_bulan == $kd_bulan) {
                                                    $att = 'selected';
                                                }
                                                echo '<option ' . $att . ' 
                                        value="' . $bln->kd_bulan . '">' . $bln->nama_bulan . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                            </form>
                        </div>
                    </div>

                    <table class="table table-hover table-bordered tabel_3" width='100%'>
                        <thead>
                            <tr>
                                <th width="5%">Kode</th>
                                <th>Nama Program / Kegiatan</th>
                                <th>Pagu Anggaran</th>
                                <th>Realisasi Bulan <?= $row_bln->nama_bulan; ?></th>
                                <th>Total Realisasi</th>
                                <th>Sisa Anggaran</th>
                                <th width="15%"><i class="fa fa-refresh"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $pagu_apbd = 0;
                            $rea_keu_prog_bln = 0;
                            $rea_keu_prog_all = 0;
                            foreach ($get_dataDpaProgram as $row_prog) {
                                $rea_keu_prog = 0;
                                $ttl_keu_prog = 0;
                                $pagu_apbd += $row_prog->pagu_prog;
                                foreach ($get_dataReaKegPptk as $row_keg) {
                                    if ($row_prog->idprgrm == $row_keg->idprgrm) {
                                        $rea_keu_prog += $row_keg->keu_realisasi;
                                        $ttl_keu_prog += $row_keg->ttl_realisasi;
                                        $rea_keu_prog_bln += $row_keg->keu_realisasi;
                                        $rea_keu_prog_all += $row_keg->ttl_realisasi;
                                    }
                                }
                                ?>
                                <tr style="background-color: #e2e1e1">
                                    <td><?= $row_prog->nuprgrm; ?></td>
                                    <td><?= $row_prog->nmprgrm; ?></td>
                                    <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_prog->pagu_prog); ?></td>
                                    <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog); ?></td>
                                    <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($ttl_keu_prog); ?></td>
                                    <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_prog->pagu_prog - $ttl_keu_prog); ?></td>
                                    <td></td>
                                </tr>
                                <?php
                                foreach ($get_dataReaKegPptk as $row_keg) {
                                    if ($row_prog->idprgrm == $row_keg->idprgrm) {

                                        if ($row_keg->status_posting == 'N') {
                                            $bg_color = '';
                                        } else {
                                            $bg_color = 'bg-green';
                                        }
                                        ?>
                                        <tr class="<?=$bg_color;?>">
                                            <td><?= $row_prog->nuprgrm . $row_keg->nukeg; ?></td>
                                            <td><?= $row_keg->nmkegunit; ?></td>
                                            <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->pagu); ?></td>
                                            <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->keu_realisasi); ?></td>
                                            <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->ttl_realisasi); ?></td>
                                            <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->pagu - $row_keg->ttl_realisasi); ?></td>
                                            <td class="no-padding">
                                                <form action="<?= site_url('dpa/Data_input/input_dpa'); ?>" method="get">
                                                    <input type="hidden" class="form-control" name="kd_bulan" value="<?= $kd_bulan; ?>">
                                                    <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                                                    <input type="hidden" class="form-control " name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                                                    <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                                                    <button class="btn btn-primary btn-flat btn-block">
                                                        <i class="fa fa-search"></i> View DPA
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2">TOTAL</th>
                                <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_apbd); ?></th>
                                <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog_bln); ?></th>
                                <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog_all); ?></th>
                                <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_apbd - $rea_keu_prog_all); ?></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <?php
        } else {
            $status = 'Pilih PPTK';
            $ket = 'Silakan Pilih Terlebih dahulu PPTK';
            statusWarning($status, $ket);
        }
        ?>
    </div>
</div>
