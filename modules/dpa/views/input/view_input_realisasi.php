<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
$attrPost = '';
if ($row_keg->status_posting == 'Y') {
    $attrPost = 'hidden';
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel ">
            <div class="panel-body">
                <div class="col-md-2">
                    <label>Pilih Bulan :</label>
                </div>
                <div class="col-md-4">
                    <form  name='fBulan' method='get'>
                        <div  class="form-group">
                            <select class="form-control select2 kd_bulan" style="width: 100%;" name='kd_bulan' onchange='document.fBulan.submit();'>
                                <?php
                                foreach ($getBulan as $bln) {
                                    $att = '';
                                    if ($bln->kd_bulan == $kd_bulan) {
                                        $att = 'selected';
                                    }
                                    echo '<option ' . $att . ' 
                                        value="' . $bln->kd_bulan . '">' . $bln->nama_bulan . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                        <input type="hidden" class="form-control " name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                        <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                    </form>
                </div>
                <div class="col-md-4">
                    <?php
                    if ($row_keg->status_posting == 'Y') {
                        $status = 'Posting';
                        $ket = 'Kegiatan ini sudah dilakukan Posting';
                        statusWarning($status, $ket);
                    }
                    ?>
                </div>
                <div class="col-md-2">
                    <form class="" method="GET" action="<?= site_url('dpa/Data_input'); ?>">
                        <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                        <input type="hidden" class="form-control" name="kd_bulan" value="<?= $kd_bulan; ?>">
                        <button class="btn btn-danger btn-flat btn-block"><i class="fa fa-backward"></i> Kembali</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <button class="btn btn-warning <?= $attrPost; ?>" onclick="postingData('<?= $kd_jabatan; ?>', '<?= $kd_bulan; ?>', '<?= $row_keg->kdkegunit; ?>', '<?= $row_bln->nama_bulan; ?>', '<?= $row_keg->nmkegunit; ?>')" id="myBtn"><i class="fa fa-power-off"></i> Posting</button>    
        <div class="panel ">
            <div class="panel-body no-padding">
                <table class="" border="1">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">DOKUMEN PELAKSANAAN ANGGARAN<br>SATUAN KERJA PERANGKAT DAERAH</th>
                            <th class="text-center" width="20%">NOMOR DPA SKPD</th>
                            <th class="text-center" width="10%" rowspan="2">Formulir<br>DPA-SKPD 2.2.1</th>
                        </tr>
                        <tr>
                            <th class="text-center"><?= $row_skpd->kdunit . $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . '5.2'; ?></th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3">PEMERINTAH PROVINSI KALIMANTAN SELATAN<BR>TAHUN ANGGARAN <?= $tahun; ?></th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3" style="padding: 2px"></th>
                        </tr>
                    </thead>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <td width="15%">Urusan Pemerintahan</td>
                            <td widtd="1%" class="text-center">:</td>
                            <td ><?= $row_bidang->kdunit . ' - ' . $row_bidang->nmunit; ?></td>
                        </tr>
                        <tr>
                            <td >Organisasi</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . ' - ' . $row_unit->nmunit; ?></td>
                        </tr>
                        <tr>
                            <td >Program</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . ' - ' . $row_prog->nmprgrm; ?></td>
                        </tr>
                        <tr>
                            <td>Kegiatan</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . ' - ' . $row_keg->nmkegunit; ?></td>
                        </tr>
                        <tr>
                            <td>Waktu Pelaksanaan</td>
                            <td class="text-center">:</td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td>Lokasi Kegiatan</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_keg->lokasi; ?></td>
                        </tr>
                        <tr>
                            <td>Sumber Dana</td>
                            <td class="text-center">:</td>
                            <td >PAD</td>
                        </tr>
                    </tbody>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <th class="text-center" colspan="3">Indikator & Tolok Ukur Kinerja Belanja Langsung</th>
                        </tr>
                        <?php foreach ($get_dataDpaKinkeg as $row_kinkeg) {
                            if($row_kinkeg->kdjkk == '01'){
                                $trg_kinkeg = 'Rp. '.numberFormat($row_keg->pagu,2);
                            }else{
                                $trg_kinkeg = $row_kinkeg->target;
                            }
?>
                            <tr>
                                <th><?= $row_kinkeg->urjkk; ?></th>
                                <td><?= $row_kinkeg->tolokur; ?></td>
                                <td><?= $trg_kinkeg; ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3">Kelompok Sasaran Kegiatan : <?= $row_keg->sasaran; ?></td>
                        </tr>
                    </tbody>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <th class="text-center" colspan="12">Rancangan Rincian Dokumen Pelaksanaan Anggaran Belanja
                                <br>Menurut Program dan Per Kegiatan Satuan Kerja Perangkat Daerah</th>
                        </tr>
                        <tr>
                            <th class="text-center" rowspan="2" width="10%">Rekening</th>
                            <th class="text-center" rowspan="2" width="30%">Uraian</th>
                            <th class="text-center" colspan="3">Rincian Perhitungan</th>
                            <th class="text-center" rowspan="2">Jumlah (Rp)</th>
                            <th class="text-center" rowspan="2">Realisasi S/D Bulan Lalu</th>
                            <th class="text-center" rowspan="2">Realisasi  <br><?= $row_bln->nama_bulan; ?></th>
                            <th class="text-center" rowspan="2">Total Realisasi S/D <br><?= $row_bln->nama_bulan; ?></th>
                            <th class="text-center" rowspan="2">Total Realisasi Keseluruhan</th>
                            <th class="text-center" rowspan="2">Sisa Pagu</th>
                            <th class="text-center" rowspan="2">Action</th>
                        </tr>
                        <tr>
                            <th class="text-center">Volume</th>
                            <th class="text-center">Satuan</th>
                            <th class="text-center">Harga Satuan</th>
                        </tr>
                        <tr>
                            <?php for ($i = 1; $i <= 12; $i++) { ?>
                                <th class="text-center"><?= $i; ?></th>
                            <?php } ?>
                        </tr>
                        <?php
                        foreach ($get_dataDpaRekLv1 as $lv1) {
                            $keu_realisasi_lv1 = 0;
                            $ttl_realisasi_lv1 = 0;
                            $ttl_realisasi_bln_lv1 = 0;
                            foreach ($get_dataDpaRekLv2 as $lv2) {
                                $kdlv1 = substr($lv2->kdper_rep, 0, 1);
                                if ($kdlv1 == $lv1->kdper_rep) {
                                    foreach ($get_dataDpaRekLv3 as $lv3) {
                                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                                        if ($kdlv2 == $lv2->kdper_rep) {
                                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                                if ($kdlv3 == $lv3->kdper_rep) {
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $keu_realisasi_lv1 += $dpa->keu_realisasi;
                                                                    $ttl_realisasi_bln_lv1 += $dpa->ttl_realisasi_bln_ll;
                                                                    $ttl_realisasi_lv1 += $dpa->ttl_realisasi;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                            <tr class="bg-gray-active">
                                <td><?= $lv1->kdper; ?></td>
                                <td><?= $lv1->nmper; ?></td>
                                <!--<td></td>-->
                                <td></td>
                                <td></td>
                                <td class="text-center"></td>
                                <td class="text-right"><?= numberFormat($lv1->nilai); ?></td>
                                <td class="text-right"><?= numberFormat($ttl_realisasi_bln_lv1); ?></td>
                                <td class="text-right"><?= numberFormat($keu_realisasi_lv1); ?></td>
                                <td class="text-right"><?= numberFormat($keu_realisasi_lv1+$ttl_realisasi_bln_lv1); ?></td>
                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv1); ?></td>
                                <td class="text-right"><?= numberFormat($lv1->nilai - $ttl_realisasi_lv1); ?></td>
                                <td class="text-center"></td>

                            </tr>
                            <?php
                            foreach ($get_dataDpaRekLv2 as $lv2) {
                                $kdlv1 = substr($lv2->kdper_rep, 0, 1);
                                if ($kdlv1 == $lv1->kdper_rep) {
                                    $keu_realisasi_lv2 = 0;
                                    $ttl_realisasi_lv2 = 0;
                                    $ttl_realisasi_bln_ll_lv2 = 0;
                                    foreach ($get_dataDpaRekLv3 as $lv3) {
                                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                                        if ($kdlv2 == $lv2->kdper_rep) {
                                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                                if ($kdlv3 == $lv3->kdper_rep) {
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $keu_realisasi_lv2 += $dpa->keu_realisasi;
                                                                    $ttl_realisasi_lv2 += $dpa->ttl_realisasi;
                                                                    $ttl_realisasi_bln_ll_lv2 += $dpa->ttl_realisasi_bln_ll;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <tr class="bg-gray-active">
                                        <td><?= $lv2->kdper; ?></td>
                                        <td><?= $lv2->nmper; ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right"><?= numberFormat($lv2->nilai); ?></td>
                                        <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll_lv2); ?></td>
                                        <td class="text-right"><?= numberFormat($keu_realisasi_lv2); ?></td>
                                        <td class="text-right"><?= numberFormat($keu_realisasi_lv2+$ttl_realisasi_bln_ll_lv2); ?></td>
                                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv2); ?></td>
                                        <td class="text-right"><?= numberFormat($lv2->nilai - $ttl_realisasi_lv2); ?></td>
                                        <td class="text-center"></td>
                                    </tr>
                                    <?php
                                    foreach ($get_dataDpaRekLv3 as $lv3) {
                                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                                        if ($kdlv2 == $lv2->kdper_rep) {
                                            $keu_realisasi_lv3 = 0;
                                            $ttl_realisasi_lv3 = 0;
                                            $ttl_realisasi_bln_ll_lv3 = 0;
                                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                                if ($kdlv3 == $lv3->kdper_rep) {
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $keu_realisasi_lv3 += $dpa->keu_realisasi;
                                                                    $ttl_realisasi_lv3 += $dpa->ttl_realisasi;
                                                                    $ttl_realisasi_bln_ll_lv3 += $dpa->ttl_realisasi_bln_ll;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                            <tr class="bg-gray">
                                                <td><?= $lv3->kdper; ?></td>
                                                <td><?= $lv3->nmper; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right"><?= numberFormat($lv3->nilai); ?></td>
                                                <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll_lv3); ?></td>
                                                <td class="text-right"><?= numberFormat($keu_realisasi_lv3); ?></td>
                                                <td class="text-right"><?= numberFormat($keu_realisasi_lv3+$ttl_realisasi_bln_ll_lv3); ?></td>
                                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv3); ?></td>
                                                <td class="text-right"><?= numberFormat($lv3->nilai - $ttl_realisasi_lv3); ?></td>
                                                <td class="text-center"></td>
                                            </tr>
                                            <?php
                                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                                if ($kdlv3 == $lv3->kdper_rep) {
                                                    $keu_realisasi_lv4 = 0;
                                                    $ttl_realisasi_lv4 = 0;
                                                    $ttl_realisasi_bln_ll_lv4 = 0;
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $keu_realisasi_lv4 += $dpa->keu_realisasi;
                                                                    $ttl_realisasi_lv4 += $dpa->ttl_realisasi;
                                                                    $ttl_realisasi_bln_ll_lv4 += $dpa->ttl_realisasi_bln_ll;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <tr class="bg-gray">
                                                        <td><?= $lv4->kdper; ?></td>
                                                        <td><?= $lv4->nmper; ?></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="text-right"><?= numberFormat($lv4->nilai); ?></td>
                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll_lv4); ?></td>
                                                        <td class="text-right"><?= numberFormat($keu_realisasi_lv4); ?></td>
                                                        <td class="text-right"><?= numberFormat($keu_realisasi_lv4+$ttl_realisasi_bln_ll_lv4); ?></td>
                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv4); ?></td>
                                                        <td class="text-right"><?= numberFormat($lv4->nilai - $ttl_realisasi_lv4); ?></td>
                                                        <td class="text-center"></td>
                                                    </tr>
                                                    <?php
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            $keu_realisasi_lv5 = 0;
                                                            $ttl_realisasi_bln_ll_lv5 = 0;
                                                            $ttl_realisasi_lv5 = 0;
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $keu_realisasi_lv5 += $dpa->keu_realisasi;
                                                                    $ttl_realisasi_bln_ll_lv5 += $dpa->ttl_realisasi_bln_ll;
                                                                    $ttl_realisasi_lv5 += $dpa->ttl_realisasi;
                                                                }
                                                            }
                                                            ?>
                                                            <tr class="bg-gray">
                                                                <td><?= $lv5->kdper; ?></td>
                                                                <td><?= $lv5->nmper; ?></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="text-right"><?= numberFormat($lv5->nilai); ?></td>
                                                                <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll_lv5); ?></td>
                                                                <td class="text-right"><?= numberFormat($keu_realisasi_lv5); ?></td>
                                                                <td class="text-right"><?= numberFormat($keu_realisasi_lv5+$ttl_realisasi_bln_ll_lv5); ?></td>
                                                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv5); ?></td>
                                                                <td class="text-right"><?= numberFormat($lv5->nilai - $ttl_realisasi_lv5); ?></td>
                                                                <td class="text-center"></td>
                                                            </tr>
                                                           <?php
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    $jumbyek = $dpa->jumbyek;
                                                                    $satuan = $dpa->satuan;
                                                                    $ttl_volume = !is_null($dpa->ttl_volume) ? $dpa->ttl_volume : 0;
                                                                    $rea_volume = !is_null($dpa->rea_volume) ? $dpa->rea_volume : 0;
                                                                    $ttl_realisasi = !is_null($dpa->ttl_realisasi) ? $dpa->ttl_realisasi : 0;
                                                                    $keu_realisasi = !is_null($dpa->keu_realisasi) ? $dpa->keu_realisasi : 0;
                                                                    $ttl_realisasi_bln_ll = !is_null($dpa->ttl_realisasi_bln_ll) ? $dpa->ttl_realisasi_bln_ll : 0;
                                                                    $sisa_volume = $dpa->jumbyek - $ttl_volume;
                                                                    $sisa_pagu = $dpa->subtotal - $ttl_realisasi;
                                                                    if ($dpa->type == 'D') {
                                                                        $bg_color = '';
                                                                    }else{
                                                                        $bg_color = '#dad8d8';
                                                                    }
                                                                    ?>
                                                            <tr style="background-color: <?=$bg_color;?> ">
                                                                        <td></td>
                                                                        <td><?= $dpa->uraian; ?></td>
                                                                        <td class="text-center"><?= $jumbyek ?></td>
                                                                        <td class="text-center"><?= $satuan; ?></td>
                                                                        <td class="text-right"><?= numberFormat($dpa->tarif); ?></td>
                                                                        <td class="text-right"><?= numberFormat($dpa->subtotal); ?></td>
                                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll); ?></td>
                                                                        <td class="text-right"><?= numberFormat($keu_realisasi); ?></td>
                                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll+$keu_realisasi); ?></td>
                                                                        <td class="text-right"><?= numberFormat($ttl_realisasi); ?></td>
                                                                        <td class="text-right"><?= numberFormat($sisa_pagu); ?></td>
                                                                        <td class="text-center no-padding">
                                                                            <?php if ($dpa->type == 'D') { ?>
                                                                                <form class="" method="GET" action="<?= site_url('dpa/Data_input/input_detail'); ?>">
                                                                                    <input type="hidden" class="form-control" name="kd_bulan" value="<?= $kd_bulan; ?>">
                                                                                    <input type="hidden" class="form-control" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                                                                                    <input type="hidden" class="form-control " name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                                                                                    <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                                                                                    <input type="hidden" class="form-control" name="mtgkey" value="<?= $dpa->mtgkey; ?>">
                                                                                    <input type="hidden" class="form-control" name="kdnilai" value="<?= $dpa->kdnilai; ?>">
                                                                                    <button class="btn btn-primary btn-flat btn-block">Input Data</button>
                                                                                </form>
                                                                            <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function postingData(kd_jabatan, kd_bulan, kdkegunit, nm_bulan, nmkegunit) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        });
        swalWithBootstrapButtons({
            title: `Apakah Anda yakin akan memposting pada Bulan ${nm_bulan} dan Kegiatan ${nmkegunit}`,
            text: "Silahkan Klik Tombol Posting untuk melakukan Aksi ini",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Posting ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('posting/Post_kegiatan/postingKegiatan'); ?>",
                    data: {kd_jabatan: kd_jabatan, kd_bulan: kd_bulan, kdkegunit: kdkegunit, status_posting: 'Y'},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi',
                        'error'
                        );
            }
        });
    }
</script>