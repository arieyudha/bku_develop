<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">

    <div class="col-md-12">
        <div class="panel ">
            <div class="panel-body no-padding">
                <table class="" border="1">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2">DOKUMEN PELAKSANAAN ANGGARAN<br>SATUAN KERJA PERANGKAT DAERAH</th>
                            <th class="text-center" width="20%">NOMOR DPA SKPD</th>
                            <th class="text-center" width="10%" rowspan="2">Formulir<br>DPA-SKPD 2.2.1</th>
                        </tr>
                        <tr>
                            <th class="text-center"><?= $row_skpd->kdunit . $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . '5.2'; ?></th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3">PEMERINTAH PROVINSI KALIMANTAN SELATAN<BR>TAHUN ANGGARAN <?= $tahun; ?></th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="3" style="padding: 2px"></th>
                        </tr>
                    </thead>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <td width="15%">Urusan Pemerintahan</td>
                            <td widtd="1%" class="text-center">:</td>
                            <td ><?= $row_bidang->kdunit . ' - ' . $row_bidang->nmunit; ?></td>
                        </tr>
                        <tr>
                            <td >Organisasi</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . ' - ' . $row_unit->nmunit; ?></td>
                        </tr>
                        <tr>
                            <td >Program</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . ' - ' . $row_prog->nmprgrm; ?></td>
                        </tr>
                        <tr>
                            <td>Kegiatan</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . ' - ' . $row_keg->nmkegunit; ?></td>
                        </tr>
                        <tr>
                            <td>Waktu Pelaksanaan</td>
                            <td class="text-center">:</td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td>Lokasi Kegiatan</td>
                            <td class="text-center">:</td>
                            <td ><?= $row_keg->lokasi; ?></td>
                        </tr>
                        <tr>
                            <td>Sumber Dana</td>
                            <td class="text-center">:</td>
                            <td >PAD</td>
                        </tr>
                    </tbody>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <th class="text-center" colspan="3">Indikator & Tolok Ukur Kinerja Belanja Langsung</th>
                        </tr>
                        <?php foreach ($get_dataDpaKinkeg as $row_kinkeg) { ?>
                            <tr>
                                <th><?= $row_kinkeg->urjkk; ?></th>
                                <td><?= $row_kinkeg->tolokur; ?></td>
                                <td><?= $row_kinkeg->target; ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3">Kelompok Sasaran Kegiatan : <?= $row_keg->sasaran; ?></td>
                        </tr>
                    </tbody>
                </table>
                <table class="" border="1">
                    <tbody>
                        <tr>
                            <th class="text-center" colspan="6">Rancangan Rincian Dokumen Pelaksanaan Anggaran Belanja
                                <br>Menurut Program dan Per Kegiatan Satuan Kerja Perangkat Daerah</th>
                        </tr>
                        <tr>
                            <th class="text-center" rowspan="2" width="10%">Rekening</th>
                            <th class="text-center" rowspan="2">Uraian</th>
                            <th class="text-center" colspan="3">Rincian Perhitungan</th>
                            <th class="text-center" rowspan="2">Jumlah (Rp)</th>
                        </tr>
                        <tr>
                            <th class="text-center">Volume</th>
                            <th class="text-center">Satuan</th>
                            <th class="text-center">Harga Satuan</th>
                        </tr>
                        <tr>
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <th class="text-center"><?= $i; ?></th>
                            <?php } ?>
                            <th class="text-center">6 = 3 x 5</th>
                        </tr>
                        <?php foreach ($get_dataDpaRekLv1 as $lv1) { ?>
                            <tr class="bg-gray-active">
                                <td><?= $lv1->kdper; ?></td>
                                <td><?= $lv1->nmper; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><?= numberFormat($lv1->nilai); ?></td>
                            </tr>
                            <?php
                            foreach ($get_dataDpaRekLv2 as $lv2) {
                                $kdlv1 = substr($lv2->kdper_rep, 0, 1);
                                if ($kdlv1 == $lv1->kdper_rep) {
                                    ?>
                                    <tr class="bg-gray-active">
                                        <td><?= $lv2->kdper; ?></td>
                                        <td><?= $lv2->nmper; ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right"><?= numberFormat($lv2->nilai); ?></td>
                                    </tr>
                                    <?php
                                    foreach ($get_dataDpaRekLv3 as $lv3) {
                                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                                        if ($kdlv2 == $lv2->kdper_rep) {
                                            ?>
                                            <tr class="bg-gray">
                                                <td><?= $lv3->kdper; ?></td>
                                                <td><?= $lv3->nmper; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right"><?= numberFormat($lv3->nilai); ?></td>
                                            </tr>
                                            <?php
                                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                                if ($kdlv3 == $lv3->kdper_rep) {
                                                    ?>
                                                    <tr class="bg-gray">
                                                        <td><?= $lv4->kdper; ?></td>
                                                        <td><?= $lv4->nmper; ?></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td class="text-right"><?= numberFormat($lv4->nilai); ?></td>
                                                    </tr>
                                                    <?php
                                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                                        if ($kdlv4 == $lv4->kdper_rep) {
                                                            ?>
                                                            <tr class="bg-gray">
                                                                <td><?= $lv5->kdper; ?></td>
                                                                <td><?= $lv5->nmper; ?></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="text-right"><?= numberFormat($lv5->nilai); ?></td>
                                                            </tr>
                                                            <?php
                                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                                    if($dpa->type == 'H'){
                                                                        $bg_color = '#dedddd';
                                                                        $jumbyek = '';
                                                                        $satuan = '';
                                                                    }else{
                                                                        $bg_color = '';
                                                                        $jumbyek = $dpa->jumbyek;
                                                                        $satuan = $dpa->satuan;
                                                                    }
                                                                    ?>
                                                            <tr style="background-color: <?=$bg_color;?>">
                                                                        <td></td>
                                                                        <td><?= $dpa->uraian; ?></td>
                                                                        <td class="text-center"><?= $jumbyek ?></td>
                                                                        <td class="text-center"><?= $satuan; ?></td>
                                                                        <td class="text-right"><?= numberFormat($dpa->tarif); ?></td>
                                                                        <td class="text-right"><?= numberFormat($dpa->subtotal); ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
