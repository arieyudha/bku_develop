<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading bg-gray">
                <h3 class="panel-title">Data Program dan Kegiatan</h3>
            </div>
            <div class="panel-body">
                <table class="table table-hover table-bordered tabel_3" width='100%'>
                    <thead>
                        <tr>
                            <th width="5%">Kode</th>
                            <th>Nama Program / Kegiatan</th>
                            <th>Pagu Anggaran</th>
                            <th width="15%"><i class="fa fa-refresh"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $pagu_apbd = 0;
                        foreach ($get_dataDpaProgram as $row_prog) {
                            $pagu_prog = 0;
                            foreach ($get_dataDpaKegiatan as $row_keg) {
                                if ($row_prog->idprgrm == $row_keg->idprgrm) {
                                    $pagu_prog += $row_keg->pagu;
                                }
                            }
                            $pagu_apbd += $pagu_prog;
                            ?>
                            <tr style="background-color: #e2e1e1">
                                <td><?= $row_prog->nuprgrm; ?></td>
                                <td><?= $row_prog->nmprgrm; ?></td>
                                <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_prog); ?></td>
                                <td></td>
                            </tr>
                            <?php
                            foreach ($get_dataDpaKegiatan as $row_keg) {
                                if ($row_prog->idprgrm == $row_keg->idprgrm) {
                                    ?>
                                    <tr>
                                        <td><?= $row_prog->nuprgrm . $row_keg->nukeg; ?></td>
                                        <td><?= $row_keg->nmkegunit; ?></td>
                                        <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->pagu); ?></td>
                                        <td class="no-padding">
                                            <form method="GET" action="<?= site_url('dpa/Data_dpa/program_kegiatan_dpa'); ?>">
                                                <input type="hidden" class="form-control" name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                                                <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                                                <button class="btn btn-primary btn-flat btn-block">
                                                    <i class="fa fa-search"></i> View DPA
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">TOTAL</th>
                            <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_apbd); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
