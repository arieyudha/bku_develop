<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data_dpa
 *
 * @author BappedaKalsel
 */
class Data_dpa extends MY_Controller {

    //put your code here
    var $a;
    var $kdtahap;
    var $unitkey;
    var $kdbidang;
    var $kdskpd;

    public function __construct() {
        parent::__construct();
        $this->kdtahap = $this->getKdTahap()->kdtahap;
        $this->unitkey = $this->getUnitkey();
        $this->kdbidang = $this->getKdBidang();
        $this->kdskpd = $this->getKdSkpd();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model(['Model_dpa', 'utility/Model_utility', 'master/Model_ref']);
        }
    }

    function program_kegiatan() {
        $record = $this->javasc_back();
        $record['get_dataDpaProgram'] = $this->Model_dpa->get_dataDpaProgram($this->a['tahun'], $this->kdtahap, $this->unitkey)->result();
        $record['get_dataDpaKegiatan'] = $this->Model_dpa->get_dataDpaKegiatan($this->a['tahun'], $this->kdtahap, $this->unitkey)->result();
        $data = $this->layout_back('view_program_kegiatan', $record);
        $data['ribbon_left'] = ribbon_left('DPA', 'Program dan kegiatan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function program_kegiatan_dpa() {
        $record = $this->javasc_back();
        $idprgrm = isset($_REQUEST['idprgrm']) ? $_REQUEST['idprgrm'] : '';
        $kdkegunit = isset($_REQUEST['kdkegunit']) ? $_REQUEST['kdkegunit'] : '';
        $record['tahun'] = $this->a['tahun'];
        $record['row_unit'] = $this->Model_ref->get_dataDaftUnit($this->unitkey)->row();
        $record['row_skpd'] = $this->Model_ref->get_dataDaftUnitKd($this->kdskpd)->row();
        $record['row_bidang'] = $this->Model_ref->get_dataDaftUnitKd($this->kdbidang)->row();
        $record['row_prog'] = $this->Model_dpa->get_dataDpaProgram($this->a['tahun'], $this->kdtahap, $this->unitkey, $idprgrm)->row();
        $record['row_keg'] = $this->Model_dpa->get_dataDpaKegiatan($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->row();
        $record['get_dataDpaKinkeg'] = $this->Model_dpa->get_dataDpaKinkeg($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv1'] = $this->Model_dpa->get_dataDpaRekLv1($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv2'] = $this->Model_dpa->get_dataDpaRekLv2($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv3'] = $this->Model_dpa->get_dataDpaRekLv3($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv4'] = $this->Model_dpa->get_dataDpaRekLv4($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv5'] = $this->Model_dpa->get_dataDpaRekLv5($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRincDet'] = $this->Model_dpa->get_dataDpaRincDet($this->a['tahun'], $this->kdtahap, $this->unitkey, $kdkegunit)->result();

        $data = $this->layout_back('view_program_kegiatan_dpa', $record);
        $data['ribbon_left'] = ribbon_left('DPA', 'Dokumen Pelaksanaan Anggaran');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }



}
