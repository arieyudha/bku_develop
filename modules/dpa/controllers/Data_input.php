<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data_input
 *
 * @author BappedaKalsel
 */
class Data_input extends MY_Controller {

    //put your code here
    var $a;
    var $kdtahap;
    var $unitkey;
    var $kdbidang;
    var $kdskpd;

    public function __construct() {
        parent::__construct();
        $this->unitkey = $this->getUnitkey();
        $this->kdbidang = $this->getKdBidang();
        $this->kdskpd = $this->getKdSkpd();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model(['Model_dpa',
                'posting/Model_posting',
                'utility/Model_utility',
                'master/Model_ref', 'Model_realisasi']);
        }
    }

    function index() {
        $record = $this->javasc_back();
        $kd_jabatan = $this->getKdJabatanPptk($this->a['kd_level']);
        $kd_bulan = isset($_REQUEST['kd_bulan']) ? $_REQUEST['kd_bulan'] : date('m');
        $record['kd_jabatan'] = $kd_jabatan;
        $record['tahun'] = $this->a['tahun'];
        $record['kd_bulan'] = $kd_bulan;
        $record['get_dataPptkJabatan'] = $this->get_dataPptk($this->a['kd_level'], $kd_jabatan);
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        if (!empty($kd_jabatan)) {
            $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
            $record['get_dataDpaProgram'] = $this->Model_dpa->get_dataDpaProgramPptk($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kd_jabatan)->result();
            $record['get_dataReaKegPptk'] = $this->Model_realisasi->get_dataReaKegPptk($this->a['tahun'], $row_kdtahap->kdtahap, $kd_bulan, $this->unitkey, $kd_jabatan)->result();
        }
        $data = $this->layout_back('input/view_input_prog_keg', $record);
        $data['ribbon_left'] = ribbon_left('Input Data DPA', 'Program dan kegiatan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function input_dpa() {
        $record = $this->javasc_back();
        $kd_jabatan = $this->input->get('kd_jabatan');
        $kd_bulan = $this->input->get('kd_bulan');
        $idprgrm = $this->input->get('idprgrm');
        $kdkegunit = $this->input->get('kdkegunit');
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $record['tahun'] = $this->a['tahun'];
        $record['kd_jabatan'] = $kd_jabatan;
        $record['kd_bulan'] = $kd_bulan;
        $record['idprgrm'] = $idprgrm;
        $record['kdkegunit'] = $kdkegunit;
        $record['kd_tahap'] = $row_kdtahap->kdtahap;
        $record['row_unit'] = $this->Model_ref->get_dataDaftUnit($this->unitkey)->row();
        $record['row_skpd'] = $this->Model_ref->get_dataDaftUnitKd($this->kdskpd)->row();
        $record['row_bidang'] = $this->Model_ref->get_dataDaftUnitKd($this->kdbidang)->row();
        $record['row_prog'] = $this->Model_dpa->get_dataDpaProgram($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $idprgrm)->row();
        $record['row_keg'] = $this->Model_posting->get_dataPostingKegiatan($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan)->row();
        $record['get_dataDpaKinkeg'] = $this->Model_dpa->get_dataDpaKinkeg($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv1'] = $this->Model_dpa->get_dataDpaRekLv1($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv2'] = $this->Model_dpa->get_dataDpaRekLv2($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv3'] = $this->Model_dpa->get_dataDpaRekLv3($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv4'] = $this->Model_dpa->get_dataDpaRekLv4($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv5'] = $this->Model_dpa->get_dataDpaRekLv5($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRincDet'] = $this->Model_realisasi->get_dataReaDpaRincDet($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan)->result();
        $data = $this->layout_back('input/view_input_realisasi', $record);
        $data['ribbon_left'] = ribbon_left('DPA', 'Input Data Realisasi');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function input_detail() {
        $record = $this->javasc_back();
        $kd_jabatan = $this->input->get('kd_jabatan');
        $kd_bulan = $this->input->get('kd_bulan');
        $idprgrm = $this->input->get('idprgrm');
        $kdkegunit = $this->input->get('kdkegunit');
        $kdnilai = $this->input->get('kdnilai');
        $mtgkey = $this->input->get('mtgkey');
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $record['tahun'] = $this->a['tahun'];
        $record['kd_jabatan'] = $kd_jabatan;
        $record['kd_bulan'] = $kd_bulan;
        $record['idprgrm'] = $idprgrm;
        $record['kdkegunit'] = $kdkegunit;
        $record['kdnilai'] = $kdnilai;
        $record['mtgkey'] = $mtgkey;
        $record['kd_tahap'] = $row_kdtahap->kdtahap;
        $record['row_unit'] = $this->Model_ref->get_dataDaftUnit($this->unitkey)->row();
        $record['row_post'] = $this->Model_posting->get_lockPptkWhereThnBlnJbtn($this->a['tahun'], $kd_bulan, $kd_jabatan)->row();
        $record['row_skpd'] = $this->Model_ref->get_dataDaftUnitKd($this->kdskpd)->row();
        $record['row_bidang'] = $this->Model_ref->get_dataDaftUnitKd($this->kdbidang)->row();
        $record['row_prog'] = $this->Model_dpa->get_dataDpaProgram($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $idprgrm)->row();
        $record['row_keg'] = $this->Model_posting->get_dataPostingKegiatan($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan)->row();
        $record['row_lv5'] = $this->Model_dpa->get_dataDpaRekLv5($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $mtgkey)->row();
        $record['row_det'] = $this->Model_realisasi->get_dataReaDpaRincDet($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan, $kdnilai)->row();
        $record['get_dataRincDetWhereMtgkey'] = $this->Model_dpa->get_dataRincDetWhereMtgkey($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $mtgkey)->result();
        $record['get_dataRealisasi'] = $this->Model_realisasi->get_dataRealisasi($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kdnilai, $mtgkey)->result();
        $data = $this->layout_back('input/view_input_detail', $record);
        $data['ribbon_left'] = ribbon_left('DPA', 'Input Data Realisasi');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function inputRealisasi() {
        $url = $this->input->post('url');
        $data['unitkey'] = $this->unitkey;
        $kd_rea = $this->post('kd_rea');
        $status_lock = $this->post('status_lock');
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['mtgkey'] = $this->post('mtgkey');
        $data['tahun'] = $this->a['tahun'];
        $data['kdnilai'] = $this->post('kdnilai');
        $data['kdtahap'] = $this->post('kdtahap');
        $data['kd_bulan'] = $this->post('kd_bulan');
        $data['keu_realisasi'] = $this->post('keu_realisasi');
        $data['harga_satuan'] = $this->post('harga_satuan');
        $data['uraian'] = $this->post('uraian');
        $data['volume'] = $this->post('rea_volume');
        $data['tgl_input'] = date('Y-m-d');
        if ($status_lock == "N") {
            if ($kd_rea == null) {
                $query = $this->insert_duplicate('data_realisasi', $data);
                $ket = 'Menambahkan Realisasi';
            } else {
                $query = $this->update('kd_rea', $kd_rea, 'data_realisasi', $data);
                $ket = 'Update Ralisasi';
            }
            if ($query) {
                $info = 'Berhasil;';
            } else {
                $info = 'Gagal';
            }
        } else {
            $ket = 'Status Anda Sedang Terkunci, Anda Tidak Boleh melakukan Input Data';
            $info = 'Gagal';
        }

        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteRealisasi() {
        $kd_rea = $this->post('kd_rea');
        $status_lock = $this->post('status_lock');
        if ($status_lock == 'N') {
            $query = $this->delete('kd_rea', $kd_rea, 'data_realisasi');
            if ($query) {
                echo 'true';
            } else {
                echo 'false';
            }
        } else {
            echo 'false';
        }
    }

}
