<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_realisasi extends CI_Model {

    //put your code here

    function get_dataDpaProgram($tahun, $kdtahap, $unitkey, $idprgrm = null) {
        if ($idprgrm == '') {
            $query = $this->db->query("select * from sip_pgrmunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' order by a.nuprgrm");
        } else {
            $query = $this->db->query("select * from sip_pgrmunit a where a.kdtahap=$kdtahap and a.tahun=$tahun 
                and a.unitkey='$unitkey' and a.idprgrm='$idprgrm' order by a.nuprgrm");
        }
        return $query;
    }

    function get_dataDpaProgramPptk($tahun, $kdtahap, $unitkey, $kd_jabatan = null) {
        if ($kd_jabatan == '') {
            $query = $this->db->query("select a.*, c.kd_jabatan, sum(b.pagu) as pagu_prog from sip_pgrmunit a 
join sip_kegunit b on b.idprgrm=a.idprgrm and a.tahun=b.tahun and a.unitkey=b.unitkey and a.kdtahap=b.kdtahap 
join pptk_kegunit c on c.kdtahap=b.kdtahap and c.unitkey=b.unitkey and c.kdkegunit=b.kdkegunit and c.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' group by a.idprgrm order by a.nuprgrm");
        } else {
            $query = $this->db->query("select a.*, c.kd_jabatan, sum(b.pagu) as pagu_prog from sip_pgrmunit a 
join sip_kegunit b on b.idprgrm=a.idprgrm and a.tahun=b.tahun and a.unitkey=b.unitkey and a.kdtahap=b.kdtahap 
join pptk_kegunit c on c.kdtahap=b.kdtahap and c.unitkey=b.unitkey and c.kdkegunit=b.kdkegunit and c.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and c.kd_jabatan=$kd_jabatan
group by a.idprgrm order by a.nuprgrm");
        }
        return $query;
    }

    function get_dataDpaKegiatan($tahun, $kdtahap, $unitkey, $kdkegunit = null) {
        if ($kdkegunit == '') {
            $query = $this->db->query("select * from sip_kegunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' order by a.nukeg  ");
        } else {
            $query = $this->db->query("select * from sip_kegunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' order by a.nukeg  ");
        }
        return $query;
    }

    function get_dataDpaKegiatanPptk($tahun, $kdtahap, $unitkey, $kd_jabatan) {
        $query = $this->db->query("select a.*, b.kd_jabatan, b.kd_pptk from sip_kegunit a 
join pptk_kegunit b on a.unitkey=b.unitkey and a.kdtahap=b.kdtahap and a.kdkegunit=b.kdkegunit and a.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and b.kd_jabatan=$kd_jabatan order by a.nukeg");
        return $query;
    }

    function get_dataReaKegPptk($tahun, $kdtahap, $kd_bulan, $unitkey, $kd_jabatan = null) {
        if ($kd_jabatan == '') {
            $query = $this->db->query("select a.*, b.kd_jabatan, b.kd_pptk, sum(d.keu_realisasi) as keu_realisasi, c.ttl_realisasi, if(isnull(e.status_posting), 'N',e.status_posting) as status_posting,
f.kode_qr, f.tgl_laporan
from sip_kegunit a 
join pptk_kegunit b on a.unitkey=b.unitkey and a.kdtahap=b.kdtahap and a.kdkegunit=b.kdkegunit and a.tahun=b.tahun 
left join post_kegiatan e on e.unitkey=a.unitkey and a.kdtahap=e.kdtahap and a.kdkegunit=e.kdkegunit and a.tahun=e.tahun and e.kd_bulan=$kd_bulan 
left join data_realisasi d on d.unitkey=a.unitkey and a.kdkegunit=d.kdkegunit and a.tahun=d.tahun and d.kd_bulan=$kd_bulan 
left join (select aa.unitkey, aa.kdkegunit, aa.tahun, aa.kdtahap, sum(aa.volume) as ttl_volume, 
sum(aa.keu_realisasi) as ttl_realisasi from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.tahun) c 
on a.unitkey=c.unitkey and a.kdkegunit=c.kdkegunit and a.tahun=c.tahun 
left join data_laporan_realisasi f on f.unitkey=a.unitkey and f.kdkegunit=a.kdkegunit  and f.tahun=a.tahun and f.kdtahap=a.kdtahap and f.kd_bulan=$kd_bulan 
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' 
group by a.unitkey, a.kdkegunit, a.tahun, a.kdtahap, a.idprgrm order by a.nukeg");
        } else {
            $query = $this->db->query("select a.*, b.kd_jabatan, b.kd_pptk, sum(d.keu_realisasi) as keu_realisasi, c.ttl_realisasi, if(isnull(e.status_posting), 'N',e.status_posting) as status_posting,
f.kode_qr, f.tgl_laporan
from sip_kegunit a 
join pptk_kegunit b on a.unitkey=b.unitkey and a.kdtahap=b.kdtahap and a.kdkegunit=b.kdkegunit and a.tahun=b.tahun 
left join post_kegiatan e on e.unitkey=a.unitkey and a.kdtahap=e.kdtahap and a.kdkegunit=e.kdkegunit and a.tahun=e.tahun and e.kd_bulan=$kd_bulan
left join data_realisasi d on d.unitkey=a.unitkey and a.kdkegunit=d.kdkegunit and a.tahun=d.tahun and d.kd_bulan=$kd_bulan
left join (select aa.unitkey, aa.kdkegunit, aa.tahun, aa.kdtahap, sum(aa.volume) as ttl_volume, 
sum(aa.keu_realisasi) as ttl_realisasi from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.tahun) c 
on a.unitkey=c.unitkey and a.kdkegunit=c.kdkegunit and a.tahun=c.tahun 
left join data_laporan_realisasi f on f.unitkey=a.unitkey and f.kdkegunit=a.kdkegunit  and f.tahun=a.tahun and f.kdtahap=a.kdtahap and f.kd_bulan=$kd_bulan 
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and b.kd_jabatan=$kd_jabatan 
group by a.unitkey, a.kdkegunit, a.tahun, a.kdtahap, a.idprgrm order by a.nukeg");
        }

        return $query;
    }

    function get_dataDpaKinkeg($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select * from sip_kinkeg a where a.tahun=$tahun and a.kdtahap=$kdtahap and a.kdkegunit='$kdkegunit' and a.unitkey='$unitkey'");
        return $query;
    }

    function get_dataDpaRekLv1($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=1) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,1)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv2($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=2) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,2)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv3($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=3) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,3)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv4($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=4) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,5)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv5($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, a.mtgkey, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=5) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,7)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataReaDpaRincDet($tahun, $kdtahap, $unitkey, $kdkegunit, $kd_bulan, $kdnilai = null) {
        if ($kdnilai == '') {
            $query = $this->db->query("select a.*, b.kd_bulan, b.volume as rea_volume, b.keu_realisasi, b.tgl_input, c.ttl_volume, c.ttl_realisasi,
sum(d.ttl_realisasi_bln_ll) AS ttl_realisasi_bln_ll, sum(d.ttl_volume_bln_ll) AS ttl_volume_bln_ll
from sip_daskdetr a
left join data_realisasi b on b.unitkey=a.unitkey and a.kdkegunit=b.kdkegunit and a.mtgkey=b.mtgkey and a.kdnilai=b.kdnilai and a.tahun=b.tahun and b.kd_bulan=$kd_bulan
left join (select aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun, aa.kdtahap, sum(aa.volume) as ttl_volume,
sum(aa.keu_realisasi) as ttl_realisasi from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun)
c on a.unitkey=c.unitkey and a.kdkegunit=c.kdkegunit and a.mtgkey=c.mtgkey and a.kdnilai=c.kdnilai and a.tahun=c.tahun
left join (select aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun, aa.kd_bulan, aa.kdtahap, sum(aa.volume) as ttl_volume_bln_ll,
sum(aa.keu_realisasi) as ttl_realisasi_bln_ll from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun, aa.kd_bulan)
d on a.unitkey=d.unitkey and a.kdkegunit=d.kdkegunit and a.mtgkey=d.mtgkey and a.kdnilai=d.kdnilai and a.tahun=d.tahun and d.kd_bulan<$kd_bulan
where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' 
    group by a.unitkey, a.kdkegunit, a.mtgkey, a.kdnilai, a.tahun, a.kdtahap order by kdjabar");
        } else {
            $query = $this->db->query("select a.*, b.kd_bulan, sum(b.volume) as rea_volume, sum(b.keu_realisasi) as keu_realisasi, b.tgl_input, c.ttl_volume, c.ttl_realisasi,
d.ttl_realisasi_bln_ll, d.ttl_volume_bln_ll
from sip_daskdetr a
left join data_realisasi b on b.unitkey=a.unitkey and a.kdkegunit=b.kdkegunit and a.mtgkey=b.mtgkey and a.kdnilai=b.kdnilai and a.tahun=b.tahun and b.kd_bulan=$kd_bulan
left join (select aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun, aa.kdtahap, sum(aa.volume) as ttl_volume,
sum(aa.keu_realisasi) as ttl_realisasi from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun)
c on a.unitkey=c.unitkey and a.kdkegunit=c.kdkegunit and a.mtgkey=c.mtgkey and a.kdnilai=c.kdnilai and a.tahun=c.tahun
left join (select aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun, aa.kd_bulan, aa.kdtahap, sum(aa.volume) as ttl_volume_bln_ll,
sum(aa.keu_realisasi) as ttl_realisasi_bln_ll from data_realisasi aa group by aa.unitkey, aa.kdkegunit, aa.mtgkey, aa.kdnilai, aa.tahun)
d on a.unitkey=d.unitkey and a.kdkegunit=d.kdkegunit and a.mtgkey=d.mtgkey and a.kdnilai=d.kdnilai and a.tahun=d.tahun and d.kd_bulan<$kd_bulan
where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' and a.kdnilai='$kdnilai'
group by a.unitkey, a.kdkegunit, a.mtgkey, a.kdnilai, a.tahun, a.kdtahap order by kdjabar");
        }
        return $query;
    }

    function get_dataRealisasi($tahun, $kdtahap, $unitkey, $kdkegunit, $kdnilai, $mtgkey, $kd_rea = null) {
        if ($kd_rea == '') {
            $query = $this->db->query("select a.*, b.nama_bulan, b.kdtriwulan from data_realisasi a 
join uti_bulan b on a.kd_bulan=b.kd_bulan where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' 
and a.kdkegunit='$kdkegunit' and a.mtgkey='$mtgkey' and a.kdnilai='$kdnilai'");
        } else {
            $query = $this->db->query("select a.*, b.nama_bulan, b.kdtriwulan from data_realisasi a 
join uti_bulan b on a.kd_bulan=b.kd_bulan where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' 
and a.kdkegunit='$kdkegunit' and a.mtgkey='$mtgkey' and a.kdnilai='$kdnilai' and a.kd_rea=$kd_rea");
        }
        return $query;
    }

}
