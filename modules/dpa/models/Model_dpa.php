<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_dpa extends CI_Model {

    //put your code here

    function get_dataDpaProgram($tahun, $kdtahap, $unitkey, $idprgrm = null) {
        if ($idprgrm == '') {
            $query = $this->db->query("select * from sip_pgrmunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' order by a.nuprgrm");
        } else {
            $query = $this->db->query("select * from sip_pgrmunit a where a.kdtahap=$kdtahap and a.tahun=$tahun 
                and a.unitkey='$unitkey' and a.idprgrm='$idprgrm' order by a.nuprgrm");
        }
        return $query;
    }

    function get_dataDpaProgramPptk($tahun, $kdtahap, $unitkey, $kd_jabatan = null) {
        if ($kd_jabatan == '') {
            $query = $this->db->query("select a.*, c.kd_jabatan, sum(b.pagu) as pagu_prog from sip_pgrmunit a 
join sip_kegunit b on b.idprgrm=a.idprgrm and a.tahun=b.tahun and a.unitkey=b.unitkey and a.kdtahap=b.kdtahap 
join pptk_kegunit c on c.kdtahap=b.kdtahap and c.unitkey=b.unitkey and c.kdkegunit=b.kdkegunit and c.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey'
group by a.idprgrm order by a.nuprgrm");
        } else {
            $query = $this->db->query("select a.*, c.kd_jabatan, sum(b.pagu) as pagu_prog from sip_pgrmunit a 
join sip_kegunit b on b.idprgrm=a.idprgrm and a.tahun=b.tahun and a.unitkey=b.unitkey and a.kdtahap=b.kdtahap 
join pptk_kegunit c on c.kdtahap=b.kdtahap and c.unitkey=b.unitkey and c.kdkegunit=b.kdkegunit and c.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and c.kd_jabatan=$kd_jabatan
group by a.idprgrm order by a.nuprgrm");
        }
        return $query;
    }

    function get_dataDpaKegiatan($tahun, $kdtahap, $unitkey, $kdkegunit = null) {
        if ($kdkegunit == '') {
            $query = $this->db->query("select * from sip_kegunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' order by a.nukeg  ");
        } else {
            $query = $this->db->query("select * from sip_kegunit a where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' order by a.nukeg");
        }
        return $query;
    }

    function get_dataDpaKegiatanPptk($tahun, $kdtahap, $unitkey, $kd_jabatan) {
        $query = $this->db->query("select a.*, b.kd_jabatan, b.kd_pptk from sip_kegunit a 
join pptk_kegunit b on a.unitkey=b.unitkey and a.kdtahap=b.kdtahap and a.kdkegunit=b.kdkegunit and a.tahun=b.tahun
where a.kdtahap=$kdtahap and a.tahun=$tahun and a.unitkey='$unitkey' and b.kd_jabatan=$kd_jabatan order by a.nukeg");
        return $query;
    }

    function get_dataDpaKinkeg($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select * from sip_kinkeg a where a.tahun=$tahun and a.kdtahap=$kdtahap and a.kdkegunit='$kdkegunit' and a.unitkey='$unitkey'");
        return $query;
    }

    function get_dataDpaRekLv1($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=1) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,1)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv2($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=2) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,2)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv3($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=3) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,3)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv4($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.unitkey, a.kdkegunit, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=4) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,5)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        return $query;
    }

    function get_dataDpaRekLv5($tahun, $kdtahap, $unitkey, $kdkegunit, $mtgkey = null) {
        if ($mtgkey == '') {
            $query = $this->db->query("select a.unitkey, a.kdkegunit, a.mtgkey, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=5) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,7)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' group by a.unitkey, a.kdkegunit, c.kdper");
        } else {
            $query = $this->db->query("select a.unitkey, a.kdkegunit, a.mtgkey, c.kdper_rep, c.kdper, c.nmper, sum(a.subtotal) as nilai from sip_daskdetr a 
join sip_matangr b on a.mtgkey=b.mtgkey
join  (select aa.kdper, SUBSTR(REPLACE(aa.kdper,'.',''),1,7) as kdper_rep, aa.nmper from sip_matangr aa where aa.mtglevel=5) 
c on c.kdper_rep=SUBSTR(REPLACE(b.kdper,'.',''),1,7)
where a.type='D' and a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' and a.mtgkey='$mtgkey' group by a.unitkey, a.kdkegunit, c.kdper");
        }
        return $query;
    }

    function get_dataDpaRincDet($tahun, $kdtahap, $unitkey, $kdkegunit) {
        $query = $this->db->query("select a.* from sip_daskdetr a
where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' order by kdjabar");
        return $query;
    }

    function get_dataRincDetWhereMtgkey($tahun, $kdtahap, $unitkey, $kdkegunit, $mtgkey) {
        $query = $this->db->query("select a.*
from sip_daskdetr a where a.tahun=$tahun and a.kdtahap=$kdtahap and a.unitkey='$unitkey' and a.kdkegunit='$kdkegunit' 
and a.mtgkey='$mtgkey' and a.type = 'D'
order by kdjabar");
        return $query;
    }

}
