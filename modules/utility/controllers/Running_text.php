<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Running_text
 *
 * @author BappedaKalsel
 */
class Running_text extends MY_Controller {

    //put your code here


    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('Model_utility');
        }
    }

    function index() {
        //record
        $record = $this->javasc_back();
        $record['row_text'] = $this->Model_utility->getRunningText()->row();

        //data
        $data = $this->layout_back('running_text', $record);
        $data['ribbon_left'] = ribbon_left('Utility', 'Periode SKPD');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }
    
    function updateRunningText(){
        $url = $this->input->post('url');
        $id = $this->input->post('id', true);
        $data['keterangan'] = $this->input->post('keterangan', true);
        $query = $this->update('id', $id, 'running_text', $data);
        $ket = 'Update Running Text';
        if($query){
            $this->aktifitas($ket);
            $info = 'Berhasil';
        }else{
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

}
