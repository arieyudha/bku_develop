<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Running_text
 *
 * @author BappedaKalsel
 */
class Jam_absen extends MY_Controller {

    //put your code here


    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('Model_utility');
        }
    }

    function index() {
        //record
        $record = $this->javasc_back();

        $kd_shif = $this->input->get('shif');
        $row_shif = $this->Model_utility->get_shif($kd_shif)->row();
        $record['row_jam'] = $this->Model_utility->getJamAbsen()->row();
        $record['get_shif'] = $this->Model_utility->get_shif()->result();
        $record['kd_shif'] = $kd_shif;
        if ($kd_shif) {
            $record['get_shifJamKerja'] = $this->Model_utility->get_shifJamKerja($row_shif->jum_haker, $kd_shif)->result();
        }

        //data
        $data = $this->layout_back('jam_absen', $record);
        $data['ribbon_left'] = ribbon_left('Utility', 'Jam Absen');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function updateJamAbsen() {
        $url = $this->input->post('url');
        $data['kd_shif'] = $this->input->post('kd_shif', true);
        $data['kd_hari'] = $this->input->post('kd_hari', true);
        $data['jam_masuk'] = $this->input->post('jam_masuk', true);
        $data['jam_pulang'] = $this->input->post('jam_pulang', true);
        $data['interval_masuk'] = $this->input->post('interval_masuk', true);
        $data['interval_pulang'] = $this->input->post('interval_pulang', true);
        $data['dispensasi_masuk'] = $this->input->post('dispensasi_masuk', true);
        $data['dispensasi_pulang'] = $this->input->post('dispensasi_pulang', true);
        $data['keterangan'] = $this->input->post('keterangan', true);
        $query = $this->insert_duplicate('set_jam_absen', $data);
        $ket = 'Insert / Update Jam Absen';
        if ($query) {
            $this->aktifitas($ket);
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

}
