<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Persentase extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('utility/Model_utility');
        }
    }
    
    public function index(){
        //record
        $record = $this->javasc_back();
        $record['rowp'] = $this->Model_utility->getPersentase()->row();
        //data
        $data = $this->layout_back('view_persentase', $record);
        $data['ribbon_left'] = ribbon_left('TPP', 'Persentase');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }
    
    function input_persentase() {
        $url = $this->input->post('url');
        $id = $this->post('id');
        $data['absen'] = $this->post('absen');
        $data['tpp'] = $this->post('tpp');
        $data['index_tpp'] = $this->post('index_tpp');
        $data['kemampuan_daerah'] = $this->post('kemampuan_daerah');
        $que = $this->update('id',$id, 'set_persentase_tpp', $data);
        $ket = 'Merubah persentase absen id : '. $id;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }
//    function input_uang() {
//        $url = $this->input->post('url');
//        $id = $this->input->post('id', true);
//        $data['uang'] = $this->input->post('uang', true);
//        $que = $this->update('id',$id, 'set_persentase_tpp', $data);
//        $ket = 'Merubah uang persentase id : '. $id;
//        if ($que) {
//            $info = 'Berhasil';
//        } else {
//            $info = 'Gagal';
//        }
//        $this->aktifitas($ket);
//        $this->flashdata($ket, $info);
//        redirect($url);
//    }
}