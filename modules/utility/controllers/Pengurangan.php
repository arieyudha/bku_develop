<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurangan extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('utility/Model_utility');
        }
    }

    public function index() {
        //record
        $record = $this->javasc_back();
        $record['getTerlambat'] = $this->Model_utility->getTerlambat()->result();
        $record['getPulangSebelum'] = $this->Model_utility->getPulangSebelum()->result();
        $record['getUtiAktifitas'] = $this->Model_utility->getUtiAktifitas()->result();
        //data
        $data = $this->layout_back('view_pengurangan', $record);
        $data['ribbon_left'] = ribbon_left('TPP', 'Pengurangan Hari Kerja');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function insertAktifitas() {
        $url = $this->post('url');
//        $url = $this->post('kode');
        $data['aktifitas_harian'] = $this->post('aktifitas');
        $data['persen'] = $this->post('persen');
        $data['tipe_data'] = $this->post('tipe_data');
        $query = $this->insert_duplicate('uti_aktifitas', $data);
        $ket = 'Menambah Data Aktifitas Harian';
        if ($query) {
            $this->aktifitas($ket);
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function updateAktifitas() {
        $url = $this->post('url');
        $kode = $this->post('kode');
        $data['aktifitas_harian'] = $this->post('aktifitas');
        $data['persen'] = $this->post('persen');
        $data['tipe_data'] = $this->post('tipe_data');
        $query = $this->update('kode', $kode, 'uti_aktifitas', $data);
        $ket = 'Update Data Aktifitas Harian';
        if ($query) {
            $this->aktifitas($ket);
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteAktifitas() {
        $kode = $this->post('kode');
        $query = $this->delete('kode', $kode, 'uti_aktifitas');
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
