<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('Model_utility');
        }
    }

    public function index() {
        //record
        $record = $this->javasc_back();
        $record['getPeriode'] = $this->Model_utility->getPeriode()->result();

        //data
        $data = $this->layout_back('periode', $record);
        $data['ribbon_left'] = ribbon_left('Utility', 'Periode SKPD');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }
    
    function kunciAktif() {
        $status = $this->input->post('status');
        $tahun = $this->input->post('tahun');
        $col['tahun'] = $tahun;
        if ($status == 'Y') {
            $data['status'] = 'N';
        } else {
            $data['status'] = 'Y';
        }
        $q = $this->update_where('uti_periode', $data, $col);
        if ($q) {
            echo 'true';
        }
    }

    function insertPeriode() {
        $url = $this->input->post('url');
        $tahun = $this->input->post('tahun', true);
        $awal = formatDatePhp($this->input->post('awal', true));
        $akhir = formatDatePhp($this->input->post('akhir', true));
        $data['keterangan'] = $this->input->post('keterangan', true);
        $data['tahun'] = $tahun;
        $data['akhir'] = $akhir;
        $data['awal'] = $awal;
        $que = $this->insert('uti_periode', $data);
        $ket = 'Menambah Data Periode : ' . $tahun;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function updatePeriode() {
        $url = $this->input->post('url');
        $tahun = $this->input->post('tahun', true);
        $awal = formatDatePhp($this->input->post('awal', true));
        $akhir = formatDatePhp($this->input->post('akhir', true));
        $data['keterangan'] = $this->input->post('keterangan', true);
        $data['tahun'] = $tahun;
        $data['akhir'] = $akhir;
        $data['awal'] = $awal;
        $que = $this->update('tahun', $tahun, 'uti_periode', $data);
        $ket = 'Update Data Periode : ' . $tahun;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deletePeriode() {
        $tahun = $this->input->post('tahun', true);
        $query = $this->delete('tahun', $tahun, 'uti_periode');
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
