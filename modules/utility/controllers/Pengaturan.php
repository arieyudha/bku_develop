<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends MY_Controller {

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model('Model_utility');
        }
    }

    public function index() {
        //record
        $record = $this->javasc_back();
        $record['getAgama'] = $this->Model_utility->getAgama()->result();
        $record['getPendidikan'] = $this->Model_utility->getPendidikan()->result();
        $record['getStatusPerkawinan'] = $this->Model_utility->getStatusPerkawinan()->result();


        //data
        $data = $this->layout_back('pengaturan', $record);
        $data['ribbon_left'] = ribbon_left('Utility', 'Pengaturan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function insertAgama() {
        $url = $this->input->post('url');
        $nama = $this->input->post('nama', true);
        $data['nama'] = strtoupper($nama);
        $que = $this->insert('uti_agama', $data);
        $ket = 'Menambah Data Agama : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function updateAgama() {
        $url = $this->input->post('url');
        $id = $this->input->post('id', true);
        $nama = $this->input->post('nama', true);
        $data['nama'] = strtoupper($nama);
        $que = $this->update('id', $id, 'uti_agama', $data);
        $ket = 'Update Data Agama : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteAgama() {
        $id = $this->input->post('id', true);
        $query = $this->delete('id', $id, 'uti_agama');
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function insertStatus() {
        $url = $this->input->post('url');
        $nama = $this->input->post('nama', true);
        $data['nama'] = strtoupper($nama);
        $que = $this->insert('uti_status', $data);
        $ket = 'Menambah Data Status : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function updateStatus() {
        $url = $this->input->post('url');
        $id = $this->input->post('id', true);
        $nama = $this->input->post('nama', true);
        $data['nama'] = strtoupper($nama);
        $que = $this->update('id', $id, 'uti_status', $data);
        $ket = 'Update Data Status : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deleteStatus() {
        $id = $this->input->post('id', true);
        $query = $this->delete('id', $id, 'uti_status');
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    function insertPendidikan() {
        $url = $this->input->post('url');
        $nama = $this->input->post('nama', true);
        $inisial = $this->input->post('inisial', true);
        $data['nama'] = strtoupper($nama);
        $data['inisial'] = $inisial;
        $que = $this->insert('uti_pendidikan', $data);
        $ket = 'Menambah Data Pendidikan : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function updatePendidikan() {
        $url = $this->input->post('url');
        $id = $this->input->post('id', true);
        $nama = $this->input->post('nama', true);
        $inisial = $this->input->post('inisial', true);
        $data['nama'] = strtoupper($nama);
        $data['inisial'] = $inisial;
        $que = $this->update('id', $id, 'uti_pendidikan', $data);
        $ket = 'Update Data Pendidikan : ' . $nama;
        if ($que) {
            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->aktifitas($ket);
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function deletePendidikan() {
        $id = $this->input->post('id', true);
        $query = $this->delete('id', $id, 'uti_pendidikan');
        if ($query) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

}
