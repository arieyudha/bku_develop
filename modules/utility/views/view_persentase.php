<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data Persentase TPP</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="<?= site_url('utility/Persentase/input_persentase') ?>" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Absen</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="absen" class="form-control text-right" value="<?= $rowp->absen ?>"><span class="input-group-addon bg-gray">%</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Kinerja</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="tpp" class="form-control text-right" value="<?= $rowp->tpp ?>">
                                    <span class="input-group-addon bg-gray">%</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Index TPP</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="index_tpp" class="form-control text-right" value="<?= $rowp->index_tpp ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Kemampuan Daerah</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="kemampuan_daerah" class="form-control text-right" value="<?= $rowp->kemampuan_daerah ?>">
                                    <span class="input-group-addon bg-gray">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $rowp->id ?>">
                    <input type="hidden" name="url" value="<?= $url ?>">
                    <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>
        </div>
    </div>
<!--    <div class="col-md-3">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data Perkalian Uang TPP</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="<?= site_url('utility/Persentase/input_uang') ?>" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Index TPP</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="uang" class="form-control" value="<?= $rowp->index_tpp ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $rowp->id ?>">
                    <input type="hidden" name="url" value="<?= $url ?>">
                    <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>
        </div>
    </div>-->
</div>