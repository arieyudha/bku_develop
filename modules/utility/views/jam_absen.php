<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-6">
        <div class="box-header bg-gray">
            <form name='fShif' method='get'>
                <select class="form-control select2 shif" style="width: 100%;" name='shif' onchange='document.fShif.submit();'>
                    <option value="">.: Pilih Shif :.</option>
                    <?php
                    foreach ($get_shif as $row) {
                        if ($row->kd_shif == $kd_shif) {
                            $att = 'selected';
                        } else {
                            $att = '';
                        }
                        echo '<option ' . $att . ' value="' . $row->kd_shif . '">' . $row->nm_shif . '</option>';
                    }
                    ?>
                </select>
            </form>
        </div>
        <?php if ($kd_shif) { ?>
            <div class="panel">
                <form action="<?= site_url('utility/Jam_absen/updateJamAbsen'); ?>" method="post">
                    <div class="panel-heading nm_hari">
                        Form Jam Absen 
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Keterangan :</label>
                                <div class="form-group">
                                    <textarea class="form-control keterangan" name="keterangan" rows="3"></textarea>
                                    <input type="hidden" class="form-control kd_shif" name="kd_shif">
                                    <input type="hidden" class="form-control kd_hari" name="kd_hari">
                                    <input type="hidden" class="form-control" name="url" value="<?= $url; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Jam Masuk :</label>
                                <div class="form-group">
                                    <input class="form-control jam_masuk" type="time" name="jam_masuk">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Jam Pulang :</label>
                                <div class="form-group">
                                    <input class="form-control jam_pulang" type="time" name="jam_pulang">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Interval Masuk :</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control interval_masuk" type="number" name="interval_masuk">
                                        <div class="input-group-addon">Menit</div>
                                    </div>
                                    <note>Waktu Interval Menit</note>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Interval Pulang :</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control interval_pulang" type="number" name="interval_pulang">
                                        <div class="input-group-addon">Menit</div>
                                    </div>
                                    <note>Waktu Interval Menit</note>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Dispensasi Masuk :</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control dispensasi_masuk" type="number" name="dispensasi_masuk">
                                        <div class="input-group-addon">Menit</div>
                                    </div>
                                    <note>Waktu Dispensasi Menit</note>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Dispensasi Pulang :</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control dispensasi_pulang" type="number" name="dispensasi_pulang">
                                        <div class="input-group-addon">Menit</div>
                                    </div>
                                    <note>Waktu Dispensasi Menit</note>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading">
                    Tabel Jam Absen
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Hari</th>
                                        <th>Jam Masuk</th>
                                        <th>Jam Pulang</th>
                                        <th width="5%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($get_shifJamKerja as $jam) { ?>
                                        <tr>
                                            <td><?= $jam->kd_hari ?></td>
                                            <td><?= $jam->nm_hari ?></td>
                                            <td class="text-center"><?= $jam->jam_masuk ?></td>
                                            <td class="text-center"><?= $jam->jam_pulang ?></td>
                                            <td>
                                                <?php
                                                $attrEdit = 'onclick="editStatus(\'' . $jam->nm_hari . '\', \'' . $jam->kd_hari . '\', \'' . $kd_shif . '\', \'' . $jam->keterangan . '\', \'' . $jam->jam_masuk . '\', \'' . $jam->jam_pulang . '\', \'' . $jam->interval_masuk . '\', \'' . $jam->interval_pulang . '\', \'' . $jam->dispensasi_masuk . '\', \'' . $jam->dispensasi_pulang . '\')"';
                                                $ketEdit = '';
                                                $classEdit = 'btn-xs';
                                                btn_edit($attrEdit, $ketEdit, $classEdit);
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script>
    function editStatus(nm_hari, kd_hari, kd_shif, keterangan, jam_masuk, jam_pulang, interval_masuk, interval_pulang, dispensasi_masuk, dispensasi_pulang) {
        $('.nm_hari').html(nm_hari);
        $('.kd_hari').val(kd_hari);
        $('.kd_shif').val(kd_shif);
        $('.keterangan').html(keterangan);
        $('.jam_masuk').val(jam_masuk);
        $('.jam_pulang').val(jam_pulang);
        $('.interval_masuk').val(interval_masuk);
        $('.interval_pulang').val(interval_pulang);
        $('.dispensasi_masuk').val(dispensasi_masuk);
        $('.dispensasi_pulang').val(dispensasi_pulang);
    }
</script>
