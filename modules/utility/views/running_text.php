<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="input-group" style="background-color: #ffffff; border: solid 2px; border-color: #33ff33">
                <span class="input-group-addon bg-green-gradient" style="font-size: 14pt">Informasi :</span>
                <marquee style="font-size: 14pt; padding-top: 15px" direction="10" onmouseover="this.stop();" onmouseout="this.start();">
                    <p style="font-size: 15pt"><?= $row_text->keterangan; ?></p>
                </marquee>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-3">
        <div class="panel">
            <form action="<?= site_url('utility/Running_text/updateRunningText'); ?>" method="post">
                <div class="panel-heading">
                    Utility Periode
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Keterangan :</label>
                            <textarea class="form-control" name="keterangan" rows="3"><?= $row_text->keterangan; ?></textarea>
                            <note>Last Update : <?= $row_text->create_date; ?></note>
                            <input type="hidden" class="form-control" name="id" value="<?= $row_text->id; ?>">
                            <input type="hidden" class="form-control" name="url" value="<?= $url; ?>">
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_periode" role="dialog" aria-labelledby="editlabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title titelModal" ></h4>
            </div>
            <form class="form_status" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon bg-gray">Tahun Periode</span>
                                    <input type="number" class="form-control tahun" name="tahun">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon bg-gray">Tanggal Awal</span>
                                    <input class="form-control awal datepicker" autocomplete="off" name="awal">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon bg-gray">Tanggal Akhir</span>
                                    <input class="form-control akhir datepicker" autocomplete="off" name="akhir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control keterangan" name="keterangan" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control url" name='url' value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });
    $('#modal_periode').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var awal = button.data('awal');
        var akhir = button.data('akhir');
        var tahun = button.data('tahun');
        var keterangan = button.data('keterangan');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.awal').val('');
            modal.find('.modal-body input.akhir').val('');
            modal.find('.modal-body input.tahun').val('').prop('readonly', false);
            modal.find('.modal-body textarea.keterangan').val('');
            $('.form_status').attr('action', '<?= site_url('utility/Periode_skp/insertPeriode'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Tambah Data Periode</b>');
        } else if (aksi == 'edit') {
            modal.find('.modal-body input.awal').val(awal);
            modal.find('.modal-body input.akhir').val(akhir);
            modal.find('.modal-body input.tahun').val(tahun).prop('readonly', true);
            modal.find('.modal-body textarea.keterangan').val(keterangan);
            $('.form_status').attr('action', '<?= site_url('utility/Periode_skp/updatePeriode'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Edit Data Periode</b>');
        }
    });

    function hapusPeriode(tahun) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Tahun Periode : ' + tahun,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('utility/Periode_skp/deletePeriode'); ?>",
                    data: {tahun: tahun},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }

</script>