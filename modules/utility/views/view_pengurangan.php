<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data Keterlambatan</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th width="3%">Kode</th>
                                <th>LAMA KETERLAMBATAN </th>
                                <th width="3%">Pengurangan (%)</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($getTerlambat as $tl) { ?>
                                <tr>
                                    <td><?= $tl->kode ?></td>
                                    <td class="text-center"><?= !is_null($tl->sampai) ? $tl->dari . ' Menit s/d ' . $tl->sampai . ' Menit' : $tl->dari . ' Menit s/d ' . ' Seterusnya' ?></td>
                                    <td class="text-center"><?= $tl->persentase . ' %' ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data Pulang Sebelum Waktu</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th width="3%">Kode</th>
                                <th>LAMA MENINGGALKAN</th>
                                <th width="3%">Pengurangan (%)</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($getPulangSebelum as $psw) { ?>
                                <tr>
                                    <td><?= $psw->kode ?></td>
                                    <td class="text-center"><?= !is_null($psw->sampai) ? $psw->dari . ' Menit s/d ' . $psw->sampai . ' Menit' : $psw->dari . ' Menit s/d ' . ' Seterusnya' ?></td>
                                    <td class="text-center"><?= $psw->persentase ?> %</td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-success box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Data Aktifitas</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <?php
//                $attr = 'data-toggle="modal" 
//                            data-aksi="tambah" 
//                            data-target="#modal_akt"';
//                $ket = 'Aktivitas';
//                $class = '';
//                btn_tambah($attr, $ket, $class);
                ?>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th width="3%">No</th>
                                <th>AKTIVITAS KINERJA</th>
                                <th width="3%">Bobot</th>  
                                <th width="3%">Persen</th>  
                                <th width="3%"><i class="fa fa-cogs"></i></th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($getUtiAktifitas as $akt) {
                                ?>
                                <tr>
                                    <td class="text-center"><?= $no++; ?></td>
                                    <td><?= $akt->aktifitas_harian; ?></td>
                                    <td class="text-center"><?= $akt->bobot; ?></td>
                                    <td class="text-center"><?= $akt->persen; ?> %</td>
                                    <td>
                                        <?php
                                        $attr = "data-toggle='modal' 
                                                data-aksi='edit' 
                                                data-tipe_data='$akt->tipe_data' 
                                                data-kode='$akt->kode' 
                                                data-persen='$akt->persen' 
                                                data-aktifitas='$akt->aktifitas_harian' 
                                                data-target='#modal_akt'";
                                        $ket = '';
                                        $class = 'btn-flat btn-xs';
                                        btn_edit($attr, $ket, $class);
                                        ?>
                                        <button class="btn btn-danger btn-flat btn-xs" onclick="hapusAktifitas('<?= $akt->kode; ?>', '<?= $akt->aktifitas_harian; ?>')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_akt" role="dialog" aria-labelledby="editlabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title titelModal" ></h4>
            </div>
            <form class="form_aktifitas" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label> Nama Aktifitas Harian</label>
                            <div class="form-group">
                                <textarea class="form-control aktifitas" name='aktifitas' rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon bg-gray">Persentase</span>
                                    <input type='number' class="form-control persen" name='persen'>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="APEL" name="tipe_data" value="APEL" >
                                        Apel Pagi / Gabungan / Olahraga
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="form-group">
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="AKTIFITAS" name="tipe_data" value="">
                                        Aktifitas
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type='hidden' class="kode" name='kode'>
                    <input type="hidden" class="form-control url" name='url' value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#modal_akt').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var kode = button.data('kode');
        var aktifitas = button.data('aktifitas');
        var persen = button.data('persen');
        var tipe_data = button.data('tipe_data');
        var modal = $(this);
//        alert(tipe_data);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.kode').val('');
            modal.find('.modal-body input.aktifitas').val('');
            modal.find('.modal-body input.persen').val('');
            modal.find(`.modal-body input.APEL`).prop('checked', true);
            $('.form_aktifitas').attr('action', '<?= site_url('utility/Pengurangan/insertAktifitas'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Tambah Data Aktifitas</b>');
        } else if (aksi == 'edit') {
            modal.find('.modal-body input.kode').val(kode);
            modal.find('.modal-body textarea.aktifitas').text(aktifitas);
            modal.find('.modal-body input.persen').val(persen);
            modal.find(`.modal-body input.${tipe_data}`).prop('checked', true);
            $('.form_aktifitas').attr('action', '<?= site_url('utility/Pengurangan/updateAktifitas'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Edit Data Aktifitas</b>');
        }
    });

    function hapusAktifitas(kode, ket) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Aktifitas dengan Ket : ' + ket,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('utility/Pengurangan/deleteAktifitas'); ?>",
                    data: {kode: kode},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }
</script>