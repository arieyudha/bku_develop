<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>  

<div class="row">
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                Utility Agama
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            $attr = 'data-toggle="modal" 
                            data-aksi="tambah" 
                            data-target="#modal_agama"';
                            $ket = 'Agama';
                            $class = 'btn-block';
                            btn_tambah($attr, $ket, $class);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama Agama</th>
                                        <th width="15%"><i class="fa fa-refresh"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($getAgama as $row_aga) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $no++; ?></td>
                                            <td><?= $row_aga->nama; ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <?php
                                                    $attrEdit = 'data-toggle="modal" 
                                                                    data-aksi="edit" 
                                                                    data-id="' . $row_aga->id . '" 
                                                                    data-nama="' . $row_aga->nama . '" 
                                                                    data-target="#modal_agama"';
                                                    $ketEdit = '';
                                                    $classEdit = 'btn-xs';
                                                    btn_edit($attrEdit, $ketEdit, $classEdit);
                                                    ?>
                                                    <?php
                                                    $attrHapus = 'onclick="hapusAgama(\'' . $row_aga->id . '\', \'' . $row_aga->nama . '\')"';
                                                    $ketHapus = '';
                                                    $classHapus = 'btn-xs';
                                                    btn_hapus($attrHapus, $ketHapus, $classHapus);
                                                    ?>

                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                Utility Pendidikan
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            $attr = 'data-toggle="modal" 
                            data-aksi="tambah" 
                            data-target="#modal_pendidikan"';
                            $ket = 'Pendidikan';
                            $class = 'btn-block';
                            btn_tambah($attr, $ket, $class);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama Pendidikan</th>
                                        <th>Inisial</th>
                                        <th width="15%"><i class="fa fa-refresh"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $noPen = 1;
                                    foreach ($getPendidikan as $row_pen) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $noPen++; ?></td>
                                            <td><?= $row_pen->nama; ?></td>
                                            <td><?= $row_pen->inisial; ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <?php
                                                    $attrEdit = 'data-toggle="modal" 
                                                                    data-aksi="edit" 
                                                                    data-id="' . $row_pen->id . '" 
                                                                    data-nama="' . $row_pen->nama . '" 
                                                                    data-inisial="' . $row_pen->inisial . '" 
                                                                    data-target="#modal_pendidikan"';
                                                    $ketEdit = '';
                                                    $classEdit = 'btn-xs';
                                                    btn_edit($attrEdit, $ketEdit, $classEdit);
                                                    ?>
                                                    <?php
                                                    $attrHapus = 'onclick="hapusPendidikan(\'' . $row_pen->id . '\', \'' . $row_pen->nama . '\')"';
                                                    $ketHapus = '';
                                                    $classHapus = 'btn-xs';
                                                    btn_hapus($attrHapus, $ketHapus, $classHapus);
                                                    ?>

                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel">
            <div class="panel-heading">
                Utility Status Perkawinan
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php
                            $attr = 'data-toggle="modal" 
                            data-aksi="tambah" 
                            data-target="#modal_status"';
                            $ket = 'Status';
                            $class = 'btn-block';
                            btn_tambah($attr, $ket, $class);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama</th>
                                        <th width="15%"><i class="fa fa-refresh"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $noStts = 1;
                                    foreach ($getStatusPerkawinan as $row_stts) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $noStts++; ?></td>
                                            <td><?= $row_stts->nama; ?></td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <?php
                                                    $attrEdit = 'data-toggle="modal" 
                                                                    data-aksi="edit" 
                                                                    data-id="' . $row_stts->id . '" 
                                                                    data-nama="' . $row_stts->nama . '" 
                                                                    data-target="#modal_status"';
                                                    $ketEdit = '';
                                                    $classEdit = 'btn-xs';
                                                    btn_edit($attrEdit, $ketEdit, $classEdit);
                                                    ?>
                                                    <?php
                                                    $attrHapus = 'onclick="hapusStatus(\'' . $row_stts->id . '\', \'' . $row_stts->nama . '\')"';
                                                    $ketHapus = '';
                                                    $classHapus = 'btn-xs';
                                                    btn_hapus($attrHapus, $ketHapus, $classHapus);
                                                    ?>

                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_status" role="dialog" aria-labelledby="editlabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title titelModal" ></h4>
            </div>
            <form class="form_status" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label> Nama</label>
                            <div class="form-group">
                                <input type='text' class="form-control nama" name='nama' >
                            </div>
                        </div>
                    </div>
                    <input type='hidden' class="id_status" name='id'>
                    <input type="hidden" class="form-control url" name='url' value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_pendidikan" role="dialog" aria-labelledby="editlabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title titelModal" ></h4>
            </div>
            <form class="form_pendidikan" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label> Nama Pendidikan</label>
                            <div class="form-group">
                                <input type='text' class="form-control nama" name='nama' >
                            </div>
                            <label> Inisial</label>
                            <div class="form-group">
                                <input type='text' class="form-control inisial" name='inisial' >
                            </div>
                        </div>
                    </div>
                    <input type='hidden' class="id_pendidikan" name='id'>
                    <input type="hidden" class="form-control url" name='url' value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_agama" role="dialog" aria-labelledby="editlabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title titelModal" ></h4>
            </div>
            <form class="form_agama" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label> Nama Agama</label>
                            <div class="form-group">
                                <input type='text' class="form-control nama" name='nama' >
                            </div>
                        </div>
                    </div>
                    <input type='hidden' class="id_agama" name='id'>
                    <input type="hidden" class="form-control url" name='url' value="<?= $url; ?>">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#modal_status').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var id = button.data('id');
        var nama = button.data('nama');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.id_status').val('');
            modal.find('.modal-body input.nama').val('');
            $('.form_status').attr('action', '<?= site_url('utility/Pengaturan/insertStatus'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Tambah Data Status</b>');
        } else if (aksi == 'edit') {
            modal.find('.modal-body input.id_status').val(id);
            modal.find('.modal-body input.nama').val(nama);
            $('.form_status').attr('action', '<?= site_url('utility/Pengaturan/updateStatus'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Edit Data Status</b>');
        }
    });
    $('#modal_pendidikan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var id = button.data('id');
        var nama = button.data('nama');
        var inisial = button.data('inisial');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.id_pendidikan').val('');
            modal.find('.modal-body input.nama').val('');
            modal.find('.modal-body input.inisial').val('');
            $('.form_pendidikan').attr('action', '<?= site_url('utility/Pengaturan/insertPendidikan'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Tambah Data Agama</b>');
        } else if (aksi == 'edit') {
            modal.find('.modal-body input.id_pendidikan').val(id);
            modal.find('.modal-body input.nama').val(nama);
            modal.find('.modal-body input.inisial').val(inisial);
            $('.form_pendidikan').attr('action', '<?= site_url('utility/Pengaturan/updatePendidikan'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Edit Data Agama</b>');
        }
    });
    $('#modal_agama').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var aksi = button.data('aksi');
        var id = button.data('id');
        var nama = button.data('nama');
        var modal = $(this);
        if (aksi == 'tambah') {
            modal.find('.modal-body input.id_agama').val('');
            modal.find('.modal-body input.nama').val('');
            $('.form_agama').attr('action', '<?= site_url('utility/Pengaturan/insertAgama'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Tambah Data Agama</b>');
        } else if (aksi == 'edit') {
            modal.find('.modal-body input.id_agama').val(id);
            modal.find('.modal-body input.nama').val(nama);
            $('.form_agama').attr('action', '<?= site_url('utility/Pengaturan/updateAgama'); ?>');
            $('.titelModal').html('<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <b>Form Edit Data Agama</b>');
        }
    });

    function hapusAgama(id, nama) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Agama dengan Ket : ' + nama,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('utility/Pengaturan/deleteAgama'); ?>",
                    data: {id: id},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }
    function hapusPendidikan(id, nama) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Pendidikan dengan Ket : ' + nama,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('utility/Pengaturan/deletePendidikan'); ?>",
                    data: {id: id},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }
    function hapusStatus(id, nama) {
        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Apa anda yakin menghapus Status Perkawinan dengan Ket : ' + nama,
            text: "Silahkan Klik Tombol Delete Untuk Menghapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete ',
            cancelButtonText: 'Cancel',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?= site_url('utility/Pengaturan/deleteStatus'); ?>",
                    data: {id: id},
                    cache: false,
                    success: function (response) {
                        if (response == 'true') {
                            notif_smartAlertSukses('Berhasil');
                        } else {
                            notif_smartAlertGagal('Gagal');
                        }
                    },
                    error: function (response) {
                        notif_smartAlertGagal('Gagal');
                    }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons(
                        'Cancel',
                        'Tidak ada aksi hapus data',
                        'error'
                        );
            }
        });
    }

</script>