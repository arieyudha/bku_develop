<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_utility extends CI_Model {

    //put your code here

    function getPeriodeAktif() {
        $query = $this->db->where('status', 'Y')->get("uti_periode");
        return $query;
    }

    function getBulan($kd_bulan = null) {
        if ($kd_bulan == '') {
            $query = $this->db->get("uti_bulan");
        } else {
            $query = $this->db->query("select * from uti_bulan a where a.kd_bulan=$kd_bulan");
        }
        return $query;
    }

    function getKdTahap($kd_bulan = null) {
        if ($kd_bulan == '') {
            $query = $this->db->query("select max(a.kdtahap) as kdtahap from uti_bulan a");
        } else {
            $query = $this->db->query("select a.kdtahap from uti_bulan a where a.kd_bulan=$kd_bulan");
        }
        return $query->row();
    }

    function getRunningText() {
        $query = $this->db->get("running_text");
        return $query;
    }

}
