<?php
class Pra_rka extends MY_Controller
{
  var $a;

  public function __construct()
  {
    parent::__construct();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_perencanaan', 'M_rek']);
    }
  }

  public function index()
  {
    if ($this->input->method() == 'post') {
      $this->_tambah();
    } else {
      $record = $this->javasc_back();
      $record['rek6'] = $this->M_rek->rek6()->result_array();
      $record['dataSubKeg'] = $this->M_perencanaan->getPerSubKeg([
        'per_subkegiatan.kd_sub_keg' => $_GET['kd_sub_keg'],
        'per_subkegiatan.unitkey' => $_GET['unitkey'],
      ])->row_array();
      $record['kd_rek_1'] = $this->M_perencanaan->group(
        "per_belanja.kd_rek_1, CONCAT(per_belanja.kd_rek_1) AS _kd_rek_1, ref_rek_1.nm_rek_1",
        $this->input->get(),
        "_kd_rek_1",
        "ref_rek_1",
        "per_belanja.kd_rek_1 = ref_rek_1.kd_rek_1"
      )->result_array();
      $record['kd_rek_2'] = $this->M_perencanaan->group(
        "per_belanja.kd_rek_1, per_belanja.kd_rek_2, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2) AS _kd_rek_2, ref_rek_2.nm_rek_2",
        $this->input->get(),
        "_kd_rek_2",
        "ref_rek_2",
        "`per_belanja`.`kd_rek_1` = `ref_rek_2`.`kd_rek_1` AND
        `per_belanja`.`kd_rek_2` = `ref_rek_2`.`kd_rek_2`"
      )->result_array();
      $record['kd_rek_3'] = $this->M_perencanaan->group(
        "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3) AS _kd_rek_3, ref_rek_3.nm_rek_3",
        $this->input->get(),
        "_kd_rek_3",
        "ref_rek_3",
        "`per_belanja`.`kd_rek_1` = `ref_rek_3`.`kd_rek_1` AND
        `per_belanja`.`kd_rek_2` = `ref_rek_3`.`kd_rek_2` AND
        `per_belanja`.`kd_rek_3` = `ref_rek_3`.`kd_rek_3`"
      )->result_array();
      $record['kd_rek_4'] = $this->M_perencanaan->group(
        "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, per_belanja.kd_rek_4, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4) AS _kd_rek_4, ref_rek_4.nm_rek_4",
        $this->input->get(),
        "_kd_rek_4",
        "ref_rek_4",
        "`per_belanja`.`kd_rek_1` = `ref_rek_4`.`kd_rek_1` AND
        `per_belanja`.`kd_rek_2` = `ref_rek_4`.`kd_rek_2` AND
        `per_belanja`.`kd_rek_3` = `ref_rek_4`.`kd_rek_3` AND 
        `per_belanja`.`kd_rek_4` = `ref_rek_4`.`kd_rek_4`"
      )->result_array();
      $record['kd_rek_5'] = $this->M_perencanaan->group(
        "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, per_belanja.kd_rek_4, per_belanja.kd_rek_5, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4,'.',per_belanja.kd_rek_5) AS _kd_rek_5, ref_rek_5.nm_rek_5",
        $this->input->get(),
        "_kd_rek_5",
        "ref_rek_5",
        "`per_belanja`.`kd_rek_1` = `ref_rek_5`.`kd_rek_1` AND
        `per_belanja`.`kd_rek_2` = `ref_rek_5`.`kd_rek_2` AND
        `per_belanja`.`kd_rek_3` = `ref_rek_5`.`kd_rek_3` AND 
        `per_belanja`.`kd_rek_4` = `ref_rek_5`.`kd_rek_4` AND 
        `per_belanja`.`kd_rek_5` = `ref_rek_5`.`kd_rek_5`"
      )->result_array();
      $record['kd_rek_6'] = $this->M_perencanaan->group(
        "per_belanja.*, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4,'.',per_belanja.kd_rek_5,'.',per_belanja.kd_rek_6) AS _kd_rek_6, ref_rek_6.nm_rek_6",
        $this->input->get(),
        "_kd_rek_6",
        "ref_rek_6",
        "`per_belanja`.`kd_rek_1` = `ref_rek_6`.`kd_rek_1` AND
        `per_belanja`.`kd_rek_2` = `ref_rek_6`.`kd_rek_2` AND
        `per_belanja`.`kd_rek_3` = `ref_rek_6`.`kd_rek_3` AND 
        `per_belanja`.`kd_rek_4` = `ref_rek_6`.`kd_rek_4` AND 
        `per_belanja`.`kd_rek_5` = `ref_rek_6`.`kd_rek_5` AND 
        `per_belanja`.`kd_rek_6` = `ref_rek_6`.`kd_rek_6`"
      )->result_array();
      $record['perBelanjaRinc'] = $this->M_perencanaan->getPerBelanjaRinc("*", $this->input->get())->result_array();
      $record['perBelanjaRincDet'] = $this->M_perencanaan->getPerBelanjaRincDet("*", $this->input->get())->result_array();
      $data = $this->layout_back('perencanaan/pra_rka/index', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Input Data', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    }
  }

  public function preview()
  {
    $record['kd_rek_1'] = $this->M_perencanaan->group(
      "per_belanja.kd_rek_1, CONCAT(per_belanja.kd_rek_1) AS _kd_rek_1, ref_rek_1.nm_rek_1",
      $this->input->get(),
      "_kd_rek_1",
      "ref_rek_1",
      "per_belanja.kd_rek_1 = ref_rek_1.kd_rek_1"
    )->result_array();
    $record['kd_rek_2'] = $this->M_perencanaan->group(
      "per_belanja.kd_rek_1, per_belanja.kd_rek_2, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2) AS _kd_rek_2, ref_rek_2.nm_rek_2",
      $this->input->get(),
      "_kd_rek_2",
      "ref_rek_2",
      "`per_belanja`.`kd_rek_1` = `ref_rek_2`.`kd_rek_1` AND
      `per_belanja`.`kd_rek_2` = `ref_rek_2`.`kd_rek_2`"
    )->result_array();
    $record['kd_rek_3'] = $this->M_perencanaan->group(
      "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3) AS _kd_rek_3, ref_rek_3.nm_rek_3",
      $this->input->get(),
      "_kd_rek_3",
      "ref_rek_3",
      "`per_belanja`.`kd_rek_1` = `ref_rek_3`.`kd_rek_1` AND
      `per_belanja`.`kd_rek_2` = `ref_rek_3`.`kd_rek_2` AND
      `per_belanja`.`kd_rek_3` = `ref_rek_3`.`kd_rek_3`"
    )->result_array();
    $record['kd_rek_4'] = $this->M_perencanaan->group(
      "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, per_belanja.kd_rek_4, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4) AS _kd_rek_4, ref_rek_4.nm_rek_4",
      $this->input->get(),
      "_kd_rek_4",
      "ref_rek_4",
      "`per_belanja`.`kd_rek_1` = `ref_rek_4`.`kd_rek_1` AND
      `per_belanja`.`kd_rek_2` = `ref_rek_4`.`kd_rek_2` AND
      `per_belanja`.`kd_rek_3` = `ref_rek_4`.`kd_rek_3` AND 
      `per_belanja`.`kd_rek_4` = `ref_rek_4`.`kd_rek_4`"
    )->result_array();
    $record['kd_rek_5'] = $this->M_perencanaan->group(
      "per_belanja.kd_rek_1, per_belanja.kd_rek_2, per_belanja.kd_rek_3, per_belanja.kd_rek_4, per_belanja.kd_rek_5, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4,'.',per_belanja.kd_rek_5) AS _kd_rek_5, ref_rek_5.nm_rek_5",
      $this->input->get(),
      "_kd_rek_5",
      "ref_rek_5",
      "`per_belanja`.`kd_rek_1` = `ref_rek_5`.`kd_rek_1` AND
      `per_belanja`.`kd_rek_2` = `ref_rek_5`.`kd_rek_2` AND
      `per_belanja`.`kd_rek_3` = `ref_rek_5`.`kd_rek_3` AND 
      `per_belanja`.`kd_rek_4` = `ref_rek_5`.`kd_rek_4` AND 
      `per_belanja`.`kd_rek_5` = `ref_rek_5`.`kd_rek_5`"
    )->result_array();
    $record['kd_rek_6'] = $this->M_perencanaan->group(
      "per_belanja.*, CONCAT(per_belanja.kd_rek_1,'.',per_belanja.kd_rek_2,'.',per_belanja.kd_rek_3,'.',per_belanja.kd_rek_4,'.',per_belanja.kd_rek_5,'.',per_belanja.kd_rek_6) AS _kd_rek_6, ref_rek_6.nm_rek_6",
      $this->input->get(),
      "_kd_rek_6",
      "ref_rek_6",
      "`per_belanja`.`kd_rek_1` = `ref_rek_6`.`kd_rek_1` AND
      `per_belanja`.`kd_rek_2` = `ref_rek_6`.`kd_rek_2` AND
      `per_belanja`.`kd_rek_3` = `ref_rek_6`.`kd_rek_3` AND 
      `per_belanja`.`kd_rek_4` = `ref_rek_6`.`kd_rek_4` AND 
      `per_belanja`.`kd_rek_5` = `ref_rek_6`.`kd_rek_5` AND 
      `per_belanja`.`kd_rek_6` = `ref_rek_6`.`kd_rek_6`"
    )->result_array();
    $record['perBelanjaRinc'] = $this->M_perencanaan->getPerBelanjaRinc("*", $this->input->get())->result_array();
    $record['perBelanjaRincDet'] = $this->M_perencanaan->getPerBelanjaRincDet("*", $this->input->get())->result_array();
    $this->load->view('preview', $record);
  }

  private function _tambah()
  {
    $kdRek6 = explode('.', $this->input->post('kd_rek_6'));
    $ins['unitkey'] = _unitkey();
    $ins['tahun'] = $this->input->get('tahun');
    $ins['kd_rek_1'] = $kdRek6[0];
    $ins['kd_rek_2'] = $kdRek6[1];
    $ins['kd_rek_3'] = $kdRek6[2];
    $ins['kd_rek_4'] = $kdRek6[3];
    $ins['kd_rek_5'] = $kdRek6[4];
    $ins['kd_rek_6'] = $kdRek6[5];
    $ins['kd_sub_keg'] = $this->input->get('kd_sub_keg');
    $ins['kd_keg'] = $this->input->get('kd_keg');
    $ins['kd_prog'] = $this->input->get('kd_prog');
    if ($this->insert('per_belanja', $ins) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka?' . builUrl($this->input->get())));
  }

  public function uraian_tambah()
  {
    if ($this->input->method() == 'post') {
      $this->_uraian_tambah();
    } else {
      $p = array( // param
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'no_rek' => $this->input->get('no_rek')
      );
      $no_rek = extrackNoRek($p['no_rek'], 6);
      $record = $this->javasc_back();
      $record['sub_keg'] = $this->M_perencanaan->getRefSubKeg(['kd_sub_keg' => $p['kd_sub_keg']])->row_array();
      $record['rek6'] = $this->M_rek->rek6([
        'kd_rek_1' => $no_rek[0]['kd_rek_1'],
        'kd_rek_2' => $no_rek[1]['kd_rek_2'],
        'kd_rek_3' => $no_rek[2]['kd_rek_3'],
        'kd_rek_4' => $no_rek[3]['kd_rek_4'],
        'kd_rek_5' => $no_rek[4]['kd_rek_5'],
        'kd_rek_6' => $no_rek[5]['kd_rek_6']
      ])->row_array();
      $data = $this->layout_back('perencanaan/pra_rka/uraian/tambah', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Input Uraian', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    }
  }

  private function _uraian_tambah()
  {
    $noRek = extrackNoRek($this->input->get('no_rek'), 6);
    $ins['unitkey'] = $this->input->get('unitkey');
    $ins['tahun'] = $this->input->get('tahun');
    $ins['kd_prog'] = $this->input->get('kd_prog');
    $ins['kd_keg'] = $this->input->get('kd_keg');
    $ins['kd_sub_keg'] = $this->input->get('kd_sub_keg');
    $ins['kd_rek_1'] = $noRek[0]['kd_rek_1'];
    $ins['kd_rek_2'] = $noRek[1]['kd_rek_2'];
    $ins['kd_rek_3'] = $noRek[2]['kd_rek_3'];
    $ins['kd_rek_4'] = $noRek[3]['kd_rek_4'];
    $ins['kd_rek_5'] = $noRek[4]['kd_rek_5'];
    $ins['kd_rek_6'] = $noRek[5]['kd_rek_6'];
    $ins['no_rinc'] = count($this->M_perencanaan->getPerBelanjaRinc("*", [
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
    ])->result_array()) + 1;
    $ins['keterangan'] = $this->input->post('keterangan');
    if ($this->insert('per_belanja_rinc', $ins) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka/uraian_tambah?' . builUrl($this->input->get())));
  }

  public function uraian_edit()
  {
    if ($this->input->method() == 'post') {
      $this->_uraian_edit();
    } else {
      $p = array( // param
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'no_rek' => $this->input->get('no_rek'),
        'no_rinc' => $this->input->get('no_rinc'),
      );
      $no_rek = extrackNoRek($p['no_rek'], 6);
      $record = $this->javasc_back();
      $record['sub_keg'] = $this->M_perencanaan->getRefSubKeg(['kd_sub_keg' => $p['kd_sub_keg']])->row_array();
      $record['rek6'] = $this->M_rek->rek6([
        'kd_rek_1' => $no_rek[0]['kd_rek_1'],
        'kd_rek_2' => $no_rek[1]['kd_rek_2'],
        'kd_rek_3' => $no_rek[2]['kd_rek_3'],
        'kd_rek_4' => $no_rek[3]['kd_rek_4'],
        'kd_rek_5' => $no_rek[4]['kd_rek_5'],
        'kd_rek_6' => $no_rek[5]['kd_rek_6']
      ])->row_array();
      $record['bel_rinc'] = $this->M_perencanaan->getPerBelanjaRinc("*", [
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'kd_rek_1' => $no_rek[0]['kd_rek_1'],
        'kd_rek_2' => $no_rek[1]['kd_rek_2'],
        'kd_rek_3' => $no_rek[2]['kd_rek_3'],
        'kd_rek_4' => $no_rek[3]['kd_rek_4'],
        'kd_rek_5' => $no_rek[4]['kd_rek_5'],
        'kd_rek_6' => $no_rek[5]['kd_rek_6'],
        'no_rinc' => $this->input->get('no_rinc'),
      ])->row_array();
      $data = $this->layout_back('perencanaan/pra_rka/uraian/edit', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Edit Uraian', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    }
  }

  public function _uraian_edit()
  {
    $noRek = extrackNoRek($this->input->get('no_rek'), 6);
    $ins['keterangan'] = $this->input->post('keterangan');
    $where = array(
      'unitkey' => $this->input->get('unitkey'),
      'tahun' => $this->input->get('tahun'),
      'kd_prog' => $this->input->get('kd_prog'),
      'kd_keg' => $this->input->get('kd_keg'),
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'no_rinc' => $this->input->get('no_rinc'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
    );
    if ($this->update_where('per_belanja_rinc', $ins, $where) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka/uraian_edit?' . builUrl($this->input->get())));
  }

  public function rinc_uraian_tambah()
  {
    if ($this->input->method() == 'post') {
      if ($_GET['no_rinc'] == 0) {
        $this->_rinc_uraian_tambah_null();
      } else {
        $this->_rinc_uraian_tambah();
      }
    } else {
      $p = array( // param
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'no_rek' => $this->input->get('no_rek'),
        'no_rinc' => $this->input->get('no_rinc'),
      );
      $noRek = extrackNoRek($p['no_rek'], 6);
      $record = $this->javasc_back();
      $record['sub_keg'] = $this->M_perencanaan->getRefSubKeg(['kd_sub_keg' => $p['kd_sub_keg']])->row_array();
      $record['rek6'] = $this->M_rek->rek6([
        'kd_rek_1' => $noRek[0]['kd_rek_1'],
        'kd_rek_2' => $noRek[1]['kd_rek_2'],
        'kd_rek_3' => $noRek[2]['kd_rek_3'],
        'kd_rek_4' => $noRek[3]['kd_rek_4'],
        'kd_rek_5' => $noRek[4]['kd_rek_5'],
        'kd_rek_6' => $noRek[5]['kd_rek_6']
      ])->row_array();
      $record['perBelanjaRinc'] = $this->M_perencanaan->getPerBelanjaRinc("*", [
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'kd_rek_1' => $noRek[0]['kd_rek_1'],
        'kd_rek_2' => $noRek[1]['kd_rek_2'],
        'kd_rek_3' => $noRek[2]['kd_rek_3'],
        'kd_rek_4' => $noRek[3]['kd_rek_4'],
        'kd_rek_5' => $noRek[4]['kd_rek_5'],
        'kd_rek_6' => $noRek[5]['kd_rek_6'],
        'no_rinc' => $this->input->get('no_rinc'),
      ])->row_array();
      $data = $this->layout_back('perencanaan/pra_rka/detail_uraian/tambah', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Input Rincian Uraian', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    }
  }

  private function _rinc_uraian_tambah()
  {
    $noRek = extrackNoRek($this->input->get('no_rek'), 6);
    $no_id = count($this->M_perencanaan->getPerBelanjaRincDet("*", [
      'unitkey' => $this->input->get('unitkey'),
      'tahun' => $this->input->get('tahun'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'no_rinc' => $this->input->get('no_rinc'),
    ])->result_array()) + 1;
    $ins = array(
      'unitkey' => $this->input->get('unitkey'),
      'tahun' => $this->input->get('tahun'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'no_rinc' => $this->input->get('no_rinc'),
      'no_id' => $no_id,
      'kd_prog' => $this->input->get('kd_prog'),
      'kd_keg' => $this->input->get('kd_keg'),
      'sat_1' => $this->input->post('sat_1'),
      'sat_2' => $this->input->post('sat_2'),
      'sat_3' => $this->input->post('sat_3'),
      'satuan123' => (float)$this->input->post('sat_1') * (float)$this->input->post('sat_3'),
      'keterangan' => $this->input->post('keterangan'),
    );
    if ($this->insert('per_belanja_rinc_det', $ins) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka/rinc_uraian_tambah?' . builUrl($this->input->get())));
  }

  public function _rinc_uraian_tambah_null()
  {
    $noRek = extrackNoRek($this->input->get('no_rek'), 6);
    $ins['unitkey'] = $this->input->get('unitkey');
    $ins['tahun'] = $this->input->get('tahun');
    $ins['kd_prog'] = $this->input->get('kd_prog');
    $ins['kd_keg'] = $this->input->get('kd_keg');
    $ins['kd_sub_keg'] = $this->input->get('kd_sub_keg');
    $ins['kd_rek_1'] = $noRek[0]['kd_rek_1'];
    $ins['kd_rek_2'] = $noRek[1]['kd_rek_2'];
    $ins['kd_rek_3'] = $noRek[2]['kd_rek_3'];
    $ins['kd_rek_4'] = $noRek[3]['kd_rek_4'];
    $ins['kd_rek_5'] = $noRek[4]['kd_rek_5'];
    $ins['kd_rek_6'] = $noRek[5]['kd_rek_6'];
    $ins['no_rinc'] = $this->input->get('no_rinc');
    $ins['keterangan'] = "";
    $this->insert('per_belanja_rinc', $ins);
    $no_id = count($this->M_perencanaan->getPerBelanjaRincDet("*", [
      'unitkey' => $this->input->get('unitkey'),
      'tahun' => $this->input->get('tahun'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'no_rinc' => $this->input->get('no_rinc'),
    ])->result_array()) + 1;
    $ins['no_id'] = $no_id;
    $ins['sat_1'] = $this->input->post('sat_1');
    $ins['sat_2'] = $this->input->post('sat_2');
    $ins['sat_3'] = $this->input->post('sat_3');
    $ins['satuan123'] = (float)$this->input->post('sat_1') * (float)$this->input->post('sat_3');
    $ins['keterangan'] = $this->input->post('keterangan');
    if ($this->insert('per_belanja_rinc_det', $ins) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka/rinc_uraian_tambah?' . builUrl($this->input->get())));
  }

  public function rinc_uraian_edit()
  {
    if ($this->input->method() == 'post') {
      $this->_rinc_uraian_edit();
    } else {
      $p = array( // param
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'no_rek' => $this->input->get('no_rek'),
        'no_rinc' => $this->input->get('no_rinc'),
      );
      $noRek = extrackNoRek($p['no_rek'], 6);
      $record = $this->javasc_back();
      $record['sub_keg'] = $this->M_perencanaan->getRefSubKeg(['kd_sub_keg' => $p['kd_sub_keg']])->row_array();
      $record['rek6'] = $this->M_rek->rek6([
        'kd_rek_1' => $noRek[0]['kd_rek_1'],
        'kd_rek_2' => $noRek[1]['kd_rek_2'],
        'kd_rek_3' => $noRek[2]['kd_rek_3'],
        'kd_rek_4' => $noRek[3]['kd_rek_4'],
        'kd_rek_5' => $noRek[4]['kd_rek_5'],
        'kd_rek_6' => $noRek[5]['kd_rek_6']
      ])->row_array();
      $record['perBelanjaRinc'] = $this->M_perencanaan->getPerBelanjaRinc("*", [
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'kd_rek_1' => $noRek[0]['kd_rek_1'],
        'kd_rek_2' => $noRek[1]['kd_rek_2'],
        'kd_rek_3' => $noRek[2]['kd_rek_3'],
        'kd_rek_4' => $noRek[3]['kd_rek_4'],
        'kd_rek_5' => $noRek[4]['kd_rek_5'],
        'kd_rek_6' => $noRek[5]['kd_rek_6'],
        'no_rinc' => $this->input->get('no_rinc'),
      ])->row_array();

      $record['bel_rinc_det'] = $this->M_perencanaan->getPerBelanjaRincDet("*", [
        'unitkey' => $this->input->get('unitkey'),
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
        'kd_sub_keg' => $this->input->get('kd_sub_keg'),
        'kd_rek_1' => $noRek[0]['kd_rek_1'],
        'kd_rek_2' => $noRek[1]['kd_rek_2'],
        'kd_rek_3' => $noRek[2]['kd_rek_3'],
        'kd_rek_4' => $noRek[3]['kd_rek_4'],
        'kd_rek_5' => $noRek[4]['kd_rek_5'],
        'kd_rek_6' => $noRek[5]['kd_rek_6'],
        'no_rinc' => $this->input->get('no_rinc'),
        'no_id' => $this->input->get('no_id'),
      ])->row_array();
      $data = $this->layout_back('perencanaan/pra_rka/detail_uraian/edit', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Edit Rincian Uraian', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    }
  }

  public function _rinc_uraian_edit()
  {
    $noRek = extrackNoRek($this->input->get('no_rek'), 6);
    $where = [
      'unitkey' => $this->input->get('unitkey'),
      'tahun' => $this->input->get('tahun'),
      'kd_prog' => $this->input->get('kd_prog'),
      'kd_keg' => $this->input->get('kd_keg'),
      'kd_sub_keg' => $this->input->get('kd_sub_keg'),
      'kd_rek_1' => $noRek[0]['kd_rek_1'],
      'kd_rek_2' => $noRek[1]['kd_rek_2'],
      'kd_rek_3' => $noRek[2]['kd_rek_3'],
      'kd_rek_4' => $noRek[3]['kd_rek_4'],
      'kd_rek_5' => $noRek[4]['kd_rek_5'],
      'kd_rek_6' => $noRek[5]['kd_rek_6'],
      'no_rinc' => $this->input->get('no_rinc'),
      'no_id' => $this->input->get('no_id'),
    ];
    $ins = [
      'sat_1' => $this->input->post('sat_1'),
      'sat_2' => $this->input->post('sat_2'),
      'sat_3' => $this->input->post('sat_3'),
      'satuan123' => (float)$this->input->post('sat_1') * (float)$this->input->post('sat_3'),
      'keterangan' => $this->input->post('keterangan'),
    ];
    if ($this->update_where('per_belanja_rinc_det', $ins, $where) > 0) {
      $this->notif(true, 'Berhasil Update Data !!!');
    } else {
      $this->notif(false, 'Gagal Update Data !!!');
    }
    redirect(base_url('rka/perencanaan/Pra_rka/rinc_uraian_edit?' . builUrl($this->input->get())));
  }
}
