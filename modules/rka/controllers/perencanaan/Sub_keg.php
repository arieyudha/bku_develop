<?php
class Sub_keg extends MY_Controller
{
  var $a;

  public function __construct()
  {
    parent::__construct();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_perencanaan']);
    }
  }

  public function tambah()
  {
    if ($this->input->method() != 'post') {
      $where = array(
        'kd_prog' => $this->input->get('kd_prog'),
        'kd_keg' => $this->input->get('kd_keg'),
      );
      $record = $this->javasc_back();
      $record['prog'] = $this->M_perencanaan->getRefProgram(['kd_prog' => $where['kd_prog']])->row_array();
      $record['keg'] = $this->M_perencanaan->getRefKeg($where)->row_array();
      $record['subKeg'] = $this->M_perencanaan->getRefSubKeg($where)->result_array();
      $data = $this->layout_back('perencanaan/subkeg/tambah', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Tambah Sub Kegiatan', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    } else {
      $this->_tambah();
    }
  }

  private function _tambah()
  {
    $data = array(
      'kd_sub_keg' => $this->post('kd_sub_keg'),
      'unitkey' => _unitkey(),
      'tahun' => $this->input->get('tahun'),
      'kd_prog' => $this->input->get('kd_prog'),
      'kd_keg' => $this->input->get('kd_keg'),
      'ket_subkegiatan' => $this->post('ket_subkegiatan'),
      'waktu_awal' => $this->post('waktu_awal'),
      'waktu_akhir' => $this->post('waktu_akhir'),
      'output_sub' => $this->post('output_sub'),
      'lokasi_detail' => $this->post('lokasi_detail'),
    );
    if ($this->insert('per_subkegiatan', $data) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Kegiatan?kd_prog=' . $data['kd_prog'] . '&tahun=' . $data['tahun']));
  }
}
