<?php
class Kegiatan extends MY_Controller
{
  var $a;

  public function __construct()
  {
    parent::__construct();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_perencanaan']);
    }
  }

  public function index()
  {
    $record = $this->javasc_back();
    $record['tahun'] = $_GET['tahun'];
    $record['prog'] = $this->M_perencanaan->getPerProgram()->result_array();
    $record['kd_prog'] = $_GET['kd_prog'];
    $record['keg'] = $this->M_perencanaan->getRefKeg(['kd_prog' => $record['kd_prog']])->result_array();
    $record['perKeg'] = $this->M_perencanaan->getPerKeg(['per_kegiatan.kd_prog' => $record['kd_prog']])->result_array();
    $record['perSubKeg'] = $this->M_perencanaan->getPerSubKeg(['per_subkegiatan.kd_prog' => $record['kd_prog']])->result_array();
    $data = $this->layout_back('perencanaan/kegiatan/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Kegiatan', 'Perencanaan');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }

  public function hapus()
  {
    $d = $this->input->get();
    if ($this->delete_where('per_kegiatan', $d) > 0) {
      $this->notif(true, 'Berhasil hapus Data !!!');
    } else {
      $this->notif(false, 'Gagal hapus Data !!!');
    }
    redirect(base_url('rka/perencanaan/Kegiatan?kd_prog=' . $d['kd_prog'] . '&tahun=' . $d['tahun']));
  }

  public function tambah()
  {
    $data = array(
      'kd_keg' => $this->input->post('kd_keg'),
      'unitkey' => _unitkey(),
      'tahun' => $this->input->post('tahun'),
      'kd_prog' => $this->input->post('kd_prog'),
      'ket_kegiatan' => $this->input->post('ket_kegiatan'),
      'kelompok_sasaran' => $this->input->post('kelompok_sasaran'),
      'lokasi' => $this->input->post('lokasi'),
      'pagu_min' => $this->input->post('pagu_min'),
      'pagu_plus' => $this->input->post('pagu_plus'),
    );
    if ($this->insert('per_kegiatan', $data) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Kegiatan?kd_prog=' . $data['kd_prog'] . '&tahun=' . $data['tahun']));
  }
}
