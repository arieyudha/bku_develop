<?php
class Program extends MY_Controller
{
  var $a;

  public function __construct()
  {
    parent::__construct();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_perencanaan', 'M_rek']);
    }
  }

  public function index()
  {
    $record = $this->javasc_back();
    $record['thn'] = isset($_GET['thn']) ? $_GET['thn'] : date('Y') + 1;
    $record['prenc'] = $this->M_perencanaan->getPerProgram(['tahun' => $record['thn']])->result_array();
    $data = $this->layout_back('perencanaan/program/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Program', 'Perencanaan');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }

  public function hapus()
  {
    $data = $this->input->get();
    if ($this->delete_where('per_program', $data) > 0) {
      $this->notif(true, 'Berhasil hapus Data !!!');
    } else {
      $this->notif(false, 'Gagal hapus Data !!!');
    }
    redirect(base_url('rka/perencanaan/Program'));
  }

  public function tambah()
  {
    if ($this->input->method() == 'get') {
      $record = $this->javasc_back();
      $record['thn'] = $this->input->get('thn');
      $record['ref_prog'] = $this->M_perencanaan->getRefProgram()->result_array();
      $data = $this->layout_back('perencanaan/program/tambah', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Program', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    } else {
      $this->_tambah();
    }
  }

  private function _tambah()
  {
    $data = [
      'kd_prog' => $this->input->post('kd_prog'),
      'unitkey' => _unitkey(),
      'tahun' => $this->input->get('thn'),
      'ket_program' => $this->input->post('ket_program'),
    ];
    if ($this->insert('per_program', $data) > 0) {
      $this->notif(true, 'Berhasil Tambah Data !!!');
    } else {
      $this->notif(false, 'Gagal Tambah Data !!!');
    }
    redirect(base_url('rka/perencanaan/Program'));
  }

  public function edit()
  {
    if ($this->input->method() == 'get') {
      $record = $this->javasc_back();
      $record['thn'] = $this->input->get('thn');
      $record['per_prog'] = $this->M_perencanaan->getPerProgram([
        'tahun' => $this->input->get('tahun'),
        'kd_prog' => $this->input->get('kd_prog')
      ])->row_array();
      $record['ref_prog'] = $this->M_perencanaan->getRefProgram()->result_array();
      $data = $this->layout_back('perencanaan/program/edit', $record);
      $data['ribbon_left'] = ribbon_left('RKA - Program', 'Perencanaan');
      $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
      $this->backend($data);
    } else {
      $this->_edit();
    }
  }

  public function _edit()
  {
    $data = [
      'kd_prog' => $this->input->post('kd_prog'),
      'ket_program' => $this->input->post('ket_program'),
    ];
    $where = [
      'tahun' => $this->input->get('tahun'),
      'kd_prog' => $this->input->get('kd_prog'),
    ];
    if ($this->update_where('per_program', $data, $where) > 0) {
      $this->notif(true, 'Berhasil Update Data !!!');
    } else {
      $this->notif(false, 'Gagal Update Data !!!');
    }
    redirect(base_url('rka/perencanaan/Program'));
  }

  public function haha()
  {
    $data['rek1'] = $this->M_rek->rek1()->result_array();
    $data['rek2'] = $this->M_rek->rek2()->result_array();
    $data['rek3'] = $this->M_rek->rek3()->result_array();
    $data['rek4'] = $this->M_rek->rek4()->result_array();
    $data['rek5'] = $this->M_rek->rek5()->result_array();
    $data['rek6'] = $this->M_rek->rek6()->result_array();
    $this->load->view('testing', $data);
  }
}
