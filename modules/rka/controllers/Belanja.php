<?php
class Belanja extends MY_Controller
{
  var $a;

  public function __construct()
  {
    parent::__construct();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref']);
    }
  }

  public function index()
  {
    $record = $this->javasc_back();
    $record['bulan'] = isset($_GET['bulan']) ? $_GET['bulan'] : date('m');
    $data = $this->layout_back('belanja/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Belanja SKPD', 'Format Program, Kegiatan & Sub Kegiatan');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }
}
