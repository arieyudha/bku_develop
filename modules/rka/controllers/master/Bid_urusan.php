<?php
class Bid_urusan extends MY_Controller
{
  var $a;
  var $kdtahap;
  var $unitkey;
  var $kdbidang;
  var $kdskpd;

  public function __construct()
  {
    parent::__construct();
    $this->kdtahap = $this->getKdTahap()->kdtahap;
    $this->unitkey = $this->getUnitkey();
    $this->kdbidang = $this->getKdBidang();
    $this->kdskpd = $this->getKdSkpd();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_rka']);
    }
  }

  public function index()
  {
    $urusan = $this->M_rka->getUrusan()->result_array();
    $datas = [];
    foreach ($urusan as $k) {
      $arr['bidang_urusan'] = $this->M_rka->getUrusanBidang(['kd_urusan' => $k['kd_urusan']])->result_array();
      $final = array_merge($k, $arr);
      array_push($datas, $final);
    }

    $record = $this->javasc_back();
    $record['urusan'] = $datas;
    $record['bulan'] = isset($_GET['bulan']) ? $_GET['bulan'] : date('m');
    $data = $this->layout_back('master/bid_urusan/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Bidang Urusan', 'Urusan');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }
}
