<?php
class Program extends MY_Controller
{
  var $a;
  var $kdtahap;
  var $unitkey;
  var $kdbidang;
  var $kdskpd;

  public function __construct()
  {
    parent::__construct();
    $this->kdtahap = $this->getKdTahap()->kdtahap;
    $this->unitkey = $this->getUnitkey();
    $this->kdbidang = $this->getKdBidang();
    $this->kdskpd = $this->getKdSkpd();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_rka']);
    }
  }

  public function index()
  {
    $record = $this->javasc_back();
    $record['bulan'] = isset($_GET['bulan']) ? $_GET['bulan'] : date('m');
    $record['keg'] = $this->data();
    $data = $this->layout_back('master/program/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Program, Kegiatan & Sub Kegiatan', 'Master');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }

  public function data()
  {
    $prog = $this->M_rka->getProgram()->result_array();
    $data1 = [];
    foreach ($prog as $p) {
      $data = $p;
      $data['keg'] = $this->M_rka->getkegiatan(['kd_prog' => $p['kd_prog']])->result_array();
      array_push($data1, $data);
    }
    return $data1;
  }

  public function sub_keg($kd_keg)
  {
    $data = $this->M_rka->getSubkegiatan(['kd_keg' => $kd_keg])->result_array();
    echo json_encode($data);
  }
}
