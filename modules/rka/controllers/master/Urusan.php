<?php
class Urusan extends MY_Controller
{
  var $a;
  var $kdtahap;
  var $unitkey;
  var $kdbidang;
  var $kdskpd;

  public function __construct()
  {
    parent::__construct();
    $this->kdtahap = $this->getKdTahap()->kdtahap;
    $this->unitkey = $this->getUnitkey();
    $this->kdbidang = $this->getKdBidang();
    $this->kdskpd = $this->getKdSkpd();
    if (!$this->a) {
      redirect('home/Login');
    } else {
      $this->load->model(['utility/Model_utility', 'master/Model_ref', 'M_rka']);
    }
  }

  public function index()
  {
    $record = $this->javasc_back();
    $record['bulan'] = isset($_GET['bulan']) ? $_GET['bulan'] : date('m');
    $record['urusan'] = $this->M_rka->getUrusan();
    $data = $this->layout_back('master/urusan/index', $record);
    $data['ribbon_left'] = ribbon_left('RKA - Urusan', 'Master');
    $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
    $this->backend($data);
  }
}
