<?php

class M_perencanaan extends CI_Model
{
  public function getPerProgram($where = false)
  {
    $this->db->from("per_program");
    $this->db->join("ref_program", "per_program.kd_prog = ref_program.kd_prog", "left");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getRefProgram($where = false)
  {
    $this->db->from("ref_program");
    $this->db->join("mapping_bidang_unit", "kd_bidang = kd_bidang90", "right");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getPerKeg($where = false)
  {
    $this->db->from("per_kegiatan");
    $this->db->join("ref_kegiatan", "ref_kegiatan.kd_keg = per_kegiatan.kd_keg AND ref_kegiatan.kd_prog = per_kegiatan.kd_prog");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getRefKeg($where = false)
  {
    $this->db->from("ref_kegiatan");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getPerSubKeg($where = false)
  {
    $this->db->from("per_subkegiatan");
    $this->db->join("ref_subkegiatan", "per_subkegiatan.kd_sub_keg = ref_subkegiatan.kd_sub_keg AND per_subkegiatan.kd_keg = ref_subkegiatan.kd_keg AND per_subkegiatan.kd_prog = ref_subkegiatan.kd_prog", "left");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getRefSubKeg($where = false)
  {
    $this->db->from("ref_subkegiatan");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getPerBelanja($select = "*", $where = false)
  {
    $this->db->select($select)->from("per_belanja");
    $this->db->join('ref_rek_6', "CONCAT(per_belanja.kd_rek_1,per_belanja.kd_rek_2,per_belanja.kd_rek_3,per_belanja.kd_rek_4,per_belanja.kd_rek_5,per_belanja.kd_rek_6) = CONCAT(ref_rek_6.kd_rek_1,ref_rek_6.kd_rek_2,ref_rek_6.kd_rek_3,ref_rek_6.kd_rek_4,ref_rek_6.kd_rek_5,ref_rek_6.kd_rek_6)");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getPerBelanjaRinc($select = "*", $where = false)
  {
    $this->db->select($select)->from("per_belanja_rinc");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getPerBelanjaRincDet($select = "*", $where = false)
  {
    $this->db->select($select)->from("per_belanja_rinc_det");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function group($select = "*", $where, $group, $join, $qJoin)
  {
    $this->db->select($select)
      ->from("per_belanja");
    $this->db->join($join, $qJoin, "left");
    if ($where != false) {
      $this->db->where($where);
    }
    $this->db->group_by($group);
    return $this->db->get();
  }
}
