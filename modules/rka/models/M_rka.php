<?php

class M_rka extends CI_Model
{
  public function getUrusan($where = false)
  {
    $this->db->from("ref_urusan");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getUrusanBidang($where = false)
  {
    $this->db->from("ref_bidang");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getProgram($where = false)
  {
    $this->db->from("ref_program");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getkegiatan($where = false)
  {
    $this->db->from("ref_kegiatan");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function getSubkegiatan($where = false)
  {
    $this->db->from("ref_subkegiatan");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }
}
