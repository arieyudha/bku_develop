<?php

class M_rek extends CI_Model
{
  public function getPerBelanjaRek($where)
  {
    $kdRek = array();
    array_push($kdRek, $this->getPerBelanjaRek1($where)->row_array());
    array_push($kdRek, $this->getPerBelanjaRek2($where)->row_array());
    array_push($kdRek, $this->getPerBelanjaRek3($where)->row_array());
    array_push($kdRek, $this->getPerBelanjaRek4($where)->row_array());
    array_push($kdRek, $this->getPerBelanjaRek5($where)->row_array());
    // array_push($kdRek, $this->getPerBelanjaRek6($where)->row_array());
    return $kdRek;
  }

  public function getPerBelanjaRek1($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1) kd_rek, b.nm_rek_1 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_1 AS b", "b.kd_rek_1 = a.kd_rek_1", "left")
      ->where($where)
      ->group_by("a.kd_rek_1")
      ->get();
  }

  public function getPerBelanjaRek2($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1, '.', a.kd_rek_2) AS kd_rek, b.nm_rek_2 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_2 AS b", "b.kd_rek_2 = a.kd_rek_2", "left")
      ->where($where)
      ->group_by("a.kd_rek_2")
      ->get();
  }

  public function getPerBelanjaRek3($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1, '.' , a.kd_rek_2, '.', a.kd_rek_3) AS kd_rek, b.nm_rek_3 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_3 AS b", "b.kd_rek_3 = a.kd_rek_3", "left")
      ->where($where)
      ->group_by("a.kd_rek_3")
      ->get();
  }

  public function getPerBelanjaRek4($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1, '.' , a.kd_rek_2, '.', a.kd_rek_3, '.', a.kd_rek_4) AS kd_rek, b.nm_rek_4 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_4 AS b", "b.kd_rek_4 = a.kd_rek_4", "left")
      ->where($where)
      ->group_by("a.kd_rek_4")
      ->get();
  }

  public function getPerBelanjaRek5($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1, '.' , a.kd_rek_2, '.', a.kd_rek_3, '.', a.kd_rek_4, '.', a.kd_rek_5) AS kd_rek, b.nm_rek_5 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_5 AS b", "b.kd_rek_5 = a.kd_rek_5", "left")
      ->where($where)
      ->group_by("a.kd_rek_5")
      ->get();
  }

  public function getPerBelanjaRek6($where)
  {
    return $this->db->select("CONCAT(a.kd_rek_1, '.' , a.kd_rek_2, '.', a.kd_rek_3, '.', a.kd_rek_4, '.', a.kd_rek_5, '.', a.kd_rek_6) AS kd_rek, b.nm_rek_6 AS nm_rek")
      ->from("per_belanja as a")
      ->join("ref_rek_6 AS b", "b.kd_rek_6 = a.kd_rek_6", "left")
      ->where($where)
      ->group_by("a.kd_rek_6")
      ->get();
  }

  public function rek1($where = false)
  {
    $this->db->from("ref_rek_1");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function rek2($where = false)
  {
    $this->db->from("ref_rek_2");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function rek3($where = false)
  {
    $this->db->from("ref_rek_3");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function rek4($where = false)
  {
    $this->db->from("ref_rek_4");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function rek5($where = false)
  {
    $this->db->from("ref_rek_5");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function rek6($where = false)
  {
    $this->db->from("ref_rek_6");
    if ($where != false) {
      $this->db->where($where);
    }
    return $this->db->get();
  }
}
