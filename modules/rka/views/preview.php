<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    table {
      border-collapse: collapse;
    }

    table,
    th,
    td {
      border: 1px solid black;
    }
  </style>
</head>

<body>
  <table style="width: 100%;">
    <thead>
      <tr>
        <th rowspan="2" style="width: 7%;">Kode Rekening</th>
        <th rowspan="2">Uraian</th>
        <th colspan="3">Rincian Perhitungan</th>
        <th rowspan="2">Jumlah</th>
      </tr>
      <tr>
        <th>Volume</th>
        <th>Satuan</th>
        <th>Harga Satuan</th>
      </tr>
      <tr>
        <?php for ($i = 1; $i <= 5; $i++) { ?>
          <th><?= $i ?></th>
        <?php } ?>
        <th>6 = (3x5)</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($kd_rek_1 as $i1 => $v1) { ?>
        <tr>
          <td><?= $v1['_kd_rek_1'] ?></td>
          <td><?= $v1['nm_rek_1'] ?></td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">
            <?php
            $sum1 = $this->db->select("SUM(satuan123) as jml")
              ->from("per_belanja_rinc_det")
              ->where([
                'unitkey' => $_GET['unitkey'],
                'tahun' => $_GET['tahun'],
                'kd_prog' => $_GET['kd_prog'],
                'kd_keg' => $_GET['kd_keg'],
                'kd_sub_keg' => $_GET['kd_sub_keg'],
                'kd_rek_1' => $v1['kd_rek_1'],
              ])
              ->group_by("CONCAT(kd_rek_1)")->get()->row_array();
            echo numberFormat($sum1['jml']);
            ?>
          </td>
        </tr>
        <?php foreach ($kd_rek_2 as $i2 => $v2) { ?>
          <?php if (($v1['kd_rek_1']) == ($v2['kd_rek_1'])) { ?>
            <tr>
              <td><?= $v2['_kd_rek_2'] ?></td>
              <td><?= $v2['nm_rek_2'] ?></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="text-right">
                <?php
                $sum2 = $this->db->select("SUM(satuan123) as jml")
                  ->from("per_belanja_rinc_det")
                  ->where([
                    'unitkey' => $_GET['unitkey'],
                    'tahun' => $_GET['tahun'],
                    'kd_prog' => $_GET['kd_prog'],
                    'kd_keg' => $_GET['kd_keg'],
                    'kd_sub_keg' => $_GET['kd_sub_keg'],
                    'kd_rek_1' => $v1['kd_rek_1'],
                    'kd_rek_2' => $v2['kd_rek_2'],
                  ])
                  ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2)")->get()->row_array();
                echo numberFormat($sum2['jml']);
                ?>
              </td>
            </tr>
            <?php foreach ($kd_rek_3 as $i3 => $v3) { ?>
              <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2']) == ($v3['kd_rek_1'] . '.' . $v3['kd_rek_2'])) { ?>
                <tr>
                  <td><?= $v3['_kd_rek_3'] ?></td>
                  <td><?= $v3['nm_rek_3'] ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="text-right">
                    <?php
                    $sum3 = $this->db->select("SUM(satuan123) as jml")
                      ->from("per_belanja_rinc_det")
                      ->where([
                        'unitkey' => $_GET['unitkey'],
                        'tahun' => $_GET['tahun'],
                        'kd_prog' => $_GET['kd_prog'],
                        'kd_keg' => $_GET['kd_keg'],
                        'kd_sub_keg' => $_GET['kd_sub_keg'],
                        'kd_rek_1' => $v1['kd_rek_1'],
                        'kd_rek_2' => $v2['kd_rek_2'],
                        'kd_rek_3' => $v3['kd_rek_3'],
                      ])
                      ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3)")->get()->row_array();
                    echo numberFormat($sum3['jml']);
                    ?>
                  </td>
                </tr>
                <?php foreach ($kd_rek_4 as $i4 => $v4) { ?>
                  <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3']) == ($v4['kd_rek_1'] . '.' . $v4['kd_rek_2'] . '.' . $v4['kd_rek_3'])) { ?>
                    <tr>
                      <td><?= $v4['_kd_rek_4'] ?></td>
                      <td><?= $v4['nm_rek_4'] ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="text-right">
                        <?php
                        $sum4 = $this->db->select("SUM(satuan123) as jml")
                          ->from("per_belanja_rinc_det")
                          ->where([
                            'unitkey' => $_GET['unitkey'],
                            'tahun' => $_GET['tahun'],
                            'kd_prog' => $_GET['kd_prog'],
                            'kd_keg' => $_GET['kd_keg'],
                            'kd_sub_keg' => $_GET['kd_sub_keg'],
                            'kd_rek_1' => $v1['kd_rek_1'],
                            'kd_rek_2' => $v2['kd_rek_2'],
                            'kd_rek_3' => $v3['kd_rek_3'],
                            'kd_rek_4' => $v4['kd_rek_4'],
                          ])
                          ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4)")->get()->row_array();
                        echo numberFormat($sum4['jml']);
                        ?>
                      </td>
                    </tr>
                    <?php foreach ($kd_rek_5 as $i5 => $v5) { ?>
                      <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3'] . '.' . $v4['kd_rek_4']) == ($v5['kd_rek_1'] . '.' . $v5['kd_rek_2'] . '.' . $v5['kd_rek_3'] . '.' . $v5['kd_rek_4'])) { ?>
                        <tr>
                          <td><?= $v5['_kd_rek_5'] ?></td>
                          <td><?= $v5['nm_rek_5'] ?></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td class="text-right">
                            <?php
                            $sum5 = $this->db->select("SUM(satuan123) as jml")
                              ->from("per_belanja_rinc_det")
                              ->where([
                                'unitkey' => $_GET['unitkey'],
                                'tahun' => $_GET['tahun'],
                                'kd_prog' => $_GET['kd_prog'],
                                'kd_keg' => $_GET['kd_keg'],
                                'kd_sub_keg' => $_GET['kd_sub_keg'],
                                'kd_rek_1' => $v1['kd_rek_1'],
                                'kd_rek_2' => $v2['kd_rek_2'],
                                'kd_rek_3' => $v3['kd_rek_3'],
                                'kd_rek_4' => $v4['kd_rek_4'],
                                'kd_rek_5' => $v5['kd_rek_5'],
                              ])
                              ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5)")->get()->row_array();
                            echo numberFormat($sum5['jml']);
                            ?>
                          </td>
                        </tr>
                        <?php foreach ($kd_rek_6 as $i6 => $v6) { ?>
                          <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3'] . '.' . $v4['kd_rek_4'] . '.' . $v5['kd_rek_5']) == ($v6['kd_rek_1'] . '.' . $v6['kd_rek_2'] . '.' . $v6['kd_rek_3'] . '.' . $v6['kd_rek_4'] . '.' . $v6['kd_rek_5'])) { ?>
                            <tr>
                              <td><?= $v6['_kd_rek_6'] ?></td>
                              <td><?= $v6['nm_rek_6'] ?></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td class="text-right">
                                <?php
                                $sum6 = $this->db->select("SUM(satuan123) as jml")
                                  ->from("per_belanja_rinc_det")
                                  ->where([
                                    'unitkey' => $_GET['unitkey'],
                                    'tahun' => $_GET['tahun'],
                                    'kd_prog' => $_GET['kd_prog'],
                                    'kd_keg' => $_GET['kd_keg'],
                                    'kd_sub_keg' => $_GET['kd_sub_keg'],
                                    'kd_rek_1' => $v1['kd_rek_1'],
                                    'kd_rek_2' => $v2['kd_rek_2'],
                                    'kd_rek_3' => $v3['kd_rek_3'],
                                    'kd_rek_4' => $v4['kd_rek_4'],
                                    'kd_rek_5' => $v5['kd_rek_5'],
                                    'kd_rek_6' => $v6['kd_rek_6'],
                                  ])
                                  ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5,'.',kd_rek_6)")->get()->row_array();
                                echo numberFormat($sum6['jml']);
                                ?>
                              </td>
                            </tr>
                            <?php foreach ($perBelanjaRinc as $ipbr => $vpbr) { ?>
                              <?php if (($vpbr['tahun'] == $v6['tahun']) && ($vpbr['kd_rek_1'] == $v6['kd_rek_1']) && ($vpbr['kd_rek_2'] == $v6['kd_rek_2']) && ($vpbr['kd_rek_3'] == $v6['kd_rek_3']) && ($vpbr['kd_rek_4'] == $v6['kd_rek_4']) && ($vpbr['kd_rek_5'] == $v6['kd_rek_5']) && ($vpbr['kd_rek_6'] == $v6['kd_rek_6']) && ($vpbr['kd_sub_keg'] == $v6['kd_sub_keg']) && ($vpbr['kd_keg'] == $v6['kd_keg']) && ($vpbr['kd_prog'] == $v6['kd_prog'])) { ?>
                                <?php if ($vpbr['no_rinc'] != 0) { ?>
                                  <tr>
                                    <td>
                                    </td>
                                    <td><?= $vpbr['keterangan'] ?></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">
                                      <?php
                                      $sumRinci = $this->db->select("SUM(satuan123) as jml")
                                        ->from("per_belanja_rinc_det")
                                        ->where([
                                          'unitkey' => $_GET['unitkey'],
                                          'tahun' => $_GET['tahun'],
                                          'kd_prog' => $_GET['kd_prog'],
                                          'kd_keg' => $_GET['kd_keg'],
                                          'kd_sub_keg' => $_GET['kd_sub_keg'],
                                          'kd_rek_1' => $v1['kd_rek_1'],
                                          'kd_rek_2' => $v2['kd_rek_2'],
                                          'kd_rek_3' => $v3['kd_rek_3'],
                                          'kd_rek_4' => $v4['kd_rek_4'],
                                          'kd_rek_5' => $v5['kd_rek_5'],
                                          'kd_rek_6' => $v6['kd_rek_6'],
                                          'no_rinc' => $vpbr['no_rinc']
                                        ])
                                        ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5,'.',kd_rek_6)")->get()->row_array();
                                      echo numberFormat($sumRinci['jml']);
                                      ?>
                                    </td>
                                  </tr>
                                <?php } ?>
                                <?php foreach ($perBelanjaRincDet as $brdi => $brdv) { ?>
                                  <?php if ((genNoRek($brdv, 6) == genNoRek($vpbr, 6)) && ($brdv['no_rinc'] == $vpbr['no_rinc'])) { ?>
                                    <tr>
                                      <td>
                                      </td>
                                      <td><?= $brdv['keterangan'] ?></td>
                                      <td class="text-center"><?= $brdv['sat_1'] ?></td>
                                      <td class="text-center"><?= $brdv['sat_2'] ?></td>
                                      <td class="text-center"><?= numberFormat($brdv['sat_3']) ?></td>
                                      <td class="text-right"><?= numberFormat($brdv['satuan123']) ?></td>
                                    </tr>
                                  <?php } ?>
                                <?php } ?>
                              <?php } ?>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</body>

</html>