<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <table border=1>
    <thead>
      <tr>
        <?php for ($i = 1; $i <= 6; $i++) { ?>
          <th>KODE REK <?= $i ?></th>
        <?php } ?>
        <th>NAMA REK</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($rek1 as $i1 => $v1) { ?>
        <tr>
          <td><?= genNoRek($v1, 1) ?></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><?= $v1['nm_rek_1'] ?></td>
        </tr>
        <?php foreach ($rek2 as $i2 => $v2) { ?>
          <?php if (genNoRek($v1, 1) == genNoRek($v2, 1)) { ?>
            <tr>
              <td></td>
              <td><?= genNoRek($v2, 2) ?></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><?= $v2['nm_rek_2'] ?></td>
            </tr>
            <?php foreach ($rek3 as $i3 => $v3) { ?>
              <?php if (genNoRek($v2, 2) == genNoRek($v3, 2)) { ?>
                <tr>
                  <td></td>
                  <td></td>
                  <td><?= genNoRek($v3, 3) ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><?= $v3['nm_rek_3'] ?></td>
                </tr>
                <?php foreach ($rek4 as $i4 => $v4) { ?>
                  <?php if (genNoRek($v3, 3) == genNoRek($v4, 3)) { ?>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><?= genNoRek($v4, 4) ?></td>
                      <td></td>
                      <td></td>
                      <td><?= $v4['nm_rek_4'] ?></td>
                    </tr>
                    <?php foreach ($rek5 as $i5 => $v5) { ?>
                      <?php if (genNoRek($v4, 4) == genNoRek($v5, 4)) { ?>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><?= genNoRek($v5, 5) ?></td>
                          <td></td>
                          <td><?= $v5['nm_rek_5'] ?></td>
                        </tr>
                        <?php foreach ($rek6 as $i6 => $v6) { ?>
                          <?php if (genNoRek($v5, 5) == genNoRek($v6, 5)) { ?>
                            <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td><?= genNoRek($v6, 6) ?></td>
                              <td><?= $v6['nm_rek_6'] ?></td>
                            </tr>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</body>

</html>