<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">RKA - Belanja SKPD</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-3">
            <div class="form-group">
              <label>Pilih Bulan</label>
              <select name="" class="form-control select2">
                <?php foreach (listBulan() as $b => $v) { ?>
                  <option <?= $bulan == $b ? 'selected' : '' ?> value="<?= $b ?>"><?= $v ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <button class="btn btn-primary"><i class="fa fa-plus mr-1"></i>Tambah Kegiatan</button>
            <table class="table table-hover table-bordered" width='100%'>
              <thead>
                <tr>
                  <th rowspan="2" width="5%">Kode</th>
                  <th rowspan="2">Uraian</th>
                  <th rowspan="2">Sumbar Dana</th>
                  <th rowspan="2">Lokasi</th>
                  <th rowspan="2">T-1</th>
                  <th colspan="5">T</th>
                  <th rowspan="2">T+1</th>
                </tr>
                <tr>
                  <th>Belanja Operasi</th>
                  <th>Belanja Modal</th>
                  <th>Belanja Tidak Terduga</th>
                  <th>Belanja Transfer</th>
                  <th>Jumlah</th>
                </tr>
                <tr class="text-center">
                  <td style="font-size: 9pt;">1</td>
                  <td style="font-size: 9pt;">2</td>
                  <td style="font-size: 9pt;">3</td>
                  <td style="font-size: 9pt;">4</td>
                  <td style="font-size: 9pt;">5</td>
                  <td style="font-size: 9pt;">6</td>
                  <td style="font-size: 9pt;">7</td>
                  <td style="font-size: 9pt;">8</td>
                  <td style="font-size: 9pt;">9</td>
                  <td style="font-size: 9pt;">10 = 6+7+8+9</td>
                  <td style="font-size: 9pt;">11</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>5</td>
                  <td>Belanja</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: right;">200.000</td>
                  <td></td>
                </tr>
                <tr>
                  <td>5.2</td>
                  <td>Belanja Langsung</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: right;">200.000</td>
                  <td></td>
                </tr>
                <tr>
                  <td>5.2.1</td>
                  <td>Belanja Pegawai</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: right;">200.000</td>
                  <td></td>
                </tr>
                <tr>
                  <td>5.2.1.03</td>
                  <td>Uang Lembur</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: right;">200.000</td>
                  <td></td>
                </tr>
                <tr>
                  <td>5.2.1.03.02</td>
                  <td>Uang Lembur Honorer</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: right;">200.000</td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $("table").DataTable({
      "paging": false,
      "ordering": false,
      "searching": false
    })
  })
</script>