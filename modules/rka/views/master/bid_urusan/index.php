<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Daftar Bidang Urusan</h3>
      </div>
      <div class="panel-body">
        <table class="table table-hover table-bordered" style="width: 100%;">
          <thead>
            <tr>
              <th>Kode Bidang</th>
              <th>Nama Urusan / Bidang Urusan</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Kode Bidang</th>
              <th>Nama Urusan / Bidang Urusan</th>
            </tr>
          </tfoot>
          <tbody>
            <?php foreach ($urusan as $u) { ?>
              <tr style="background-color: #4fe3d2;">
                <td></td>
                <td><?= $u['nm_urusan'] ?></td>
              </tr>
              <?php foreach ($u['bidang_urusan'] as $b) { ?>
                <tr>
                  <td><?= $b['kd_bidang'] ?></td>
                  <td><?= $b['nm_bidang'] ?></td>
                </tr>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $("table").DataTable({
      scrollY: '50vh',
      scrollCollapse: true,
      paging: false,
      ordering: false,
    })
  })
</script>