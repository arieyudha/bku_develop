<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Daftar Urusan</h3>
      </div>
      <div class="panel-body">
        <table class="table table-hover table-bordered" width='100%'>
          <thead>
            <tr>
              <th style="width: 10%;">No</th>
              <th>Nama Urusan</th>
            </tr>
          </thead>
          <tbody>
            <?php $n = 1;
            foreach ($urusan->result_array() as $a) { ?>
              <tr>
                <td class="text-center"><?= $n ?></td>
                <td><?= $a['nm_urusan'] ?></td>
              </tr>
            <?php $n++;
            } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $("table").DataTable({
      "paging": false,
    })
  })
</script>