<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Program, Kegiatan & Sub Kegiatan</h3>
      </div>
      <div class="panel-body">
        <table class="table table-sm myTable" style="width: 100%;">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Program <i class="fa fa-arrow-right"></i> Kegiatan</th>
              <th>Sub Kegiatan</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($keg as $p) { ?>
              <tr style="background-color: #7590ff;">
                <th><?= $p['kd_prog'] ?></th>
                <th><?= $p['nm_program'] ?></th>
                <th></th>
              </tr>
              <?php foreach ($p['keg'] as $k) { ?>
                <tr style="background-color: #9cafff;">
                  <td><?= $k['kd_keg'] ?></td>
                  <td><?= $k['nm_kegiatan'] ?></td>
                  <td><button onclick="subKeg('<?= $k['kd_keg'] ?>')" class="btn btn-sm btn-warning">Sub Keg</button></td>
                </tr>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalSubKeg" tabindex="-1" role="dialog" aria-labelledby="modalSubKegLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalSubKegLabel">Sub Kegiatan</h4>
      </div>
      <div class="modal-body" style="height:500px;overflow:auto;">
        <table class="table table-sm" style="width: 100%;">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Sub Kegiatan</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $(".myTable").DataTable({
      scrollY: '60vh',
      scrollCollapse: true,
      paging: false,
      ordering: false,
    })

    subKeg = (kd_keg) => {
      $.getJSON(`<?= base_url('rka/master/Program/sub_keg/') ?>${kd_keg}`, function(data) {
        console.log(data);
        var items = [];
        $.each(data, function(key, val) {
          items.push("<tr>" +
            "<td>" + val.kd_sub_keg + "</td>" +
            "<td>" + val.nm_sub_kegiatan + "</td>" +
            "</tr>");
        });
        // console.log(items)
        $(".modal table tbody").html(items)
      });
      $('#modalSubKeg').modal('show')
    }
  })
</script>