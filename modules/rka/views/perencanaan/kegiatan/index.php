<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Program & Kegiatan</h3>
      </div>
      <div class="panel-footer">
        <form action="" method="get">
          <div class="form-group">
            <label for="exampleInputEmail1">Program :</label>
            <select name="kd_prog" class="form-control" onchange="this.form.submit()">
              <?php foreach ($prog as $p) { ?>
                <option value="<?= $p['kd_prog'] ?>" <?= $kd_prog == $p['kd_prog'] ? 'selected' : '' ?>><?= $p['kd_prog'] . ' - ' . $p['nm_program'] ?></option>
              <?php } ?>
            </select>
          </div>
          <input type="hidden" name="tahun" value="<?= $tahun ?>">
          <a href="<?= base_url('rka/perencanaan/Program') ?>" class="btn btn-danger"><strong><i class="fa fa-backward fa-fw"></i>Kembali</strong></a>
        </form>
      </div>
      <div class="panel-body">
        <button class="btn btn-primary" data-toggle="modal" data-target="#mdlAddKeg" style="margin-bottom: 10px;"><strong><i class="fa fa-plus fa-fw"></i>Kegiatan</strong></button>
        <div class="table-responsive" style="width: 100%;">
          <?= $this->session->flashdata('notif') ?>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Kode</th>
                <th>Kegiatan <i class="fa fa-arrow-right"></i> Sub Kegiatan</th>
                <th>Lokasi<br>Detail Lokasi</th>
                <th>Jumlah Dana (Rp)</th>
                <th>Tambah</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($perKeg as $i => $pk) { ?>
                <tr style="background-color: #61c5ff;">
                  <td><?= $pk['kd_keg'] ?></td>
                  <td><?= $pk['nm_kegiatan'] ?></td>
                  <td><?= $pk['lokasi'] ?></td>
                  <td>Rp. <?= $pk['pagu'] ?></td>
                  <td class="text-center">
                    <a href="<?= base_url('rka/perencanaan/Sub_keg/tambah?kd_prog=' . $kd_prog . '&kd_keg=' . $pk['kd_keg'] . '&tahun=' . $tahun) ?>" class="btn btn-primary btn-sm"><strong><i class="fa fa-plus fa-fw"></i>Sub Kegiatan</a>
                  </td>
                  <td class="text-center">
                    <a class="btn btn-sm btn-danger" href="<?= base_url('rka/perencanaan/Kegiatan/hapus?kd_prog=' . $kd_prog . '&tahun=' . $tahun . '&kd_keg=' . $pk['kd_keg']) ?>"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php foreach ($perSubKeg as $ip => $vp) { ?>
                  <?php if ($pk['kd_keg'] == $vp['kd_keg']) { ?>
                    <tr>
                      <td class="text-right"><?= $vp['kd_sub_keg'] ?></td>
                      <td><?= $vp['nm_sub_kegiatan'] ?></td>
                      <td><?= $vp['lokasi_detail'] ?></td>
                      <td></td>
                      <td class="text-center">
                        <a href="<?= base_url('rka/perencanaan/Pra_rka?unitkey=' . _unitkey() . '&tahun=' . $vp['tahun'] . '&kd_prog=' . $vp['kd_prog'] . '&kd_keg=' . $vp['kd_keg'] . '&kd_sub_keg=' . $vp['kd_sub_keg']) ?>" class="btn btn-success btn-sm"><strong><i class="fa fa-plus fa-fw"></i>Pra RKA</a>
                      </td>
                      <td class="text-center"></td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="mdlAddKeg" tabindex="-1" role="dialog" aria-labelledby="mdlAddKegLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?= base_url('rka/perencanaan/Kegiatan/tambah') ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mdlAddKegLabel">Tambah Kegiatan</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Pilih Kegiatan</label>
            <select name="kd_keg" class="form-control" required>
              <option disabled selected value="">--PILIH--</option>
              <?php foreach ($keg as $k) { ?>
                <option value="<?= $k['kd_keg'] ?>"><?= $k['kd_keg'] . ' - ' . $k['nm_kegiatan'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Lokasi</label>
            <input type="text" name="lokasi" placeholder="Lokasi" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Kelompok Sasaran</label>
            <textarea name="kelompok_sasaran" placeholder="Kelompok Sasaran" class="form-control" required></textarea>
          </div>
          <div class="form-group">
            <label>Keterangan Kegiatan</label>
            <input type="text" name="ket_kegiatan" class="form-control" placeholder="Keterangan Kegiatan" required>
          </div>
          <input type="hidden" name="tahun" value="<?= $tahun ?>">
          <input type="hidden" name="kd_prog" value="<?= $kd_prog ?>">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Pagu N - 1</label>
                <div class="input-group">
                  <span class="input-group-addon" id="sizing-addon2">Rp.</span>
                  <input type="number" class="form-control" name="pagu_min" aria-describedby="sizing-addon2">
                  <span class="input-group-addon" id="sizing-addon2">,00</span>
                </div>
                <small class="text-danger">*Masukkan tanpa tanda baca</small>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Pagu N + 1</label>
                <div class="input-group">
                  <span class="input-group-addon" id="sizing-addon2">Rp.</span>
                  <input type="number" class="form-control" name="pagu_plus" aria-describedby="sizing-addon2">
                  <span class="input-group-addon" id="sizing-addon2">,00</span>
                </div>
                <small class="text-danger">*Masukkan tanpa tanda baca</small>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>