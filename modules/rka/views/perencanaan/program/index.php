<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Program & Kegiatan</h3>
      </div>
      <div class="panel-body">
        <?= $this->session->flashdata('notif') ?>
        <div class="row">
          <div class="col-sm-3">
            <form action="" method="get">
              <div class="form-group">
                <label>Pilih Tahun</label>
                <select name="thn" class="form-control" onchange="this.form.submit();">
                  <option disabled value="">--PILIH TAHUN--</option>
                  <?php for ($i = 2020; $i <= date("Y") + 1; $i++) { ?>
                    <option <?= $thn == $i ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
                  <?php } ?>
                </select>
              </div>
              <a class="btn btn-primary" href="<?= base_url('rka/perencanaan/Program/tambah?thn=' . $thn) ?>"><i class="fa fa-plus fa-fw"></i>Tambah</a>
            </form>
          </div>
          <div class="col-sm-12">
            <table class="table table-sm" style="width: 100%;">
              <thead>
                <tr>
                  <th>Kode Program</th>
                  <th>Ket Program</th>
                  <th>Kegiatan</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($prenc as $p) { ?>
                  <tr>
                    <td><?= $p['kd_prog'] ?></td>
                    <td><?= $p['nm_program'] ?></td>
                    <td class="text-center"><a href="<?= base_url('rka/perencanaan/Kegiatan?kd_prog=' . $p['kd_prog'] . '&tahun=' . $thn) ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a></td>
                    <td class="text-center">
                      <a href="<?= base_url('rka/perencanaan/Program/edit?kd_prog=' . $p['kd_prog'] . '&tahun=' . $p['tahun']) ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                      <a href="<?= base_url('rka/perencanaan/Program/hapus?kd_prog=' . $p['kd_prog'] . '&unitkey=' . $p['unitkey'] . '&tahun=' . $p['tahun']) ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $("table").DataTable({
      ordering: false,
      paging: false,
    })
  })
</script>