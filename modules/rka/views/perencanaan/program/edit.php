<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-6">
    <div class="panel">
      <div class="panel-heading bg-gray">
        <h3 class="panel-title">Edit Program</h3>
      </div>
      <div class="panel-body">
        <form method="POST" action="">
          <div class="form-group">
            <label>Program</label>
            <select name="kd_prog" class="form-control select2" required>
              <option value="" disabled>--PILIH PROGRAM--</option>
              <?php foreach ($ref_prog as $r) { ?>
                <option <?= $per_prog['kd_prog'] == $r['kd_prog'] ? 'selected' : '' ?> value="<?= $r['kd_prog'] ?>"><?= $r['kd_prog'] . ' - ' . $r['nm_program'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="ket_program" class="form-control" required><?= $per_prog['ket_program'] ?></textarea>
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>