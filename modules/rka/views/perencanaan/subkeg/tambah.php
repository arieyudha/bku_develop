<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
  <div class="col-sm-8 col-md-offset-2">
    <div class="panel">
      <form action="" method="post">
        <div class="panel-heading bg-gray">
          <h3 class="panel-title">Tambah Sub Kegiatan</h3>
        </div>
        <div class="panel-body">
          <h4><strong>Program : <?= $prog['kd_prog'] . ' - ' . $prog['nm_program'] ?></strong></h4>
          <h4><strong>Kegiatan : <?= $keg['kd_keg'] . ' - ' . $keg['nm_kegiatan'] ?></strong></h4>
          <hr>
          <div class="form-group">
            <label>Sub Kegiatan</label>
            <select name="kd_sub_keg" class="form-control" required>
              <option value="">--PILIH--</option>
              <?php foreach ($subKeg as $i => $v) { ?>
                <option value="<?= $v['kd_sub_keg'] ?>"><?= $v['kd_sub_keg'] . ' - ' . $v['nm_sub_kegiatan'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Detail Lokasi</label>
            <input type="text" name="lokasi_detail" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Output</label>
            <textarea name="output_sub" class="form-control" required></textarea>
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="ket_subkegiatan" class="form-control" required></textarea>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Awal</label>
                <select name="waktu_awal" class="form-control" required>
                  <option value="">--PILIH--</option>
                  <?php foreach (listBulan() as $i => $v) { ?>
                    <option value="<?= $v ?>"><?= $v ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Akhir</label>
                <select name="waktu_akhir" class="form-control" required>
                  <option value="">--PILIH--</option>
                  <?php foreach (listBulan() as $i => $v) { ?>
                    <option value="<?= $v ?>"><?= $v ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw"></i>Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>