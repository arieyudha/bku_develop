<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
  <div class="col-sm-5">
    <div class="panel panel-default">
      <div class="panel-body">
        <table class="table table-sm table-striped">
          <tr>
            <th>Sub Kegiatan</th>
            <td>:</td>
            <td><?= $sub_keg['nm_sub_kegiatan'] ?></td>
          </tr>
          <tr>
            <th>Belanja</th>
            <td>:</td>
            <td><?= genNoRek($rek6, 6) . ' - ' . $rek6['nm_rek_6'] ?></td>
          </tr>
          <tr>
            <th>Keterangan</th>
            <td>:</td>
            <td><?= $perBelanjaRinc['keterangan'] ?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-sm-7">
    <?= $this->session->flashdata('notif') ?>
    <div class="panel panel-default">
      <form action="" method="post">
        <div class="panel-body">
          <?php if ($_GET['no_rinc'] == 0) { ?>
            <input type="hidden" name="no_rinc" value="0">
          <?php } ?>
          <div class="form-group">
            <label>Uraian</label>
            <textarea class="form-control" placeholder="Uraian" name="keterangan" required></textarea>
          </div>
          <div class="form-group">
            <label>Volume</label>
            <input type="text" class="form-control" placeholder="Satuan" name="sat_1" required>
          </div>
          <div class="form-group">
            <label>Satuan</label>
            <input type="text" class="form-control" placeholder="Satuan" name="sat_2" required>
          </div>
          <div class="form-group">
            <label>Harga Satuan</label>
            <input type="text" class="form-control" placeholder="Satuan" name="sat_3" required>
          </div>
        </div>
        <div class="panel-footer">
          <a href="<?= base_url('rka/perencanaan/Pra_rka?unitkey=' . $_GET['unitkey'] . '&tahun=' . $_GET['tahun'] . '&kd_prog=' . $_GET['kd_prog'] . '&kd_keg=' . $_GET['kd_keg'] . '&kd_sub_keg=' . $_GET['kd_sub_keg'] . '') ?>" class="btn btn-danger"><i class="fa fa-backward fa-fw"></i> Kembali</a>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>