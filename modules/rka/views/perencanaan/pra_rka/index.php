<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-6">
            <table class="table table-sm table-striped">
              <tr>
                <th>Sub Kegiatan</th>
                <td>:</td>
                <td><?= $dataSubKeg['nm_sub_kegiatan'] ?></td>
              </tr>
              <tr>
                <th>Sumber Dana</th>
                <td>:</td>
                <td><?= $dataSubKeg['kd_sumber'] ?></td>
              </tr>
              <tr>
                <th>Lokasi</th>
                <td>:</td>
                <td><?= $dataSubKeg['lokasi_detail'] ?></td>
              </tr>
            </table>
          </div>
          <div class="col-sm-6">
            <table class="table table-sm table-striped">
              <tr>
                <th>Waktu Pelaksanaan</th>
                <td>:</td>
                <td><?= $dataSubKeg['waktu_awal'] . ' s/d ' . $dataSubKeg['waktu_akhir'] ?></td>
              </tr>
              <tr>
                <th>Keluaran Sub Kegiatan</th>
                <td>:</td>
                <td><?= $dataSubKeg['output_sub'] ?></td>
              </tr>
              <tr>
                <th>Jumlah Dana</th>
                <td>:</td>
                <td></td>
              </tr>
            </table>
          </div>
        </div>
        <hr>
        <div>
          <a href="<?= base_url('rka/perencanaan/Kegiatan?kd_prog=' . $_GET['kd_prog'] . '&tahun=' . $_GET['tahun'] . '') ?>" class="btn btn-danger"><i class="fa fa-backward fa-fw"></i>Kembali</a>
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalBelanjaLangsung"><i class="fa fa-plus fa-fw"></i>Belanja</button>
          <a target="_blank" href="<?= base_url('rka/perencanaan/Pra_rka/preview?' . builUrl($this->input->get())) ?>" class="btn btn-success"><i class="fa fa-eye fa-fw"></i>Preview</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="panel">
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-sm table-bordered table-striped" style="width: 100%;">
            <thead>
              <tr>
                <th rowspan="2" style="width: 7%;">Opsi</th>
                <th rowspan="2" style="width: 7%;">Kode Rekening</th>
                <th rowspan="2">Uraian</th>
                <th colspan="3">Rincian Perhitungan</th>
                <th rowspan="2">Jumlah</th>
                <th rowspan="2">Hapus</th>
              </tr>
              <tr>
                <th>Volume</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($kd_rek_1 as $i1 => $v1) { ?>
                <tr>
                  <td></td>
                  <td><?= $v1['_kd_rek_1'] ?></td>
                  <td><?= $v1['nm_rek_1'] ?></td>
                  <td colspan="3"></td>
                  <td class="text-right">
                    <?php
                    $sum1 = $this->db->select("SUM(satuan123) as jml")
                      ->from("per_belanja_rinc_det")
                      ->where([
                        'unitkey' => $_GET['unitkey'],
                        'tahun' => $_GET['tahun'],
                        'kd_prog' => $_GET['kd_prog'],
                        'kd_keg' => $_GET['kd_keg'],
                        'kd_sub_keg' => $_GET['kd_sub_keg'],
                        'kd_rek_1' => $v1['kd_rek_1'],
                      ])
                      ->group_by("CONCAT(kd_rek_1)")->get()->row_array();
                    echo numberFormat($sum1['jml']);
                    ?>
                  </td>
                  <td></td>
                </tr>
                <?php foreach ($kd_rek_2 as $i2 => $v2) { ?>
                  <?php if (($v1['kd_rek_1']) == ($v2['kd_rek_1'])) { ?>
                    <tr>
                      <td></td>
                      <td><?= $v2['_kd_rek_2'] ?></td>
                      <td><?= $v2['nm_rek_2'] ?></td>
                      <td colspan="3"></td>
                      <td class="text-right">
                        <?php
                        $sum2 = $this->db->select("SUM(satuan123) as jml")
                          ->from("per_belanja_rinc_det")
                          ->where([
                            'unitkey' => $_GET['unitkey'],
                            'tahun' => $_GET['tahun'],
                            'kd_prog' => $_GET['kd_prog'],
                            'kd_keg' => $_GET['kd_keg'],
                            'kd_sub_keg' => $_GET['kd_sub_keg'],
                            'kd_rek_1' => $v1['kd_rek_1'],
                            'kd_rek_2' => $v2['kd_rek_2'],
                          ])
                          ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2)")->get()->row_array();
                        echo numberFormat($sum2['jml']);
                        ?>
                      </td>
                      <td></td>
                    </tr>
                    <?php foreach ($kd_rek_3 as $i3 => $v3) { ?>
                      <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2']) == ($v3['kd_rek_1'] . '.' . $v3['kd_rek_2'])) { ?>
                        <tr>
                          <td></td>
                          <td><?= $v3['_kd_rek_3'] ?></td>
                          <td><?= $v3['nm_rek_3'] ?></td>
                          <td colspan="3"></td>
                          <td class="text-right">
                            <?php
                            $sum3 = $this->db->select("SUM(satuan123) as jml")
                              ->from("per_belanja_rinc_det")
                              ->where([
                                'unitkey' => $_GET['unitkey'],
                                'tahun' => $_GET['tahun'],
                                'kd_prog' => $_GET['kd_prog'],
                                'kd_keg' => $_GET['kd_keg'],
                                'kd_sub_keg' => $_GET['kd_sub_keg'],
                                'kd_rek_1' => $v1['kd_rek_1'],
                                'kd_rek_2' => $v2['kd_rek_2'],
                                'kd_rek_3' => $v3['kd_rek_3'],
                              ])
                              ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3)")->get()->row_array();
                            echo numberFormat($sum3['jml']);
                            ?>
                          </td>
                          <td></td>
                        </tr>
                        <?php foreach ($kd_rek_4 as $i4 => $v4) { ?>
                          <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3']) == ($v4['kd_rek_1'] . '.' . $v4['kd_rek_2'] . '.' . $v4['kd_rek_3'])) { ?>
                            <tr>
                              <td></td>
                              <td><?= $v4['_kd_rek_4'] ?></td>
                              <td><?= $v4['nm_rek_4'] ?></td>
                              <td colspan="3"></td>
                              <td class="text-right">
                                <?php
                                $sum4 = $this->db->select("SUM(satuan123) as jml")
                                  ->from("per_belanja_rinc_det")
                                  ->where([
                                    'unitkey' => $_GET['unitkey'],
                                    'tahun' => $_GET['tahun'],
                                    'kd_prog' => $_GET['kd_prog'],
                                    'kd_keg' => $_GET['kd_keg'],
                                    'kd_sub_keg' => $_GET['kd_sub_keg'],
                                    'kd_rek_1' => $v1['kd_rek_1'],
                                    'kd_rek_2' => $v2['kd_rek_2'],
                                    'kd_rek_3' => $v3['kd_rek_3'],
                                    'kd_rek_4' => $v4['kd_rek_4'],
                                  ])
                                  ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4)")->get()->row_array();
                                echo numberFormat($sum4['jml']);
                                ?>
                              </td>
                              <td></td>
                            </tr>
                            <?php foreach ($kd_rek_5 as $i5 => $v5) { ?>
                              <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3'] . '.' . $v4['kd_rek_4']) == ($v5['kd_rek_1'] . '.' . $v5['kd_rek_2'] . '.' . $v5['kd_rek_3'] . '.' . $v5['kd_rek_4'])) { ?>
                                <tr>
                                  <td></td>
                                  <td><?= $v5['_kd_rek_5'] ?></td>
                                  <td><?= $v5['nm_rek_5'] ?></td>
                                  <td colspan="3"></td>
                                  <td class="text-right">
                                    <?php
                                    $sum5 = $this->db->select("SUM(satuan123) as jml")
                                      ->from("per_belanja_rinc_det")
                                      ->where([
                                        'unitkey' => $_GET['unitkey'],
                                        'tahun' => $_GET['tahun'],
                                        'kd_prog' => $_GET['kd_prog'],
                                        'kd_keg' => $_GET['kd_keg'],
                                        'kd_sub_keg' => $_GET['kd_sub_keg'],
                                        'kd_rek_1' => $v1['kd_rek_1'],
                                        'kd_rek_2' => $v2['kd_rek_2'],
                                        'kd_rek_3' => $v3['kd_rek_3'],
                                        'kd_rek_4' => $v4['kd_rek_4'],
                                        'kd_rek_5' => $v5['kd_rek_5'],
                                      ])
                                      ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5)")->get()->row_array();
                                    echo numberFormat($sum5['jml']);
                                    ?>
                                  </td>
                                  <td></td>
                                </tr>
                                <?php foreach ($kd_rek_6 as $i6 => $v6) { ?>
                                  <?php if (($v1['kd_rek_1'] . '.' . $v2['kd_rek_2'] . '.' . $v3['kd_rek_3'] . '.' . $v4['kd_rek_4'] . '.' . $v5['kd_rek_5']) == ($v6['kd_rek_1'] . '.' . $v6['kd_rek_2'] . '.' . $v6['kd_rek_3'] . '.' . $v6['kd_rek_4'] . '.' . $v6['kd_rek_5'])) { ?>
                                    <tr>
                                      <td>
                                        <a href="<?= base_url('rka/perencanaan/Pra_rka/uraian_tambah?unitkey=' . $v6['unitkey'] . '&tahun=' . $v6['tahun'] . '&kd_prog=' . $v6['kd_prog'] . '&kd_keg=' . $v6['kd_keg'] . '&kd_sub_keg=' . $v6['kd_sub_keg'] . '&no_rek=' . genNoRek($v6, 6) . '') ?>" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus fa-fw"></i>Uraian</a>
                                        <a href="<?= base_url('rka/perencanaan/Pra_rka/rinc_uraian_tambah?unitkey=' . $v6['unitkey'] . '&tahun=' . $v6['tahun'] . '&kd_prog=' . $v6['kd_prog'] . '&kd_keg=' . $v6['kd_keg'] . '&kd_sub_keg=' . $v6['kd_sub_keg'] . '&no_rek=' . genNoRek($v6, 6) . '&no_rinc=' . 0 . '') ?>" class="btn btn-warning btn-sm btn-block"><i class="fa fa-plus fa-fw"></i>Rincian</a>
                                      </td>
                                      <td><?= $v6['_kd_rek_6'] ?></td>
                                      <td><?= $v6['nm_rek_6'] ?></td>
                                      <td colspan="3"></td>
                                      <td class="text-right">
                                        <?php
                                        $sum6 = $this->db->select("SUM(satuan123) as jml")
                                          ->from("per_belanja_rinc_det")
                                          ->where([
                                            'unitkey' => $_GET['unitkey'],
                                            'tahun' => $_GET['tahun'],
                                            'kd_prog' => $_GET['kd_prog'],
                                            'kd_keg' => $_GET['kd_keg'],
                                            'kd_sub_keg' => $_GET['kd_sub_keg'],
                                            'kd_rek_1' => $v1['kd_rek_1'],
                                            'kd_rek_2' => $v2['kd_rek_2'],
                                            'kd_rek_3' => $v3['kd_rek_3'],
                                            'kd_rek_4' => $v4['kd_rek_4'],
                                            'kd_rek_5' => $v5['kd_rek_5'],
                                            'kd_rek_6' => $v6['kd_rek_6'],
                                          ])
                                          ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5,'.',kd_rek_6)")->get()->row_array();
                                        echo numberFormat($sum6['jml']);
                                        ?>
                                      </td>
                                      <td></td>
                                    </tr>
                                    <?php foreach ($perBelanjaRinc as $ipbr => $vpbr) { ?>
                                      <?php if (($vpbr['tahun'] == $v6['tahun']) && ($vpbr['kd_rek_1'] == $v6['kd_rek_1']) && ($vpbr['kd_rek_2'] == $v6['kd_rek_2']) && ($vpbr['kd_rek_3'] == $v6['kd_rek_3']) && ($vpbr['kd_rek_4'] == $v6['kd_rek_4']) && ($vpbr['kd_rek_5'] == $v6['kd_rek_5']) && ($vpbr['kd_rek_6'] == $v6['kd_rek_6']) && ($vpbr['kd_sub_keg'] == $v6['kd_sub_keg']) && ($vpbr['kd_keg'] == $v6['kd_keg']) && ($vpbr['kd_prog'] == $v6['kd_prog'])) { ?>
                                        <?php if ($vpbr['no_rinc'] != 0) { ?>
                                          <tr>
                                            <td>
                                              <a href="<?= base_url('rka/perencanaan/Pra_rka/rinc_uraian_tambah?unitkey=' . $vpbr['unitkey'] . '&tahun=' . $vpbr['tahun'] . '&kd_prog=' . $vpbr['kd_prog'] . '&kd_keg=' . $vpbr['kd_keg'] . '&kd_sub_keg=' . $vpbr['kd_sub_keg'] . '&no_rek=' . genNoRek($vpbr, 6) . '&no_rinc=' . $vpbr['no_rinc'] . '') ?>" class="btn btn-success btn-sm btn-block"><i class="fa fa-plus fa-fw"></i>Detail</a>
                                            </td>
                                            <td>
                                              <a href="<?= base_url('rka/perencanaan/Pra_rka/uraian_edit?unitkey=' . $vpbr['unitkey'] . '&tahun=' . $vpbr['tahun'] . '&kd_prog=' . $vpbr['kd_prog'] . '&kd_keg=' . $vpbr['kd_keg'] . '&kd_sub_keg=' . $vpbr['kd_sub_keg'] . '&no_rek=' . genNoRek($vpbr, 6) . '&no_rinc=' . $vpbr['no_rinc'] . '') ?>" class="btn btn-warning btn-sm btn-block"><i class="fa fa-pencil fa-fw"></i>Edit</a>
                                            </td>
                                            <td><?= $vpbr['keterangan'] ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right">
                                              <?php
                                              $sumRinci = $this->db->select("SUM(satuan123) as jml")
                                                ->from("per_belanja_rinc_det")
                                                ->where([
                                                  'unitkey' => $_GET['unitkey'],
                                                  'tahun' => $_GET['tahun'],
                                                  'kd_prog' => $_GET['kd_prog'],
                                                  'kd_keg' => $_GET['kd_keg'],
                                                  'kd_sub_keg' => $_GET['kd_sub_keg'],
                                                  'kd_rek_1' => $v1['kd_rek_1'],
                                                  'kd_rek_2' => $v2['kd_rek_2'],
                                                  'kd_rek_3' => $v3['kd_rek_3'],
                                                  'kd_rek_4' => $v4['kd_rek_4'],
                                                  'kd_rek_5' => $v5['kd_rek_5'],
                                                  'kd_rek_6' => $v6['kd_rek_6'],
                                                  'no_rinc' => $vpbr['no_rinc']
                                                ])
                                                ->group_by("CONCAT(kd_rek_1,'.',kd_rek_2,'.',kd_rek_3,'.',kd_rek_4,'.',kd_rek_5,'.',kd_rek_6)")->get()->row_array();
                                              echo numberFormat($sumRinci['jml']);
                                              ?>
                                            </td>
                                            <td></td>
                                          </tr>
                                        <?php } ?>
                                        <?php foreach ($perBelanjaRincDet as $brdi => $brdv) { ?>
                                          <?php if ((genNoRek($brdv, 6) == genNoRek($vpbr, 6)) && ($brdv['no_rinc'] == $vpbr['no_rinc'])) { ?>
                                            <tr>
                                              <td></td>
                                              <td>
                                                <a href="<?= base_url('rka/perencanaan/Pra_rka/rinc_uraian_edit?unitkey=' . $brdv['unitkey'] . '&tahun=' . $brdv['tahun'] . '&kd_prog=' . $brdv['kd_prog'] . '&kd_keg=' . $brdv['kd_keg'] . '&kd_sub_keg=' . $brdv['kd_sub_keg'] . '&no_rek=' . genNoRek($brdv, 6) . '&no_rinc=' . $brdv['no_rinc'] . '&no_id=' . $brdv['no_id'] . '') ?>" class="btn btn-warning btn-sm btn-block"><i class="fa fa-pencil fa-fw"></i>Edit</a>
                                              </td>
                                              <td><?= $brdv['keterangan'] ?></td>
                                              <td class="text-center"><?= $brdv['sat_1'] ?></td>
                                              <td class="text-center"><?= $brdv['sat_2'] ?></td>
                                              <td class="text-center"><?= numberFormat($brdv['sat_3']) ?></td>
                                              <td class="text-right"><?= numberFormat($brdv['satuan123']) ?></td>
                                              <td></td>
                                            </tr>
                                          <?php } ?>
                                        <?php } ?>
                                      <?php } ?>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                              <?php } ?>
                            <?php } ?>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Belanja Langsung -->
<div class="modal fade" id="modalBelanjaLangsung" role="dialog" aria-labelledby="modalBelanjaLangsungLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalBelanjaLangsungLabel"><strong>Tambah Belanja Langsung</strong></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Pilih Belanja</label>
            <select class="form-control select2" name="kd_rek_6" style="width: 100%;" required>
              <option value="">--PILIH BELANJA--</option>
              <?php foreach ($rek6 as $r6) {
                $noRek = genNoRek($r6, 6);
              ?>
                <option value="<?= $noRek ?>"><?= $noRek . ' - ' . $r6['nm_rek_6'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal Belanja Langsung -->

<script>
  const base_url = "<?= base_url() ?>"
  document.addEventListener("DOMContentLoaded", () => {
    $('#tBelanja').dataTable({
      "scrollY": "500px",
      "scrollCollapse": true,
      "paging": false,
      "order": [],
      "ordering": false
    });
  });
</script>