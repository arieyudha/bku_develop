<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<?= $this->session->flashdata('notif') ?>
<div class="row">
  <div class="col-sm-6">
    <div class="panel panel-default">
      <div class="panel-body">
        <table class="table table-sm table-striped">
          <tr>
            <th>Sub Kegiatan</th>
            <td>:</td>
            <td><?= $sub_keg['nm_sub_kegiatan'] ?></td>
          </tr>
          <tr>
            <th>Belanja</th>
            <td>:</td>
            <td>
              <?= genNoRek($rek6, 6) . ' - ' . $rek6['nm_rek_6'] ?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <form action="" method="post">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>Keterangan Belanja</label>
            <textarea name="keterangan" placeholder="Keterangan Belanja" class="form-control"><?= $bel_rinc['keterangan'] ?></textarea>
          </div>
        </div>
        <div class="panel-footer">
          <a class="btn btn-danger" href="<?= base_url('rka/perencanaan/Pra_rka?unitkey=' . $_GET['unitkey'] . '&tahun=' . $_GET['tahun'] . '&kd_prog=' . $_GET['kd_prog'] . '&kd_keg=' . $_GET['kd_keg'] . '&kd_sub_keg=' . $_GET['kd_sub_keg'] . '') ?>"><i class="fa fa-backward fa-fw"></i>Kembali</a>
          <button class="btn btn-primary"><i class="fa fa-plus fa-fw"></i>Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>