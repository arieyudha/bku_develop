<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_kas extends MY_Controller {

    var $a;
    var $kdtahap;
    var $unitkey;
    var $kdbidang;
    var $kdskpd;
    private $constructor = [
        'mode' => 'utf-8',
        'format' => 'Legal-P',
        'default_font_size' => 1,
        'default_font' => 'Tahoma',
        'margin_left' => 8,
        'margin_right' => 8,
        'margin_top' => 8,
        'margin_bottom' => 30,
        'margin_header' => 8,
        'margin_footer' => 30,
        "tempDir" => "./temp"
    ];
    private $constructor_l = [
        'mode' => 'utf-8',
        'format' => 'Legal-L',
        'default_font_size' => 1,
        'default_font' => 'Tahoma',
        'margin_left' => 8,
        'margin_right' => 30,
        'margin_top' => 8,
        'margin_bottom' => 12,
        'margin_header' => 8,
        'margin_footer' => 8,
        "tempDir" => "./temp"
    ];

    public function __construct() {
        parent::__construct();
        $this->unitkey = $this->getUnitkey();
        $this->kdbidang = $this->getKdBidang();
        $this->kdskpd = $this->getKdSkpd();
        if (!$this->a) {
            redirect('home/Login');
        } else {
            $this->load->model([
                'dpa/Model_dpa',
                'posting/Model_posting',
                'utility/Model_utility',
                'master/Model_ref',
                'dpa/Model_realisasi'
            ]);
            $this->load->library('ci_qr_code');
            $this->config->load('qr_code');
        }
        $this->mpdf = new \Mpdf\Mpdf($this->constructor);
        $this->mpdf_l = new \Mpdf\Mpdf($this->constructor_l);
    }

    function index() {
        $record = $this->javasc_back();
//        $kd_jabatan = isset($_REQUEST['kd_jabatan']) ? $_REQUEST['kd_jabatan'] : '';
        $kd_jabatan = $this->getKdJabatanPptk($this->a['kd_level']);
        $kd_bulan = isset($_REQUEST['kd_bulan']) ? $_REQUEST['kd_bulan'] : date('m');
        $record['kd_jabatan'] = $kd_jabatan;
        $record['tahun'] = $this->a['tahun'];
        $record['kd_bulan'] = $kd_bulan;
        $record['get_dataPptkJabatan'] = $this->get_dataPptk($this->a['kd_level']);
        $record['getBulan'] = $this->Model_utility->getBulan()->result();
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
        $record['get_dataDpaProgram'] = $this->Model_dpa->get_dataDpaProgramPptk($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kd_jabatan)->result();
        $record['get_dataReaKegPptk'] = $this->Model_realisasi->get_dataReaKegPptk($this->a['tahun'], $row_kdtahap->kdtahap, $kd_bulan, $this->unitkey, $kd_jabatan)->result();
        $data = $this->layout_back('laporan/view_laporan_prog_keg', $record);
        $data['ribbon_left'] = ribbon_left('Laporan Data DPA', 'Program dan kegiatan');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

    function getPptk() {
        $kd_jabatan = $this->post('kd_jabatan');
        $get_dataPptkJabatan = $this->get_dataPegawaiPptk($kd_jabatan);
        return jsonArray($get_dataPptkJabatan);
    }

    function inputLaporan() {
        $url = $this->input->post('url');
        $kd_jabatan = $this->post('kd_jabatan');
        $data['unitkey'] = $this->unitkey;
        $data['kdkegunit'] = $this->post('kdkegunit');
        $data['idprgrm'] = $this->post('idprgrm');
        $data['kd_bulan'] = $this->post('kd_bulan');
        $data['kdtahap'] = $this->post('kdtahap');
        $data['kode_qr'] = $this->post('kode_qr');
        $data['tgl_laporan'] = $this->post('tgl_laporan');
        $data['nama_pptk'] = $this->post('nama_pptk');
        $data['jabatan_pptk'] = $this->post('jabatan_pptk');
        $data['pangkat_pptk'] = $this->post('pangkat_pptk');
        $data['gol_pptk'] = $this->post('gol_pptk');
        $data['tahun'] = $this->a['tahun'];
        $get_dataReaKegPptk = $this->Model_realisasi->get_dataReaKegPptk($this->a['tahun'], $data['kdtahap'], $data['kd_bulan'], $data['unitkey'], $kd_jabatan)->result_array();
        $dataArray = array();
        foreach ($get_dataReaKegPptk as $row) {
            $array['kdtahap'] = $row['kdtahap'];
            $array['unitkey'] = $row['unitkey'];
            $array['kdkegunit'] = $row['kdkegunit'];
            $array['tahun'] = $row['tahun'];
            $array['idprgrm'] = $row['idprgrm'];
            $array['lokasi'] = $row['lokasi'];
            $array['pagu'] = $row['pagu'];
            $array['sasaran'] = $row['sasaran'];
            $array['nukeg'] = $row['nukeg'];
            $array['nmkegunit'] = $row['nmkegunit'];
            $array['kd_jabatan'] = $row['kd_jabatan'];
            $array['kd_pptk'] = $row['kd_pptk'];
            $array['keu_realisasi'] = $row['keu_realisasi'];
            $array['ttl_realisasi'] = $row['ttl_realisasi'];
            $dataArray[] = $array;
        }
        $data['array_laporan'] = json_encode($dataArray);
        $qry = $this->insert_duplicate('data_laporan_realisasi', $data);
        $ket = 'Menambahkan Data Laporan';
        if ($qry) {
            $kd = $data['kode_qr'] . ".png";
            $qr_code_config = array(
                'imagedir' => $this->config->item('imagedir'),
                'ciqrcodelib' => $this->config->item('ciqrcodelib'),
                'size' => $this->config->item('size')
            );
            $this->ci_qr_code->initialize($qr_code_config);
            $codeContents = $data['kode_qr'];
            $params = array(
                'data' => $codeContents,
                'level' => 'H',
                'size' => 8,
                'savename' => FCPATH . $qr_code_config['imagedir'] . $kd
            );
            $this->ci_qr_code->generate($params);

            $info = 'Berhasil';
        } else {
            $info = 'Gagal';
        }
        $this->flashdata($ket, $info);
        redirect($url);
    }

    function buku_kas_umum() {
        $kd_jabatan = $this->input->get('kd_jabatan');
        $kd_bulan = $this->input->get('kd_bulan');
        $idprgrm = $this->input->get('idprgrm');
        $kdkegunit = $this->input->get('kdkegunit');
        $row_kdtahap = $this->getKdTahap($kd_bulan);
        $record['row_bln'] = $this->Model_utility->getBulan($kd_bulan)->row();
        $record['tahun'] = $this->a['tahun'];
        $record['kd_jabatan'] = $kd_jabatan;
        $record['kd_bulan'] = $kd_bulan;
        $record['idprgrm'] = $idprgrm;
        $record['kdkegunit'] = $kdkegunit;
        $record['kd_tahap'] = $row_kdtahap->kdtahap;
        $record['row_unit'] = $this->Model_ref->get_dataDaftUnit($this->unitkey)->row();
        $record['row_skpd'] = $this->Model_ref->get_dataDaftUnitKd($this->kdskpd)->row();
        $record['row_bidang'] = $this->Model_ref->get_dataDaftUnitKd($this->kdbidang)->row();
        $record['row_prog'] = $this->Model_dpa->get_dataDpaProgram($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $idprgrm)->row();
        $row_keg = $this->Model_posting->get_dataPostingKegiatan($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan)->row();
        $record['row_keg'] = $row_keg;
        $record['get_dataDpaKinkeg'] = $this->Model_dpa->get_dataDpaKinkeg($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv1'] = $this->Model_dpa->get_dataDpaRekLv1($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv2'] = $this->Model_dpa->get_dataDpaRekLv2($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv3'] = $this->Model_dpa->get_dataDpaRekLv3($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv4'] = $this->Model_dpa->get_dataDpaRekLv4($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRekLv5'] = $this->Model_dpa->get_dataDpaRekLv5($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit)->result();
        $record['get_dataDpaRincDet'] = $this->Model_realisasi->get_dataReaDpaRincDet($this->a['tahun'], $row_kdtahap->kdtahap, $this->unitkey, $kdkegunit, $kd_bulan)->result();


        $nm_keg = str_replace(' ', '_', $row_keg->nmkegunit);

        $pdfFilePath = "buku_kas_umum_$nm_keg.pdf";
        $data['titel'] = $pdfFilePath;
        $data['content'] = $this->load->view('laporan/lap_buku_kas_umum', $record, TRUE);
//        $add_page = $this->load->view('laporan/add_page_bku', $record, true);
        $htmlMpdf = $this->load->view('back/paper_pdf', $data, true);
        $this->mpdf_l->SetHTMLFooter('<table width="100%" border="0" style="vertical-align: bottom; font-family: serif; font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;"><tr>
<td width="" style="border-right: 0px ;"><span style="font-weight: bold; font-style: italic;">Dicetak lewat ' . base_url() . ' Tanggal ' . Tgl_indo::indo_angka($row_keg->tgl_laporan) . '</span></td>
<td width="" style="border-right: 0px ;"><span style="font-weight: bold; font-style: italic;"> QR-CODE : ' . $row_keg->kode_qr . '</span></td>
<td width="10%" align="center" style="font-weight: bold; font-style: italic;border-right: 0px ;"></td>
<td width="20%" style="text-align: right;border-left: 0px ; ">Halaman {PAGENO} Dari {nbpg}</td>
</tr></table>');
        $this->mpdf_l->WriteHTML($htmlMpdf);
//        $this->mpdf_l->AddPage();
//        $this->mpdf_l->WriteHTML($add_page);
        $this->mpdf_l->Output($pdfFilePath, "I");
    }

}
