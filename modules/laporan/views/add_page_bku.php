<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$a = $this->session->userdata('logged_in');
?>
<html>
    <head>
        <style type="text/css">
            td {
                padding: 3px 5px 3px 5px;
                font-size: 10pt;
            }
            th {
                padding: 3px 5px 3px 5px;
                font-size: 10pt;
                font-weight: bold;
            }

            table, table .main {
                width: 100%;
                border-collapse: collapse;
                background: #fff;
            }

            .center { text-align: center;}
            .right { text-align: right;}
            .putus { border-bottom: 1px dotted #666; border-top: 1px dotted #666; }
            .bawah { border-bottom: 0px ; }
            .atas { border-top: 0px ; }
            .kanan { border-right: 0px ; }
            .kiri { border-left: 0px ; }
            .all { border: 1px solid #666; }

        </style>
    </head>
    <body>

        <table class='main' repeat_header="1" cellspacing="0" border="0" width="100%" >
            <tr>
                <td width="60%"></td>
                <td class="text-center">Nama Jabatan</td>
            </tr>
            <?php for ($i = 0; $i < 3; $i++) { ?>
                <tr>
                    <td colspan="2" style="padding: 10px"></td>
                </tr>
            <?php } ?>
            <tr>
                <td width="60%"></td>
                <td class="text-center">Nama<br> NIP.</td>
            </tr>
        </table>


    </body>
</html>