<?php
$url = getUrl();
$attrPost = '';
if ($row_keg->status_posting == 'Y') {
    $attrPost = 'hidden';
}
?>
<table border="0" class="table" width="100%">
    <tr >
        <td class="text-center" style="font-size: 16pt;font-weight: bold" colspan="4"><?= strtoupper('Rekapitulasi Realisasi Anggaran Dan Belanja'); ?></td>
    </tr>
    <tr>
        <td width="15%">Urusan Pemerintahan</td>
        <td width="5%" class="text-center">:</td>
        <td ><?= $row_bidang->kdunit . ' - ' . $row_bidang->nmunit; ?></td>
        <td rowspan="5" class="text-center" style="font-size: 10pt">
            <img src="<?= base_url('assets/qr_codes/' . $row_keg->kode_qr . '.png'); ?>" width="8%">
            <br>
            <?= $row_keg->kode_qr; ?>
        </td>
    </tr>
    <tr>
        <td >Organisasi</td>
        <td class="text-center">:</td>
        <td ><?= $row_unit->kdunit . ' - ' . $row_unit->nmunit; ?></td>
    </tr>
    <tr>
        <td >Program</td>
        <td class="text-center">:</td>
        <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . ' - ' . $row_prog->nmprgrm; ?></td>
    </tr>
    <tr>
        <td>Kegiatan</td>
        <td class="text-center">:</td>
        <td ><?= $row_unit->kdunit . $row_prog->nuprgrm . $row_keg->nukeg . ' - ' . $row_keg->nmkegunit; ?></td>
    </tr>
    <tr>
        <td>Bulan</td>
        <td class="text-center">:</td>
        <td ><?= $row_bln->nama_bulan . ' - ' . $tahun; ?></td>
    </tr>
</table>
<table class="table" border="1">
    <thead>
        <tr>
            <th class="text-center" rowspan="2" width="10%">Rekening</th>
            <th class="text-center" rowspan="2">Uraian</th>
            <th class="text-center" colspan="2">Rincian Perhitungan</th>
            <th class="text-center" rowspan="2" width="10%">Pagu Anggaran</th>
            <th class="text-center" colspan="3">Realisasi</th>
            <th class="text-center" rowspan="2" width="10%">Sisa Pagu Anggaran</th>
            <th class="text-center" rowspan="2" width="7%">Sisa <br>Volume</th>
        </tr>
        <tr>
            <th class="text-center" width="7%">Volume</th>
            <th class="text-center" width="10%">Harga Satuan</th>
            <th class="text-center" width="10%">Bulan Lalu</th>
            <th class="text-center" width="10%">Bulan Ini</th>
            <th class="text-center" width="10%">S/D Bulan ini</th>
        </tr>
        <tr>
            <?php for ($i = 1; $i <= 7; $i++) { ?>
                <th class="text-center"><?= $i; ?></th>
            <?php } ?>
            <th class="text-center">8 = 6+7</th>
            <th class="text-center">9 = 5-8</th>
            <th class="text-center">10</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($get_dataDpaRekLv1 as $lv1) {
            $keu_realisasi_lv1 = 0;
            $ttl_realisasi_lv1 = 0;
            $ttl_realisasi_lv1_bln_ll = 0;
            foreach ($get_dataDpaRekLv2 as $lv2) {
                $kdlv1 = substr($lv2->kdper_rep, 0, 1);
                if ($kdlv1 == $lv1->kdper_rep) {
                    foreach ($get_dataDpaRekLv3 as $lv3) {
                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                        if ($kdlv2 == $lv2->kdper_rep) {
                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                if ($kdlv3 == $lv3->kdper_rep) {
                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                        if ($kdlv4 == $lv4->kdper_rep) {
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_lv1 += $dpa->keu_realisasi;
                                                    $ttl_realisasi_lv1 += $dpa->ttl_realisasi;
                                                    $ttl_realisasi_lv1_bln_ll += $dpa->ttl_realisasi_bln_ll;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $ttl_realisasi_lv1_sd_bln = $ttl_realisasi_lv1_bln_ll + $keu_realisasi_lv1;
            ?>
            <tr class="bg-gray-active">
                <td><?= $lv1->kdper; ?></td>
                <td><?= $lv1->nmper; ?></td>
                <td></td>
                <td class="text-center"></td>
                <td class="text-right"><?= numberFormat($lv1->nilai); ?></td>
                <td class="text-right"><?= numberFormat($ttl_realisasi_lv1_bln_ll); ?></td>
                <td class="text-right"><?= numberFormat($keu_realisasi_lv1); ?></td>
                <td class="text-right"><?= numberFormat($ttl_realisasi_lv1_sd_bln); ?></td>
                <td class="text-right"><?= numberFormat($lv1->nilai - $ttl_realisasi_lv1_sd_bln); ?></td>
                <td class="text-center"></td>
            </tr>
            <?php
            foreach ($get_dataDpaRekLv2 as $lv2) {
                $kdlv1 = substr($lv2->kdper_rep, 0, 1);
                if ($kdlv1 == $lv1->kdper_rep) {
                    $keu_realisasi_lv2 = 0;
                    $ttl_realisasi_lv2 = 0;
                    $ttl_realisasi_lv2_bln_ll = 0;
                    foreach ($get_dataDpaRekLv3 as $lv3) {
                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                        if ($kdlv2 == $lv2->kdper_rep) {
                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                if ($kdlv3 == $lv3->kdper_rep) {
                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                        if ($kdlv4 == $lv4->kdper_rep) {
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_lv2 += $dpa->keu_realisasi;
                                                    $ttl_realisasi_lv2 += $dpa->ttl_realisasi;
                                                    $ttl_realisasi_lv2_bln_ll += $dpa->ttl_realisasi_bln_ll;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $ttl_realisasi_lv2_sd_bln = $ttl_realisasi_lv2_bln_ll + $keu_realisasi_lv2;
                    ?>
                    <tr class="bg-gray-active">
                        <td><?= $lv2->kdper; ?></td>
                        <td><?= $lv2->nmper; ?></td>
                        <td></td>
                        <td></td>
                        <td class="text-right"><?= numberFormat($lv2->nilai); ?></td>
                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv2_bln_ll); ?></td>
                        <td class="text-right"><?= numberFormat($keu_realisasi_lv2); ?></td>
                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv2_sd_bln); ?></td>
                        <td class="text-right"><?= numberFormat($lv2->nilai - $ttl_realisasi_lv2_sd_bln); ?></td>
                        <td class="text-center"></td>
                    </tr>
                    <?php
                    foreach ($get_dataDpaRekLv3 as $lv3) {
                        $kdlv2 = substr($lv3->kdper_rep, 0, 2);
                        if ($kdlv2 == $lv2->kdper_rep) {
                            $keu_realisasi_lv3 = 0;
                            $ttl_realisasi_lv3 = 0;
                            $ttl_realisasi_lv3_bln_ll = 0;
                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                if ($kdlv3 == $lv3->kdper_rep) {
                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                        if ($kdlv4 == $lv4->kdper_rep) {
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_lv3 += $dpa->keu_realisasi;
                                                    $ttl_realisasi_lv3 += $dpa->ttl_realisasi;
                                                    $ttl_realisasi_lv3_bln_ll += $dpa->ttl_realisasi_bln_ll;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $ttl_realisasi_lv3_sd_bln = $ttl_realisasi_lv3_bln_ll + $keu_realisasi_lv3;
                            ?>
                            <tr class="bg-gray">
                                <td><?= $lv3->kdper; ?></td>
                                <td><?= $lv3->nmper; ?></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><?= numberFormat($lv3->nilai); ?></td>
                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv3_bln_ll); ?></td>
                                <td class="text-right"><?= numberFormat($keu_realisasi_lv3); ?></td>
                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv3_sd_bln); ?></td>
                                <td class="text-right"><?= numberFormat($lv3->nilai - $ttl_realisasi_lv3_sd_bln); ?></td>
                                <td class="text-center"></td>
                            </tr>
                            <?php
                            foreach ($get_dataDpaRekLv4 as $lv4) {
                                $kdlv3 = substr($lv4->kdper_rep, 0, 3);
                                if ($kdlv3 == $lv3->kdper_rep) {
                                    $keu_realisasi_lv4 = 0;
                                    $ttl_realisasi_lv4 = 0;
                                    $ttl_realisasi_lv4_bln_ll = 0;
                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                        if ($kdlv4 == $lv4->kdper_rep) {
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_lv4 += $dpa->keu_realisasi;
                                                    $ttl_realisasi_lv4 += $dpa->ttl_realisasi;
                                                    $ttl_realisasi_lv4_bln_ll += $dpa->ttl_realisasi_bln_ll;
                                                }
                                            }
                                        }
                                    }

                                    $ttl_realisasi_lv4_sd_bln = $ttl_realisasi_lv4_bln_ll + $keu_realisasi_lv4;
                                    ?>
                                    <tr class="bg-gray">
                                        <td><?= $lv4->kdper; ?></td>
                                        <td><?= $lv4->nmper; ?></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right"><?= numberFormat($lv4->nilai); ?></td>
                                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv4_bln_ll); ?></td>
                                        <td class="text-right"><?= numberFormat($keu_realisasi_lv4); ?></td>
                                        <td class="text-right"><?= numberFormat($ttl_realisasi_lv4_sd_bln); ?></td>
                                        <td class="text-right"><?= numberFormat($lv4->nilai - $ttl_realisasi_lv4_sd_bln); ?></td>
                                        <td class="text-center"></td>
                                    </tr>
                                    <?php
                                    foreach ($get_dataDpaRekLv5 as $lv5) {
                                        $kdlv4 = substr($lv5->kdper_rep, 0, 5);
                                        if ($kdlv4 == $lv4->kdper_rep) {
                                            $keu_realisasi_lv5 = 0;
                                            $ttl_realisasi_lv5 = 0;
                                            $ttl_realisasi_lv5_bln_ll = 0;
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_lv5 += $dpa->keu_realisasi;
                                                    $ttl_realisasi_lv5 += $dpa->ttl_realisasi;
                                                    $ttl_realisasi_lv5_bln_ll += $dpa->ttl_realisasi_bln_ll;
                                                }
                                            }
                                            $ttl_realisasi_lv5_sd_bln = $ttl_realisasi_lv5_bln_ll + $keu_realisasi_lv5
                                            ?>
                                            <tr class="bg-gray">
                                                <td><?= $lv5->kdper; ?></td>
                                                <td><?= $lv5->nmper; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right"><?= numberFormat($lv5->nilai); ?></td>
                                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv5_bln_ll); ?></td>
                                                <td class="text-right"><?= numberFormat($keu_realisasi_lv5); ?></td>
                                                <td class="text-right"><?= numberFormat($ttl_realisasi_lv5_sd_bln); ?></td>
                                                <td class="text-right"><?= numberFormat($lv5->nilai - $ttl_realisasi_lv5_sd_bln); ?></td>
                                                <td class="text-center"></td>
                                            </tr>
                                            <?php
                                            foreach ($get_dataDpaRincDet as $dpa) {
                                                if ($dpa->mtgkey == $lv5->mtgkey) {
                                                    $keu_realisasi_type_h = 0;
                                                    $ttl_realisasi_type_h = 0;
                                                    $ttl_realisasi_type_h_bln_ll = 0;
                                                    if ($dpa->type == 'H') {
                                                        foreach ($get_dataDpaRincDet as $dpaD) {
                                                            $kdjabatTD = substr($dpaD->kdjabar, 0, 3);
                                                            $kdjabatTH = $dpa->kdjabar;
                                                            if ($dpaD->mtgkey == $lv5->mtgkey and $kdjabatTH==$kdjabatTD and $dpaD->type == 'D') {
                                                                $keu_realisasi_type_h += $dpaD->keu_realisasi;
                                                                $ttl_realisasi_type_h += $dpaD->ttl_realisasi;
                                                                $ttl_realisasi_type_h_bln_ll += $dpaD->ttl_realisasi_bln_ll;
                                                            }
                                                        }
                                                    }
                                                    $ttl_realisasi_type_h_sd_bln = $ttl_realisasi_type_h_bln_ll + $keu_realisasi_type_h;
                                                    if ($dpa->type == 'D') {
                                                        $jumbyek = $dpa->jumbyek;
                                                        $satuan = $dpa->satuan;
                                                        $tarif = numberFormat($dpa->tarif);
                                                        $ttl_volume = !is_null($dpa->ttl_volume) ? $dpa->ttl_volume : 0;
                                                        $rea_volume = !is_null($dpa->rea_volume) ? $dpa->rea_volume : 0;
                                                        $ttl_realisasi = !is_null($dpa->ttl_realisasi) ? $dpa->ttl_realisasi : 0;
                                                        $ttl_realisasi_bln_ll = !is_null($dpa->ttl_realisasi_bln_ll) ? $dpa->ttl_realisasi_bln_ll : 0;
                                                        $keu_realisasi = !is_null($dpa->keu_realisasi) ? $dpa->keu_realisasi : 0;
                                                        $sisa_volume = $dpa->jumbyek - $ttl_volume;
                                                        $ttl_realisasi_sd_bln = $ttl_realisasi_bln_ll + $keu_realisasi;
                                                        $sisa_pagu = $dpa->subtotal - $ttl_realisasi_sd_bln;
                                                    } elseif ($dpa->type == 'H') {
                                                        $jumbyek = '';
                                                        $satuan = '';
                                                        $tarif = '';
                                                        $ttl_volume = '';
                                                        $rea_volume = '';
                                                        $ttl_realisasi = $ttl_realisasi_type_h;
                                                        $ttl_realisasi_bln_ll = $ttl_realisasi_type_h_bln_ll;
                                                        $keu_realisasi = $keu_realisasi_type_h;
                                                        $sisa_volume = '';
                                                        $ttl_realisasi_sd_bln = $ttl_realisasi_type_h_bln_ll + $keu_realisasi_type_h;
                                                        $sisa_pagu = $dpa->subtotal - $ttl_realisasi_type_h;
                                                    }
                                                    ?>
                                                    <tr >
                                                        <td></td>
                                                        <td><?= $dpa->uraian; ?></td>
                                                        <td class="text-center"><?= $jumbyek . ' ' . $satuan ?></td>
                                                        <td class="text-right"><?= $tarif; ?></td>
                                                        <td class="text-right"><?= numberFormat($dpa->subtotal); ?></td>
                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_bln_ll); ?></td>
                                                        <td class="text-right"><?= numberFormat($keu_realisasi); ?></td>
                                                        <td class="text-right"><?= numberFormat($ttl_realisasi_sd_bln); ?></td>
                                                        <td class="text-right"><?= numberFormat($sisa_pagu); ?></td>
                                                        <td class="text-center">
                                                            <?php
                                                            if ($dpa->type == 'D') {
                                                                echo $satuan != 'Tahun  ' ? numberFormat($jumbyek - $dpa->ttl_volume) . ' ' . $satuan : '1 Tahun';
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        ?>
    </tbody>
</table>
