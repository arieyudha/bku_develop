<?php
$url = getUrl();
echo $javasc;
echo $notifikasi;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading bg-gray">
                <h3 class="panel-title">Data Program dan Kegiatan</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <form  name='fBulan' method='get'>
                            <div class="col-md-4">
                                <label>Pilih Bulan</label>
                                <div  class="form-group">
                                    <select class="form-control select2 kd_bulan" style="width: 100%;" name='kd_bulan' onchange='document.fBulan.submit();'>
                                        <?php
                                        foreach ($getBulan as $bln) {
                                            $att = '';
                                            if ($bln->kd_bulan == $kd_bulan) {
                                                $att = 'selected';
                                            }
                                            echo '<option ' . $att . ' 
                                        value="' . $bln->kd_bulan . '">' . $bln->nama_bulan . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" class="form-control kd_jabatan" name="kd_jabatan" value="<?= $kd_jabatan; ?>">
                        </form>
                    </div>
                </div>

                <table class="table table-hover table-bordered tabel_3" width='100%'>
                    <thead>
                        <tr>
                            <th width="5%">Kode</th>
                            <th>Nama Program / Kegiatan</th>
                            <th>Pagu Anggaran</th>
                            <th>Realisasi Bulan <?= $row_bln->nama_bulan; ?></th>
                            <th>Total Realisasi</th>
                            <th>Sisa Anggaran</th>
                            <th width="15%"><i class="fa fa-refresh"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $pagu_apbd = 0;
                        $rea_keu_prog_bln = 0;
                        $rea_keu_prog_all = 0;
                        foreach ($get_dataDpaProgram as $row_prog) {
                            $rea_keu_prog = 0;
                            $ttl_keu_prog = 0;
                            $pagu_apbd += $row_prog->pagu_prog;
                            foreach ($get_dataReaKegPptk as $row_keg) {
                                if ($row_prog->idprgrm == $row_keg->idprgrm) {
                                    $rea_keu_prog += $row_keg->keu_realisasi;
                                    $ttl_keu_prog += $row_keg->ttl_realisasi;
                                    $rea_keu_prog_bln += $row_keg->keu_realisasi;
                                    $rea_keu_prog_all += $row_keg->ttl_realisasi;
                                }
                            }
                            ?>
                            <tr style="background-color: #e2e1e1">
                                <td><?= $row_prog->nuprgrm; ?></td>
                                <td><?= $row_prog->nmprgrm; ?></td>
                                <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_prog->pagu_prog); ?></td>
                                <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog); ?></td>
                                <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($ttl_keu_prog); ?></td>
                                <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_prog->pagu_prog - $ttl_keu_prog); ?></td>
                                <td></td>
                            </tr>
                            <?php
                            foreach ($get_dataReaKegPptk as $row_keg) {
                                if ($row_prog->idprgrm == $row_keg->idprgrm) {

                                    if ($row_keg->status_posting == 'N') {
                                        $bg_color = '';
                                        $attrBtn = 'disabled';
                                    } else {
                                        $bg_color = 'bg-green';
                                        $attrBtn = '';
                                    }
                                    ?>
                                    <tr class="<?= $bg_color; ?>">
                                        <td><?= $row_prog->nuprgrm . $row_keg->nukeg; ?></td>
                                        <td><?= $row_keg->nmkegunit; ?></td>
                                        <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->pagu); ?></td>
                                        <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->keu_realisasi); ?></td>
                                        <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->ttl_realisasi); ?></td>
                                        <td class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($row_keg->pagu - $row_keg->ttl_realisasi); ?></td>
                                        <td class="no-padding">
                                            <?php if (is_null($row_keg->kode_qr) || $row_keg->kode_qr == '') { ?>
                                                <button <?= $attrBtn; ?>
                                                    data-toggle="modal"
                                                    data-target="#modal-laporan"
                                                    data-kdkegunit="<?= $row_keg->kdkegunit; ?>"
                                                    data-idprgrm="<?= $row_keg->idprgrm; ?>"
                                                    data-kdtahap="<?= $row_keg->kdtahap; ?>"
                                                    data-kd_jabatan="<?= $row_keg->kd_jabatan; ?>"
                                                    class="btn btn-primary btn-block btn-flat">
                                                    <i class="fa fa-code"></i> Laporan
                                                </button>
                                            <?php } else { ?>
                                                <form target="_blank" action="<?= site_url('laporan/Buku_kas/buku_kas_umum'); ?>" method="get">
                                                    <input type="hidden" class="form-control" name="kd_bulan" value="<?= $kd_bulan; ?>">
                                                    <input type="hidden" class="form-control " name="kd_jabatan" value="<?= $row_keg->kd_jabatan; ?>">
                                                    <input type="hidden" class="form-control " name="idprgrm" value="<?= $row_keg->idprgrm; ?>">
                                                    <input type="hidden" class="form-control" name="kdkegunit" value="<?= $row_keg->kdkegunit; ?>">
                                                    <button class="btn btn-danger btn-flat btn-block">
                                                        <i class="fa fa-print"></i> Laporan
                                                    </button>
                                                </form>
                                            <?php }
                                            ?>
                                        </td>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">TOTAL</th>
                            <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_apbd); ?></th>
                            <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog_bln); ?></th>
                            <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($rea_keu_prog_all); ?></th>
                            <th class="text-right"> <span class="pull-left">Rp. </span><?= numberFormat($pagu_apbd - $rea_keu_prog_all); ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-laporan" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title label_head" id=""></h4>
            </div>
            <form class="form-laporan" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Tanggal Laporan</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon bg-gray">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" class="form-control tgl_laporan" name="tgl_laporan" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Nama PPTK</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control nama_pptk" name="nama_pptk" readonly required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>NIP PPTK</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control nip_pptk" name="nip_pptk" readonly required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Jabatan PPTK</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control jabatan_pptk" name="jabatan_pptk" readonly required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Pangkat PPTK</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control pangkat_pptk" name="pangkat_pptk" readonly required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Golongan PPTK</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control golongan_pptk" name="gol_pptk" readonly required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control kd_bulan" name="kd_bulan" value="<?= $kd_bulan; ?>">
                    <input type="hidden" class="form-control kdkegunit" name="kdkegunit">
                    <input type="hidden" class="form-control kdtahap" name="kdtahap">
                    <input type="hidden" class="form-control idprgrm" name="idprgrm">
                    <input type="hidden" class="form-control url" name="url" value="<?= $url; ?>">
                    <input type="hidden" class="form-control kode_qr" name="kode_qr" value="<?= str_shuffle(date('YmdHsi')); ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $('#modal-laporan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var kdkegunit = button.data('kdkegunit');
        var kdtahap = button.data('kdtahap');
        var idprgrm = button.data('idprgrm');
        var kd_jabatan = button.data('kd_jabatan');
        var modal = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '<?= site_url('laporan/Buku_kas/getPptk') ?>',
            data: {kd_jabatan: kd_jabatan},
            success: (respon) => {
                $.each(respon, (i, res) => {
                    $('.nama_pptk').val(res.nama_lengkap);
                    $('.nip_pptk').val(res.nip);
                    $('.jabatan_pptk').val(res.nama_jabatan);
                    $('.pangkat_pptk').val(res.pangkat);
                    $('.golongan_pptk').val(res.golongan);
                });
            }

        });
        modal.find('.modal-body input.kdkegunit').val(kdkegunit);
        modal.find('.modal-body input.idprgrm').val(idprgrm);
        modal.find('.modal-body input.kdtahap').val(kdtahap);
        $('.label_head').html('Form Laporan');
        $('.form-laporan').attr('action', '<?= site_url('laporan/Buku_kas/inputLaporan'); ?>');
    });
</script>