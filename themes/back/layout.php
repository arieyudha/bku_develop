<!DOCTYPE html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
<html>
    <head>
        <?= $head; ?>
    </head>
    <body class="hold-transition skin-green-light sidebar-mini fixed">
        <div class="wrapper">
            <header class="main-header">
                <?= $nav_header; ?>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <?= $nav; ?>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php if (isset($ribbon_left)) { ?>
                    <section class="content-header bg-gray-active" style="padding-bottom: 15px">
                        <h1>
                            <?= $ribbon_left; ?>
                        </h1>
                        <ol class="breadcrumb">
                            <?= $ribbon_right; ?>
                        </ol>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                    </section>
                <?php } ?>

                <!-- Main content -->
                <div id='notivs'></div>
                <section class="content">
                    <?= $content; ?>
                </section>
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer bg-gray-active">
                <?= $footer; ?>
            </footer>

            <!-- Control Sidebar -->

        </div>
    </body>

</html>