<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>View</title>

        <?= js_asset('jquery.min.js', 'bower_components/jquery/dist/'); ?>
        <?= js_asset('bootstrap.min.js', 'bower_components/bootstrap/dist/js/'); ?>

        <?= css_asset('bootstrap.min.css', 'bower_components/bootstrap/dist/css/'); ?>
        <?= css_asset('font-awesome.min.css', 'bower_components/font-awesome/css/'); ?>
        <style type="text/css">
            hr {
                display: block;
                height: 3px;
                border: 0;
                border-top: 1px solid #000000;
                margin: 1em 0;
                padding: 0;
            }
            body{
                font-size: 11pt;
                text-align: left;
            }
            table .main  tr td {
                font-size: 10pt;
                text-align: left;
            }
            table, table .main {
                width: 100%;
                border-collapse: collapse;
                background: #fff;
                text-align: left;

            }
            table tr th{
                padding: 3px;
                border-collapse: collapse;
                text-align: center;
            }

            table tr td{
                vertical-align: top;
                padding: 3px;
            }

            table .padding_8 tr td{
                padding: 8px;
            }
            table .padding_3 tr th{
                padding: 3px;
            }
            table .padding_3 tr td{
                padding: 3px;
            }
            .left {text-align: left;}
            .putus { border-bottom: 1px dotted #666; border-top: 1px dotted #666; }
            .bawah { border-bottom: 0px ; }
            .border-bawah { border-bottom: 2pt ; }
            .atas { border-top: 0px ; }
            .kanan { border-right: 0px ; }
            .kiri { border-left: 0px ; }
            .all { border: 1px solid #666; }
            .center {text-align: center;}
            .form-check{
                display:inline-block; 
                position:relative; 
                width:50px; 
                height:25px;
            }
            body {
                margin: 0;
                padding: 0;
                font: 11pt "Arial";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 21cm;
                min-height: 29.7cm;
                padding: 1cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 1px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
        </style>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
    <body >
        <?php echo $content; ?>
    </body>
    <script src="<?php echo base_url(); ?>assets/bower_components/fancybox/jquery.fancybox.js"></script>
</html>

<script>
    $(document).ready(function () {
        $('.fancybox').fancybox();
    });
</script>