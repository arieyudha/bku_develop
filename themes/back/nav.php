<?php
defined('BASEPATH') or exit('No direct script access allowed');
$a = aksesLog();

?>


<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src='<?= logoKab(); ?>' class='img-circle' alt='User Image' />
        </div>
        <div class="pull-left info">
            <p  ><?= $a['nama_user']; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul id='navig' class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Utama</li>
        <?php
        foreach ($get_menu->result() as $row) {
            if ($row->parent == 0) {
                if ($row->link != '#') {
                    ?>
                    <li class="">
                        <a href="<?= site_url($row->link); ?>" title="Urusan">
                            <i class="fa <?= $row->icon; ?>"></i> <span class="menu-item-parent"><?= $row->nama; ?></span>
                            <!--                            <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>-->
                        </a>
                    </li>
                <?php } else { ?>
                    <li class="treeview">
                        <a href="#" title="Urusan">
                            <i class="fa <?= $row->icon; ?>"></i> <span class="menu-item-parent"><?= $row->nama; ?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <?php
                            foreach ($get_menu->result() as $row_p) {
                                if ($row_p->parent == $row->id) {
                                    if ($row_p->link != '#') {
                                        ?>
                                        <li><a href="<?= site_url($row_p->link); ?>"><i class="fa <?= $row_p->icon; ?>"></i> <?= $row_p->nama; ?></a></li>
                                    <?php } else { ?>
                                        <li class="treeview">
                                            <a href="#" title="Urusan">
                                                <i class="fa <?= $row_p->icon; ?>"></i> <span class="menu-item-parent"><?= $row_p->nama; ?></span>
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php
                                                foreach ($get_menu->result() as $row_ps) {
                                                    if ($row_ps->parent == $row_p->id) {
                                                        ?>
                                                        <li><a href="<?= site_url($row_ps->link); ?>"><i class="fa <?= $row_ps->icon; ?>"></i> <?= $row_ps->nama; ?></a></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
            }
        }
        ?>
    </ul>
</nav>
<!--<script> if (!window.jQuery) {
    document.write('<script src="<?= base_url(); ?>assets/js/libs/jquery-2.1.1.min.js"><\/script>');
}</script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?= js_asset('jquery.min.js', 'bower_components/jquery/dist/'); ?>
<script>
<?php
$ci = &get_instance();
$folder = $ci->uri->segment(1);
$controller = $ci->uri->segment(2);
$method = $ci->uri->segment(3);
if (empty($folder) and empty($method)) {
    $url = $controller;
} elseif (empty($method)) {
    $url = $folder . '/' . $controller;
} else {
    $url = $folder . '/' . $controller . '/' . $method;
}
?>
    $(function () {
        $('#navig a[href~="<?= site_url($url); ?>"]').parents('li').addClass('active');

    })
</script>