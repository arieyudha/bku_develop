<?php
$a = aksesLog();
?>

<a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>DM</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><?= $a['ket_level']; ?></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <ul class="nav navbar-nav hidden-xs">
        <li class="">
            <a href="#" class="btn btn-flat">
                Hari dan Tanggal : <?= Tgl_indo::hari(date('D')) . ', ' . Tgl_indo::indo_angka(date('Y-m-d')); ?>
            </a>
        </li>
    </ul>
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src='<?=logoKab();?>' class='user-image' alt='User Image' />
                    <span class="hidden-xs"><br></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="<?= logoKab(); ?>" class=" img-circle" alt="User Image">

                        <p>
                            <?= $a['nama_user']; ?>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="<?= base_url('setting/Profil'); ?>" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?= base_url('home/Login/logout/' . $a['kd_user']); ?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <!--            <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>-->
        </ul>
    </div>
</nav>