<!-- ./wrapper -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>v-->

<?= js_asset('sweetalert2.min.js', 'plugins/sweetalert/dist/'); ?>
<?= js_asset('jquery.min.js', 'bower_components/jquery/dist/'); ?>
<?= js_asset('jquery-ui.min.js', 'bower_components/jquery-ui/'); ?>
<?= js_asset('bootstrap.min.js', 'bower_components/bootstrap/dist/js/'); ?>
<?= js_asset('raphael.min.js', 'bower_components/raphael/'); ?>
<?= js_asset('morris.min.js', 'bower_components/morris.js/'); ?>
<?= js_asset('jquery.sparkline.min.js', 'bower_components/jquery-sparkline/dist/'); ?>
<?= js_asset('bootstrap3-wysihtml5.all.min.js', 'plugins/bootstrap-wysihtml5/'); ?>
<?= js_asset('demo.js', 'dist/js/'); ?>
<?= js_asset('adminlte.min.js', 'dist/js/'); ?>
<?= js_asset('jquery.slimscroll.min.js', 'bower_components/jquery-slimscroll/'); ?>
<?= js_asset('fastclick.js', 'bower_components/fastclick/lib/'); ?>
<?= js_asset('moment.js', 'bower_components/moment/'); ?>
<?= js_asset('fullcalendar.min.js', 'bower_components/fullcalendar/dist/'); ?>
<?= js_asset('jquery.dataTables.min.js', 'bower_components/datatables.net/js/'); ?>
<?= js_asset('dataTables.bootstrap.min.js', 'bower_components/datatables.net-bs/js/'); ?>
<?= js_asset('dataTables.bootstrap.min.js', 'bower_components/datatables.net-bs/js/'); ?>
<?= js_asset('jquery.inputmask.js', 'plugins/input-mask/'); ?>
<?= js_asset('jquery.inputmask.date.extensions.js', 'plugins/input-mask/'); ?>
<?= js_asset('jquery.inputmask.extensions.js', 'plugins/input-mask/'); ?>
<?= js_asset('daterangepicker.js', 'bower_components/bootstrap-daterangepicker/'); ?>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>
<!--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>
<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>-->
<script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>
<script src="<?= base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?= base_url(); ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.autocomplete.js"></script>
  <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });

    function callBackClassAfter(classParent, classAfter) {
        $(classParent).after(`<span class="${classAfter}"></span>`).css('margin-right', '10px');
        $(classParent).keyup(function () {
            $(this).css({'border': '1px solid #ccc', 'background': 'none'});
        });
    }

    $(document).ready(function () {
        $('.tabel_3').DataTable({
            "scrollY": "60vh",
            "scrollX": true,
            "scrollCollapse": true,
            'ordering': false,
            "paging": false
        });
        $('.tabel_2').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': true,
            'pageLength': 50

        });

        $('.tabel_1').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true,
            'pageLength': 100

        });
        /* END TABLETOOLS */
        setTimeout(function () {
            $('#notiv').fadeOut('slow');
        }, 4000);
    });

    function notif_smartAlertSukses(ket) {
        Swal({
            type: 'success',
            title: ket,
            timer: 2500,
            onBeforeOpen: () => {
                Swal.showLoading()
            }
        }).then((result) => {
            window.location.reload();
        });
    }
    function notif_smartAlertGagal(ket) {
        Swal({
            position: 'top',
            type: 'error',
            title: ket,
            showConfirmButton: false,
            timer: 2000
        });
    }

    $(document).ready(function () {
        $('.overlay').hide();

        // Format mata uang.
//        $('.uang').mask('0.000.000.000.000', {reverse: true});
        $('.duit').mask('0.000.000.000.000', {reverse: true});

        // Format nomor HP.
        $('.no_hp').mask('0000−0000−0000');

        // Format tahun pelajaran.
        $('.tapel').mask('0000/0000');
    });

</script>