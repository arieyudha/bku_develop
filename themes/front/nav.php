
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
<div class="login-info">
    <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
        <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <?php
            echo "<img src='" . base_url() . "assets/img/logo.png' class='online' />";
            ?>
            <span>
                Pemprov Kalsel
            </span>
        </a> 

    </span>
</div>
<!-- end user info -->

<!-- NAVIGATION : This navigation is also responsive-->
<nav>
    <ul id='navig'>

        <li class="">
            <a href="<?=site_url('frontend/Home');?>" title="Urusan">
                <i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Dashboard</span>
            </a>
        </li>
        <li class="">
            <a href="<?=site_url('frontend/Rincian_anggaran');?>" title="Urusan">
                <i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Rincian Anggaran</span>
            </a>
        </li>
    </ul>	
</nav>
<script> 
    if (!window.jQuery) {
        document.write('<script src="<?= base_url(); ?>assets/js/libs/jquery-2.1.1.min.js"><\/script>'); 
    }
</script>
