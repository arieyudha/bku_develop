<!DOCTYPE html>
<html lang="en-us" class="">
    <head>
        <?= $head; ?>
    </head>
    <body class="desktop-detected pace-done mobile-view-activated fixed-header fixed-navigation">

        <!-- HEADER -->
        <header id="header" >
            <?= $nav_header; ?>
        </header>
        <!-- END HEADER -->

        <!-- Left panel : Navigation area -->
        <!-- Note: This width of the aside area can be adjusted through LESS variables -->
        <aside id="left-panel">
            <?= $nav; ?>
        </aside>

        <div id="main" role="main">
                <div id="ribbon" class="bg-gray-active">
                    
                </div>
                <div id='notivs'></div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <?= $content; ?>
            </div>
        </div>


        <div class="page-footer bg-gray-light" >
            <?= $footer; ?>
        </div>
        <!-- END PAGE FOOTER -->

        <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
        Note: These tiles are completely responsive,
        you can add as many as you like
        -->

    </body>

</html>