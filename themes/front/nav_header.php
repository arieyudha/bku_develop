<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
<span class="hidden-xs">
    <div id="logo-group" >
        <span id="logo" style="color:#000;font-size: 14pt;"><center><strong>E-Monev Prov</strong></center></span>
    </div>
</span>
<div class="pull-left" >
    <div id="hide-menu" class="btn-header pull-right">
        <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
    </div>
</div>
<div class="pull-right">
    <div id="logout" class="btn-header transparent pull-right">
        <span> <a href="<?= site_url('login/Login'); ?>" title="Sign Out"><i class="fa fa-sign-in"></i> Login</a> </span>
    </div>
    <div id="fullscreen"  class="btn-header transparent pull-right" >
        <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen">
                <i class="fa fa-arrows-alt"></i></a> </span>
    </div>
</div>