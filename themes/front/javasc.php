<!--<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?= base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>-->
<script> if (!window.jQuery) {
        document.write('<script src="<?= base_url(); ?>assets/js/libs/jquery-2.1.1.min.js"><\/script>'); }</script>
<script> if (!window.jQuery.ui) {
        document.write('<script src="<?= base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }</script>
<script src="<?= base_url(); ?>assets/js/app.config.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
<script src="<?= base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<script src="<?= base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!--<script src="<?= base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>-->
<script src="<?= base_url(); ?>assets/js/app.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/x-editable/x-editable.min.js"></script>
<script src="<?= base_url(); ?>assets/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/croppie.js"></script>
<script src="<?php echo base_url(); ?>assets/js/froala_editor.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(function () {
        $('.textedit').froalaEditor({
            heightMin: 200,
            heightMax: 200,
            toolbarButtons: ['fontFamily', '|', 'fontSize', '|', 'paragraphFormat', '|', 'bold', 'italic', 'underline', 'undo', 'redo', 'codeView', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent'],
            fontFamilySelection: true,
            fontSizeSelection: true,
            paragraphFormatSelection: true,

        })
    });

    $('.validate-form select').on('change', function (e) {
        $('.validate-form').validate().element($(this));
    });
    $(document).ready(function () {
        pageSetUp();

        $('.tabel_3').DataTable({
            "scrollY": "80vh",
            "scrollX": true,
            "scrollCollapse": true,
            'ordering': false,
            "paging": false
        });
        $('.tabel_31').DataTable({
            "scrollY": "80vh",
            "scrollX": true,
            "scrollCollapse": true,
            'ordering': true,
            "paging": false
        });
        $('.tabel_2').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true,
            'pageLength': 25

        });

        $('.tabel_1').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': false,
            'autoWidth': true,
            'pageLength': 100

        });
        /* END TABLETOOLS */

    });


    setTimeout(function () {
        $('#notiv').fadeOut('slow');
    }, 4000);

    $(document).ready(function () {
        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target), action = target.data('action');
            if (action === 'expand-all') {
                $('.tut').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.tut').nestable('collapseAll');
            }
        });
        $('#nestable3').nestable();
    })

    function sukses(ket) {
        $("#notivs").html('<div class="alert alert-success alert-dismissable animated fadeIn" id="notification"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <h4><i class="icon fa fa-check"></i> Sukses!</h4> Berhasil di ' + ket + '. </div>');
        $('#close-modal').trigger("click");
        $('.close-modal').trigger("click");
        setTimeout(function () {
            location.reload();
            $('#notification').fadeOut('slow');
        }, 2000);
    }
    function gagal(ket) {
        $("#notivs").html('<div class="alert alert-danger alert-dismissible" id="alert-notification"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-ban"></i> Peringatan!</h4> Gagal Di Simpan. </div>');
        $('#close-modal').trigger("click");
        $('.close-modal').trigger("click");
        setTimeout(function () {
            $('#alert-notification').fadeOut('slow');
        }, 2000);
    }

    function notif_alert_sukses(ket) {
        $.smallBox({
            title: "Berhasil",
            content: "<i class='fa fa-check'></i> <i>" + ket + "</i>",
            color: "#296191",
            iconSmall: "fa fa-thumbs-up bounce animated",
            timeout: 2000
        });
        setTimeout(function () {
            location.reload();
        }, 2000);
    }

    function notif_sukses_noReload(ket) {
        $.smallBox({
            title: "Berhasil",
            content: "<i class='fa fa-check'></i> <i>" + ket + "</i>",
            color: "#296191",
            iconSmall: "fa fa-thumbs-up bounce animated",
            timeout: 2000
        });
        $('.close-modal').trigger("click");
        setTimeout(function () {
        }, 2000);
    }

    function notif_alert_gagal(ket) {
        $.smallBox({
            title: "Gagal !!!",
            content: "<i class='fa fa-remove'></i> <i>" + ket + "</i>",
            color: "#990F0F",
            iconSmall: "fa fa-thumbs-up bounce animated",
            timeout: 4000
        });
    }
//    $(document).ready(function () {
//        $(".preloader").fadeOut();
//    })

    $(window).bind("load", function () {
        $(".preloader").fadeOut('slow')
    });
</script>
