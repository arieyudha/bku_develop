-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table tpp_tapin.ref_kategori_cari
DROP TABLE IF EXISTS `ref_kategori_cari`;
CREATE TABLE IF NOT EXISTS `ref_kategori_cari` (
  `kode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kategori` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table tpp_tapin.ref_kategori_cari: 3 rows
DELETE FROM `ref_kategori_cari`;
/*!40000 ALTER TABLE `ref_kategori_cari` DISABLE KEYS */;
INSERT INTO `ref_kategori_cari` (`kode`, `kategori`) VALUES
	('nama', 'Nama'),
	('nip', 'NIP'),
	('jabatan', 'Jabatan');
/*!40000 ALTER TABLE `ref_kategori_cari` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
