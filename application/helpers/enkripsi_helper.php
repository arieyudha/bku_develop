<?php

function _enkripsiDB()
{
  $ci = &get_instance();
  $ci->load->library('encryption');
  $data = array(
    'hostname' => $ci->encryption->encrypt('localhost'),
    'username' => $ci->encryption->encrypt('root'),
    'password' => $ci->encryption->encrypt(''),
    'database' => $ci->encryption->encrypt('bku_develop'),
  );
  return $data;
}

function _deskripsiDB()
{
  $ci = &get_instance();
  $ci->load->library('encryption');
  $get = _enkripsiDB();
  $data = array(
    'hostname' => $ci->encryption->decrypt($get['hostname']),
    'username' => $ci->encryption->decrypt($get['username']),
    'password' => $ci->encryption->decrypt($get['password']),
    'database' => $ci->encryption->decrypt($get['database']),
  );
  return $data;
}

function enkripsiText($text)
{
  $ci = &get_instance();
  $ci->load->library('encryption');
  return $ci->encryption->encrypt($text);
}
function deskripsiText($text)
{
  $ci = &get_instance();
  $ci->load->library('encryption');
  return $ci->encryption->decrypt($text);
}
