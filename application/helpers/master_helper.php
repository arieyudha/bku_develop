<?php

function listBulan()
{
  return array(
    1 => 'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember',
  );
}

function _unitkey()
{
  return '94_';
}

function genNoRek($dataRek, $l)
{
  $noRek = "";
  for ($i = 1; $i <= $l; $i++) {
    $t = $i < $l ? '.' : '';
    $noRek .= $dataRek['kd_rek_' . $i] . $t;
  }
  return $noRek;
}

function extrackNoRek($dataRek, $length)
{
  $length -= 1;
  $pecahKdRek = explode(".", $dataRek);
  $kd_rek = array();
  for ($i = 0; $i <= $length; $i++) {
    $kd = $i;
    array_push($kd_rek, [
      'kd_rek_' . ($kd + 1) => $pecahKdRek[$i]
    ]);
  }
  return $kd_rek;
}

function builUrl($param)
{
  return http_build_query($param) . "\n";
}
