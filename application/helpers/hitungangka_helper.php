<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of hitungangka_helper
 *
 * @author BappedaKalsel
 */
function pendapatanTpp($nip = null) {
    $ci = &get_instance();
    if ($nip == '') {
        $data = $ci->db->query("select a.*, d.nm_unit, b.nip, c.absen, c.tpp, c.uang, 
round((a.job_value*c.uang)*a.penyeimbang) as total_tpp,
round((a.job_value*c.uang)*a.penyeimbang)*c.absen/100 as nilai_absen,
round((a.job_value*c.uang)*a.penyeimbang)*c.tpp/100 as nilai_tpp 
from ref_peta_jabatan a 
join data_pegawai b on a.kd_jabatan=b.kd_jabatan
join ref_unit d on a.kd_urusan=d.kd_urusan and a.kd_bidang=d.kd_bidang and a.kd_unit=d.kd_unit
join set_persentase_tpp c
order by a.kelas_jabatan desc, a.job_value desc");
    } else {
        $data = $ci->db->query("select a.*, d.nm_unit, b.nip, c.absen, c.tpp, c.uang, 
round((a.job_value*c.uang)*a.penyeimbang) as total_tpp,
round((a.job_value*c.uang)*a.penyeimbang)*c.absen/100 as nilai_absen,
round((a.job_value*c.uang)*a.penyeimbang)*c.tpp/100 as nilai_tpp 
from ref_peta_jabatan a 
join data_pegawai b on a.kd_jabatan=b.kd_jabatan
join ref_unit d on a.kd_urusan=d.kd_urusan and a.kd_bidang=d.kd_bidang and a.kd_unit=d.kd_unit
join set_persentase_tpp c where b.nip='$nip'
order by a.kelas_jabatan desc, a.job_value desc");
    }
    return $data;
}
