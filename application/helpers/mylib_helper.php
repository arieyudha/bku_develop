<?php

function ribbon_left($name_page = NULL, $name_page2 = NULL) {
    $data = $name_page;
    if ($name_page2 != '') {
        $data .= '<small>' . $name_page2 . '</small>';
    }
    return $data;
}

function ribbon_right($name_page = NULL, $name_page2 = NULL) {
    $data = '<li><a href="#"><i class="fa fa-dashboard"></i> ' . $name_page . '</a></li>';
    if ($name_page2 != '') {
        $data .= '<li class="active">' . $name_page2 . '</li>';
    }
    return $data;
}

function cetak($str) {
    echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function urlApi($params) {
    $url = "http://localhost:81/api_dpa_sipkd/index.php/$params";
    return $url;
}

function urlApiOffice($params) {
    $url = "http://172.17.10.1/eoffice/rest_api_surat/index.php/$params";
    return $url;
}

function base_url_ol($uri) {
    $ci = &get_instance();
    $que = $ci->db->query("select * from http_url where ket='OL'")->row_array();
    if (is_null($que['folder_link'])) {
        $url_data = $que['http_link'] . $que['domain_link'] . ':' . $que['port_link'] . '/' . $uri;
    } else {
        $url_data = $que['http_link'] . $que['domain_link'] . ':' . $que['port_link'] . '/' . $que['folder_link'] . '/' . $uri;
    }
    return $url_data;
}

function base_url_lokal($uri) {
    $ci = &get_instance();
    $que = $ci->db->query("select * from http_url where ket='OF'")->row_array();
    $url_data = $que['http_link'] . $que['domain_link'] . ':' . $que['port_link'] . '/' . $que['folder_link'] . '/' . $uri;
    echo $url_data;
}

//function jumlahHariBulan($month, $year, $haker) {
//    $tglAwal = date('Y-m-01', mktime(0, 0, 0, $month, 1, $year));
//    $tglAkhir = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
//    $aw = strtotime($tglAwal);
//    $ak = strtotime($tglAkhir);
//    $hariKerja = array();
//    if ($haker == 5) {
//        for ($i = $aw; $i <= $ak; $i += (60 * 60 * 24)) {
//            if (date('w', $i) !== '0' && date('w', $i) !== '6') {
//                $hariKerja[] = $i;
//            }
//        }
//    } elseif ($haker == 6) {
//        for ($i = $aw; $i <= $ak; $i += (60 * 60 * 24)) {
//            if (date('w', $i) !== '0') {
//                $hariKerja[] = $i;
//            }
//        }
//    }
//    $jmlHrKerja = count($hariKerja);
//    return $jmlHrKerja;
//}
//
//function jumlahHariKerjaBulan($kd_level) {
//    $ci = &get_instance();
//    $a = aksesLog();
//    if ($kd_level == 1) {
//        $kd_urusan = '4';
//        $kd_bidang = '5';
//        $kd_unit = '7';
//    } else {
//        $kd_urusan = $a['kd_urusan'];
//        $kd_bidang = $a['kd_bidang'];
//        $kd_unit = $a['kd_unit'];
//    }
//    $query = $ci->db->query("select * from set_haker_skpd a where a.kd_urusan=$kd_urusan and a.kd_bidang=$kd_bidang and a.kd_unit=$kd_unit")->row_array();
//    $data = jumlahHariBulan(date('m'), date('Y'), $query['hari_kerja']);
//    return $data;
//}

function formatDatePhp($tanggal) {
    $date = DateTime::createFromFormat('d/m/Y', $tanggal);
    return $date->format('Y-m-d');
}

function jsonArray($array) {
    $ci = &get_instance();
    return $ci->output->set_content_type('application/json')->set_output(json_encode($array));
}

function jsonCurl($params) {
    $ci = &get_instance();
    $ci->load->library('curl');
    return json_decode($ci->curl->simple_get($params));
}

function escape_string($data) {
    $ci = &get_instance();
    return $ci->db->escape($data);
}

function getPegawaiNip($nip) {
    $ci = &get_instance();
    $ci->load->model('database/Model_database');
    $data = $ci->Model_database->getPegawaiUnitWhereNip($nip)->row_array();
    return $data;
}

function statusPegawai($nip) {
//    $nip = '198709212015032003';
    $get = getPegawaiNip($nip);
    $data = array();
    $data['status_pegawai'] = $get['status_pegawai'];
    if ($get['status_pegawai'] == 'PNS') {
        $data['persen'] = 100;
    } else {
        $data['persen'] = 80;
    }
    return $data;
}

function besarKecil($kalimat) {
    return ucwords(strtolower($kalimat));
}

function statusWarning($status = null, $ket = null) {
    $data = '
        <div class="alert alert-warning"><h4><i class="icon fa fa-warning"></i>' . $status . '</h4>
            ' . $ket . '
        </div>';
    echo $data;
}

function numberFormat($value, $jml = NULL) {
    return number_format($value, $jml, ',', '.');
}

function aksesLog() {
    $ci = &get_instance();
    return $ci->session->userdata('is_logined');
}

function btn_tambah($attr = '', $ket = '', $class = '') {
    $a = aksesLog();
    $m = aksesMenu($a['kd_user']);
    $rul = $m['rul_tambah'];
    echo "<button $rul class='btn btn-primary btn-flat $class' $attr><i class='fa fa-plus'></i> $ket</button>";
}

function btn_edit($attr = '', $ket = '', $class = '') {
    $a = aksesLog();
    $m = aksesMenu($a['kd_user']);
    $rul = $m['rul_edit'];
    echo "<button $rul class='btn btn-warning btn-flat $class' $attr><i class='fa fa-pencil'></i> $ket</button>";
}

function btn_hapus($attr = '', $ket = '', $class = '') {
    $a = aksesLog();
    $m = aksesMenu($a['kd_user']);
    $rul = $m['rul_hapus'];
    echo "<button $rul class='btn btn-danger btn-flat $class' $attr><i class='fa fa-trash'></i> $ket</button>";
}

function aksesMenu($kd_user) {
    $ci = &get_instance();
    $folder = $ci->uri->segment(1);
    $controller = $ci->uri->segment(2);
    $method = $ci->uri->segment(3);
    if (empty($folder) and empty($method)) {
        $url = $controller;
    } elseif (empty($method)) {
        $url = $folder . '/' . $controller;
    } else {
        $url = $folder . '/' . $controller . '/' . $method;
    }
    $query = $ci->db->query("select b.link from menu_role a join menu b on a.id_menu=b.id where a.kd_user=$kd_user and b.link='$url'")->row_array();
    if ($query['link'] != $url) {
        if (empty($folder) and empty($method)) {
            $link = $controller;
        } else {
            $link = $folder . '/' . $controller;
        }
        return $ci->db->query("select a.*, b.*,
if(a.lihat=1, '', 'disabled') as rul_lihat,
if(a.tambah=1, '', 'disabled') as rul_tambah,
if(a.edit=1, '', 'disabled') as rul_edit,
if(a.hapus=1, '', 'disabled') as rul_hapus,
if(a.print=1, '', 'disabled') as rul_print,
if(a.posting=1, '', 'disabled') as rul_posting,
b.link from menu_role a join menu b on a.id_menu=b.id where a.kd_user=$kd_user and b.link like '$link%'")->row_array();
    } else {
        return $ci->db->query("select a.*, b.*,
if(a.lihat=1, '', 'disabled') as rul_lihat,
if(a.tambah=1, '', 'disabled') as rul_tambah,
if(a.edit=1, '', 'disabled') as rul_edit,
if(a.hapus=1, '', 'disabled') as rul_hapus,
if(a.print=1, '', 'disabled') as rul_print,
if(a.posting=1, '', 'disabled') as rul_posting,
b.link from menu_role a join menu b on a.id_menu=b.id where a.kd_user=$kd_user and b.link ='$url'")->row_array();
    }
}

function aktifitas($ket) {
    $ci = &get_instance();
    $a = aksesLog();
    if ($ci->agent->is_browser()) {
        $browser = $ci->agent->browser() . ' ' . $ci->agent->version() . ' (' . $ci->agent->platform() . ')';
    } elseif ($ci->agent->is_robot()) {
        $browser = $ci->agent->robot();
    } elseif ($ci->agent->is_mobile()) {
        $browser = $ci->agent->mobile();
    } else {
        $browser = 'Unidentified User Agent';
    }
    $ip = $_SERVER['REMOTE_ADDR'];
    $nm_komputer = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    $data1['ip'] = $ip;
    $data1['tanggal'] = date("Y-m-d");
    $data1['jam'] = date("H:i:s");
    $data1['browser'] = $browser;
    $data1['komputer'] = $nm_komputer;
    $data1['nama_user'] = $a['nama_user'];
    $data1['keterangan'] = $ket;
    $data1['ket_level'] = $a['ket_level'];
    $data1['kd_user'] = $a['kd_user'];
    $ci->db->insert('aktifitas', $data1);
}

function batasAkt() {
    return 1;
}

function batasVerf() {
    return 5;
}

function intervalDate($date, $interval) {
    return date('Y-m-d', strtotime($date . " + $interval days"));
}

function intervalMinDate($date, $interval) {
    return date('Y-m-d', strtotime($date . " - $interval days"));
}

function Terbilang($x) {
    $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
        return " " . $abil[$x];
    elseif ($x < 20)
        return Terbilang($x - 10) . "belas";
    elseif ($x < 100)
        return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
    elseif ($x < 200)
        return " seratus" . Terbilang($x - 100);
    elseif ($x < 1000)
        return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
    elseif ($x < 2000)
        return " seribu" . Terbilang($x - 1000);
    elseif ($x < 1000000)
        return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
    elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}
