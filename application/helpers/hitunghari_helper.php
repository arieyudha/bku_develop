<?php

function tgl_awal_bulan($month, $year) {
    $tgl = date('01', mktime(0, 0, 0, $month, 1, $year));
    return $tgl;
}

function tgl_akhir_bulan($month, $year) {
    $tglAkhir = date('t', mktime(0, 0, 0, $month, 1, $year));
    return $tglAkhir;
}

function getHariKerjaPegawai($month, $year, $haker) {
    $tglAwal = date('Y-m-01', mktime(0, 0, 0, $month, 1, $year));
    $tglAkhir = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
    $tgl_awal_bulan = date('d', strtotime($tglAwal));
    $tgl_akhir_bulan = date('d', strtotime($tglAkhir));

    $hariKerja = array();
    if ($haker == 5) {
        for ($i = $tgl_awal_bulan; $i <= $tgl_akhir_bulan; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $i, $year));
            $w = strtotime($date);
            if (date('w', $w) !== '0' and date('w', $w) !== '6') {
                $hariKerja[] = sprintf('%02s', $i);
            }
        }
    } elseif ($haker == 6) {
        for ($i = $tgl_awal_bulan; $i <= $tgl_akhir_bulan; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $i, $year));
            $w = strtotime($date);
            if (date('w', $w) !== '0') {
                $hariKerja[] = sprintf('%02s', $i);
            }
        }
    }

    return $hariKerja;
}

function jumlahHariBulan($month, $year, $haker) {
    $tglAwal = date('Y-m-01', mktime(0, 0, 0, $month, 1, $year));
    $tglAkhir = date('Y-m-t', mktime(0, 0, 0, $month, 1, $year));
    $tgl_awal_bulan = date('d', strtotime($tglAwal));
    $tgl_akhir_bulan = date('d', strtotime($tglAkhir));

    $hariKerja = array();
    if ($haker == 5) {
        for ($i = $tgl_awal_bulan; $i <= $tgl_akhir_bulan; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $i, $year));
            $w = strtotime($date);
            if (date('w', $w) === '0' or date('w', $w) === '6') {
                $hariKerja[] = sprintf('%02s', $i);
            }
        }
    } elseif ($haker == 6) {
        for ($i = $tgl_awal_bulan; $i <= $tgl_akhir_bulan; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $i, $year));
            $w = strtotime($date);
            if (date('w', $w) === '0') {
                $hariKerja[] = sprintf('%02s', $i);
            }
        }
    }
    return $hariKerja;
}

function jumlahHariKerjaBulan($kd_level) {
    $haker = haker_skpd($kd_level);
    $data = count(jumlahHariBulan(date('m'), date('Y'), $haker));
    return $data;
}

function haker_skpd($kd_level) {
    $ci = &get_instance();
    $a = aksesLog();
    if ($kd_level == 1) {
        $kd_urusan = '4';
        $kd_bidang = '5';
        $kd_unit = '7';
    } else {
        $kd_urusan = $a['kd_urusan'];
        $kd_bidang = $a['kd_bidang'];
        $kd_unit = $a['kd_unit'];
    }
    $data = $ci->db->query("select b.*, if(isnull(a.hari_kerja), 5, a.hari_kerja) as hari_kerja from set_haker_skpd a 
right join ref_unit b on a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit 
where b.kd_urusan=$kd_urusan and b.kd_bidang=$kd_bidang and b.kd_unit=$kd_unit")->row()->hari_kerja;
    return $data;
}

function hariKerjaPegawai($month, $year, $kd_level) {
    $ttlHrLbr = kalenderMerah($month, $year, $kd_level);
    $ttlKalender = kalender_bulan($month, $year);
    $ttlHrKerja = array_diff($ttlKalender, $ttlHrLbr);
    return $ttlHrKerja;
}

function kalenderMerah($month, $year, $kd_level) {
    $haker = haker_skpd($kd_level);
    $hariLbr = jumlahHariLibur($month, $year, $haker);
    $wekeend = jumlahHariBulan($month, $year, $haker);
    $grop_array = array_merge($wekeend, $hariLbr);
    return $grop_array;
}

function kalender_bulan($month, $year) {
    $tglAwalBln = tgl_awal_bulan($month, $year);
    $tglAkhirBln = tgl_akhir_bulan($month, $year);
    $arrayKalender = array();
    for ($i = $tglAwalBln; $i <= $tglAkhirBln; $i++) {
        $arrayKalender[] = sprintf('%02s', $i);
    }
    return $arrayKalender;
}

function jumlahHariLibur($month, $year, $haker) {
    $ci = &get_instance();
    $ci->load->model('setting/Model_absen');
    $tgl_libur = $ci->Model_absen->getHariLibur($month, $year)->result();
    $tglLibur = array();
    foreach ($tgl_libur as $row) {
        $tglLibur[] = date('d', strtotime($row->tgl));
    }
    $count = count($tglLibur);
    $hariLibur = array();
    if ($haker == 5) {
        for ($i = 0; $i < $count; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $tglLibur[$i], $year));
            $w = strtotime($date);
            if (date('w', $w) !== '0' && date('w', $w) !== '6') {
                $hariLibur[] = $tglLibur[$i];
            }
        }
    } elseif ($haker == 6) {
        for ($i = 0; $i < $count; $i++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $tglLibur[$i], $year));
            $w = strtotime($date);
            if (date('w', $w) !== '0') {
                $hariLibur[] = $tglLibur[$i];
            }
        }
    }
    return $hariLibur;
}

function hariKerjaPegawaiSub($month, $year, $kd_urusan, $kd_bidang, $kd_unit, $kd_sub) {
    $ttlHrLbr = kalenderMerahSub($month, $year, $kd_urusan, $kd_bidang, $kd_unit, $kd_sub);
    $ttlKalender = kalender_bulan($month, $year);
    $ttlHrKerja = array_diff($ttlKalender, $ttlHrLbr);
    return $ttlHrKerja;
}

function kalenderMerahSub($month, $year, $kd_urusan, $kd_bidang, $kd_unit, $kd_sub) {
    $haker = haker_sub_skpd($kd_urusan, $kd_bidang, $kd_unit, $kd_sub);
    $hariLbr = jumlahHariLibur($month, $year, $haker);
    $wekeend = jumlahHariBulan($month, $year, $haker);
    $grop_array = array_merge($wekeend, $hariLbr);
    return $grop_array;
}

function haker_sub_skpd($kd_urusan, $kd_bidang, $kd_unit, $kd_sub) {
    $ci = &get_instance();
    $data = $ci->db->query("select b.*, if(isnull(a.hari_kerja), 5, a.hari_kerja) as hari_kerja from set_haker_skpd a 
right join ref_sub_unit b on a.kd_urusan=b.kd_urusan and a.kd_bidang=b.kd_bidang and a.kd_unit=b.kd_unit and a.kd_sub=b.kd_sub
where b.kd_urusan='$kd_urusan' and b.kd_bidang='$kd_bidang' and b.kd_unit='$kd_unit' and b.kd_sub='$kd_sub'")->row()->hari_kerja;
    return $data;
}
