<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config_Controller
 *
 * @author BappedaKalsel
 */
class Config_Controller extends CI_Controller {

    //put your code here

    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
        $this->load->model(['utility/Model_utility', 'master/Model_ref', 'setting/Model_pptk']);
    }

    function get_dataPptk($kd_level, $kd_jabatan = null) {
        $getDataPegawai = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        if ($kd_level == 1) {
            $data_pptk = $this->Model_pptk->get_pptkPegawai()->result();
        } elseif ($kd_level == 2) {
            $data_pptk = $this->Model_pptk->get_pptkPegawai($kd_jabatan)->result();
        }
        $data_array = array();
        foreach ($getDataPegawai as $row_pgw) {
            foreach ($data_pptk as $row_pptk) {
                if ($row_pgw->kd_jabatan == $row_pptk->kd_jabatan) {
                    $data['jml_keg'] = $row_pptk->jml_keg;
                    $data['kd_pptk'] = $row_pptk->kd_pptk;
                    $data['kd_jabatan'] = $row_pptk->kd_jabatan;
                    $data['nama_jabatan'] = $row_pgw->nama_jabatan;
                    $data['nama_lengkap'] = $row_pgw->glr_depan . ' ' . $row_pgw->nama . ' ' . $row_pgw->glr_belakang;
                    $data['nip'] = $row_pgw->nip;
                    $data['pangkat'] = $row_pgw->pangkat;
                    $data['golongan'] = $row_pgw->golongan;
                    $data_array[] = $data;
                }
            }
        }
        return $data_array;
    }

    function get_dataValidator($kd_level, $kd_jabatan = null) {
        $getDataPegawai = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        if ($kd_level == 1) {
            $data_validator = $this->Model_pptk->get_validatorPegawai()->result();
        } elseif ($kd_level == 3) {
            $data_validator = $this->Model_pptk->get_validatorPegawai($kd_jabatan)->result();
        }
        $data_array = array();
        foreach ($getDataPegawai as $row_pgw) {
            foreach ($data_validator as $row_val) {
                if ($row_pgw->kd_jabatan == $row_val->kd_jabatan) {
                    $data['kd_validator'] = $row_val->kd_validator;
                    $data['kd_jabatan'] = $row_val->kd_jabatan;
                    $data['nama_jabatan'] = $row_pgw->nama_jabatan;
                    $data['nama_lengkap'] = $row_pgw->glr_depan . ' ' . $row_pgw->nama . ' ' . $row_pgw->glr_belakang;
                    $data['nip'] = $row_pgw->nip;
                    $data['pangkat'] = $row_pgw->pangkat;
                    $data['golongan'] = $row_pgw->golongan;
                    $data_array[] = $data;
                }
            }
        }
        return $data_array;
    }

    function get_dataPptkValidator($kd_jabatan) {
        $getDataPegawai = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        $data_validator = $this->Model_pptk->get_pegawaiPptkWhereValidator($kd_jabatan)->result();
        $data_array = array();
        foreach ($getDataPegawai as $row_pgw) {
            foreach ($data_validator as $row_val) {
                if ($row_pgw->kd_jabatan == $row_val->kd_jabatan_pptk) {
                    $data['kd_validator'] = $row_val->kd_validator;
                    $data['kd_pptk'] = $row_val->kd_pptk;
                    $data['kd_jabatan'] = $row_val->kd_jabatan;
                    $data['nama_jabatan'] = $row_pgw->nama_jabatan;
                    $data['nama_lengkap'] = $row_pgw->glr_depan . ' ' . $row_pgw->nama . ' ' . $row_pgw->glr_belakang;
                    $data['nip'] = $row_pgw->nip;
                    $data['pangkat'] = $row_pgw->pangkat;
                    $data['golongan'] = $row_pgw->golongan;
                    $data_array[] = $data;
                }
            }
        }
        return $data_array;
    }

    function get_dataPegawaiPptk($kd_jabatan) {
        $getDataPegawai = jsonCurl(urlApiOffice('Api_pegawai/getDataPegawai'));
        $data_pptk = $this->Model_pptk->get_pptkPegawai($kd_jabatan)->result();
        $data_array = array();
        foreach ($getDataPegawai as $row_pgw) {
            foreach ($data_pptk as $row_pptk) {
                if ($row_pgw->kd_jabatan == $row_pptk->kd_jabatan) {
                    $data['jml_keg'] = $row_pptk->jml_keg;
                    $data['kd_pptk'] = $row_pptk->kd_pptk;
                    $data['kd_jabatan'] = $row_pptk->kd_jabatan;
                    $data['nama_jabatan'] = $row_pgw->nama_jabatan;
                    $data['nama_lengkap'] = $row_pgw->glr_depan . ' ' . $row_pgw->nama . ' ' . $row_pgw->glr_belakang;
                    $data['nip'] = $row_pgw->nip;
                    $data['pangkat'] = $row_pgw->pangkat;
                    $data['golongan'] = $row_pgw->golongan;
                    $data_array[] = $data;
                }
            }
        }
        return $data_array;
    }

    function getKdTahap($kd_bulan = null) {
        $kd_tahap = $this->Model_utility->getKdTahap($kd_bulan);
        return $kd_tahap;
    }

    function getKdJabatanPptk($kd_level) {
        if ($kd_level == 1) {
            $kd_jabatan = isset($_REQUEST['kd_jabatan']) ? $_REQUEST['kd_jabatan'] : '';
        } elseif ($kd_level == 2) {
            $kd_jabatan = $this->a['kd_jabatan'];
        }
        return $kd_jabatan;
    }

    function getUnitkey() {
        $data = '94_';
        return $data;
    }

    function getKdBidang() {
        $data = '4.03.';
        return $data;
    }

    function getKdUrusan() {
        $data = '4.';
        return $data;
    }

    function getKdSkpd() {
        $data = '4.03.01.';
        return $data;
    }

    function webService($port, $url, $parameter) {
        $curl = curl_init();
        set_time_limit(0);
        curl_setopt_array($curl, array(
            CURLOPT_PORT => $port,
            CURLOPT_URL => "http://" . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $parameter,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
                )
        );
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $response = ("Error #:" . $err);
        } else {
            $response;
        }

        return $response;
    }

}
