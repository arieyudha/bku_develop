<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_Auth
 *
 * @author Asus
 */
class Model_Auth extends CI_Model {

    //put your code here
    public function validate_login($username) {
        $query = $this->db->query("select a.*, c.ket_level from user a
            join user_level c on c.kd_level=a.kd_level
            where a.username=" . escape_string($username) . " limit 1");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function cekUsername($username) {
        $query = $this->db->query("select count(*) as cek from user a where a.username='$username'");
        if ($query) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_levelUser($kd_level = null) {
        if ($kd_level == '') {
            $query = $this->db->get("user_level");
        } else {
            $data = array(
                'kd_level' => $kd_level
            );
            $query = $this->db->get_where("user_level", $data);
        }
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    function get_userWhereLevel($kd_level = null) {
        if ($kd_level == null) {
            $query = $this->db->query("select * from user a");
        } else {
            $query = $this->db->query("select * from user a where a.kd_level=$kd_level");
        }
        return $query;
    }

    function get_userWhereKdUnit($kode_unit, $kd_level = null) {
        if ($kd_level == '') {
            $query = $this->db->query("select * from user a join user_group b on a.kd_user=b.kd_user
where b.kode_unit='$kode_unit'");
        } else {
            $query = $this->db->query("select * from user a join user_group b on a.kd_user=b.kd_user
where b.kode_unit='$kode_unit' and a.kd_level=$kd_level");
        }
        return $query;
    }

    public function user_group($kd_level, $kd_user = null) {
        if ($kd_level == 2) {
            $query = $this->db->query("select a.*, b.kode_unit, b.kd_sub, b.nip from user a 
join user_group b on a.kd_user=b.kd_user
where a.kd_user=$kd_user order by kd_level , kd_user asc");
        }
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    public function get_user($kd_user = '') {
        if ($kd_user === '') {
            $query = $this->db->query("select a.*, c.ket_level, b.kd_jabatan from user a 
left join user_group b on b.kd_user=a.kd_user
join user_level c on c.kd_level=a.kd_level");
        } else {
            $query = $this->db->query("select a.*, c.ket_level, b.kd_jabatan from user a 
left join user_group b on b.kd_user=a.kd_user
join user_level c on c.kd_level=a.kd_level
            where a.kd_user='$kd_user'");
        }
        if ($query) {
            return $query;
        } else {
            return false;
        }
    }

    public function maxUser() {
        $query = $this->db->query("select max(kd_user)+1 jml_user from user");
        if ($query) {
            return $query->row()->jml_user;
        } else {
            return false;
        }
    }

    public function getUserGroupSubSkpd() {
        $query = $this->db->query("select a.*, b.kode_unit, b.kd_sub from user a join user_group b on a.kd_user=b.kd_user and a.kd_level=b.kd_level where a.kd_level=4");
        return $query;
    }

    public function getUserGroupPgw() {
        $query = $this->db->query("select a.*, b.nip from user a join user_group b on a.kd_user=b.kd_user and a.kd_level=b.kd_level where a.kd_level=3");
        return $query;
    }

    public function getUserGroupSkpd() {
        $query = $this->db->query("select a.*, b.kode_unit from user a join user_group b on a.kd_user=b.kd_user and a.kd_level=b.kd_level where a.kd_level=2");
        return $query;
    }

    public function aktifitas($data) {
        $query = $this->db->order_by('tanggal', 'desc')->order_by('jam', 'desc')->get_where('aktifitas', $data);
        return $query;
    }

    function get_menu() {
        $query = $this->db->query("SELECT b.*, if(b.link = '#', (select COUNT(parent) from menu a where a.parent=b.id), 0) as jml_menu FROM menu b order by parent, urutan asc ");
        return $query;
    }

    function get_menuWhereAdminSkpd($admin, $skpd = NULL) {
        if ($skpd == '') {
            $query = $this->db->query("SELECT b.* FROM menu b where admin=$admin order by parent, urutan asc");
        } elseif ($skpd != '') {
            $query = $this->db->query("SELECT b.* FROM menu b where admin=$admin and skpd=$skpd order by parent, urutan asc");
        }
        return $query;
    }

    function get_menuAkses($kd_user) {
        $query = $this->db->query("SELECT b.*
FROM menu b where id in(select id_menu from menu_role a where a.kd_user=$kd_user) order by parent, urutan asc ");
        return $query;
    }

    function update_urutanTambah($urutan, $parent) {
        $query = $this->db->query("update menu a, (select urutan+1 as urutan, id from menu where urutan >=$urutan and parent = $parent) as r set a.urutan = r.urutan where a.id=r.id");
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_urutanEditMin($parent, $urutan, $urutan_edit) {
        $urutan_plus = $urutan_edit + 1;
        $query = $this->db->query("update menu a set a.urutan = a.urutan-1 where a.parent=$parent and  a.urutan BETWEEN $urutan_plus and $urutan ");
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_urutanEditPlus($parent, $urutan, $urutan_edit) {
        $urutan_min = $urutan_edit - 1;
        $query = $this->db->query("update menu a set a.urutan = a.urutan+1 where a.parent=$parent and a.urutan BETWEEN $urutan and $urutan_min");
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_urutanHapus($parent, $urutan) {
        $urutan_min = $urutan_edit - 1;
        $query = $this->db->query("update menu a, (select urutan-1 as urutan, id from menu where urutan >= $urutan and parent = $parent) as r set a.urutan = r.urutan where a.id=r.id;");
        if ($query) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_ruleMenu($kd_user) {
        $query = $this->db->query("select a.*, b.nama, b.parent, b.link, b.urutan, b.icon, b.id from menu_role a join menu b on a.id_menu=b.id where a.kd_user=$kd_user order by urutan asc, b.parent asc");
        return $query;
    }

    function get_menuWhereField($field) {
        $query = $this->db->query("SELECT b.* FROM menu b where $field=1 order by parent, urutan asc ");
        return $query->result();
    }

    function get_pegawai($url) {
        $query = $this->db->query("SELECT a.kd_jabatan id, a.parent_jabatan pid, a.nama, a.nip, b.nama_jabatan jabatan, CONCAT('$url',a.foto) foto FROM ref_pegawai a JOIN
ref_jabatan b ON a.kd_jabatan = b.kd_jabatan
ORDER BY b.kd_jabatan");
        return $query->result();
    }

}
