<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Not_found
 *
 * @author Asus
 */
class Not_found extends MY_Controller {

    //put your code here
    var $a;

    public function __construct() {
        parent::__construct();
        $this->a = aksesLog();
    }

    public function index() {
        $record = $this->javasc_back();
        //data
        $data = $this->layout_back('not_found', $record);
        $data['ribbon_left'] = ribbon_left('Not Found', 'Error 404');
        $data['ribbon_right'] = ribbon_right($this->a['ket_level'], $this->a['nama_user']);
        $this->backend($data);
    }

}
